package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import seek.ReadDataset;

public class ViewDataset extends HttpServlet {

	public void doPost( HttpServletRequest req, HttpServletResponse res )
		throws ServletException, IOException {

		HttpSession session = req.getSession();
		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		String organism = req.getParameter("organism");

		//Accepted input parameters:
		//dset_keyword
		//dset
		//category_keyword
		//category
		//print_annot (y/n)
		//page
		//mode (0, 1, 2, 3, 4)

		//Returns:
		//Mode 0: get datasets annotated to category
		//If print_annot=="y"
		//INPUT: (page)
		//OUTPUT:
		//total number of pages
		//gse	gse_annotation
		//If print_annot=="n"
		//gse

		//Mode 1: search dataset within selection
		//INPUT: (dset, page, dset_keyword)
		//OUTPUT:
		//total number of pages
		//gse	gse_annotation

		//Mode 2: search for a category
		//INPUT: (category_keyword, page)
		//OUTPUT:
		//total number of pages
		//category	dataset count

		//Mode 3: get category,dataset count
		//INPUT: (category, page)
		//OUTPUT:
		//total number of pages
		//category	dataset count

		int mode = Integer.parseInt(java.net.URLDecoder.decode(
			req.getParameter("mode"), "UTF-8"));

		int numPerPage = 100;

		//MODE 0 =======================================================
		if(mode==0){

			String dcat = java.net.URLDecoder.decode(
				req.getParameter("category"), "UTF-8");
			String[] cat = StringUtils.split(dcat, ";");

			SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
			sf.ReadDatasetMap();	
			sf.ReadDatasetAnnotation();
			sf.ReadGSEDescription();

			Vector<String> v_dset = new Vector<String>();
			Set<String> hashDsetList = new HashSet<String>();
			for(int i=0; i<cat.length; i++){
				if(sf.mapEntity.containsKey(cat[i])){
					Vector<String> gselist = sf.mapEntity.get(cat[i]);
					for(String g : gselist)
						for(String gg : sf.gse_index.get(g))
							hashDsetList.add(gg);
				}
			}

			for(String d : hashDsetList)
				v_dset.add(d);

			Map<String, String> gse_annot = new HashMap<String,String>();
			for(String d : v_dset){
				String gse_id = "";
				if(d.indexOf("GSE")==0){
					int gid = 0;
					if(d.contains("_"))
						gid = d.indexOf("_");
					else
						gid = d.indexOf(".");
					gse_id = d.substring(0, gid);
				}else{
					gse_id = d;
				}
				String combined = sf.mapGSEDescription.get(gse_id);
				for(String gg : sf.gse_index.get(gse_id))
					gse_annot.put(gg, combined);
			}

			Vector<String> keys = new Vector<String>(gse_annot.keySet());
			Collections.sort(keys);

			String[] stbuilder = null;

			String print_annot = java.net.URLDecoder.decode(
				req.getParameter("print_annot"), "UTF-8");

			if(print_annot.equals("y")){
				int pageID = Integer.parseInt(java.net.URLDecoder.decode(
					req.getParameter("page"), "UTF-8")) - 1;
				int numPages = (int) gse_annot.size() / numPerPage;
				if(gse_annot.size() % numPerPage != 0)
					numPages++;
			
				int start_i = numPerPage * pageID;
				int end_i = start_i + numPerPage;
				if(pageID==numPages-1 || numPages==0)
					end_i = gse_annot.size();

				stbuilder = new String[end_i - start_i + 1]; //1 more to store total # pages
				stbuilder[0] = Integer.toString(numPages);
				for(int i=start_i; i<end_i; i++){
					String e = keys.get(i);
					stbuilder[i-start_i+1] = String.format("%s\t%s", e, gse_annot.get(e));
				}
			}else{
				stbuilder = new String[keys.size()];
				for(int i=0; i<keys.size(); i++)
					stbuilder[i] = String.format("%s", keys.get(i));
			}

			String response = StringUtils.join(stbuilder, ";");
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(response);

		//MODE 1 ===========================================================
		}else if(mode == 1){ //search dataset within a selection

			String dcat = java.net.URLDecoder.decode(
				req.getParameter("dset"), "UTF-8");
			String[] dset = StringUtils.split(dcat, ";");

			String keyword = java.net.URLDecoder.decode(
				req.getParameter("dset_keyword"), "UTF-8");

			SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
			sf.ReadDatasetMap();	
			sf.ReadDatasetAnnotation();
			sf.ReadGSEDescription();

			Vector<String> v_dset = new Vector<String>();
			for(int i=0; i<dset.length; i++)
				v_dset.add(dset[i]); //each should be GSE.GPL

			Vector<String> filtered = new Vector<String>();
			Map<String, String> gse_annot = new HashMap<String,String>();
			int ii = StringUtils.indexOfIgnoreCase(keyword, "platform:");
			boolean matchPlatform = false;
			if(ii!=-1){
				keyword = keyword.substring(ii + 9 +1);
				matchPlatform = true;
			}

			for(String d : v_dset){
				String gse_id = "";
				if(d.indexOf("GSE")==0){
					int gid = 0;
					if(d.contains("_"))
						gid = d.indexOf("_");
					else
						gid = d.indexOf(".");
					gse_id = d.substring(0, gid);
				}else{
					gse_id = d;
				}
				String combined = sf.mapGSEDescription.get(gse_id);
				for(String gg : sf.gse_index.get(gse_id))
					gse_annot.put(gg, combined);

				if(matchPlatform){
					String gpl_id = d.substring(d.indexOf(".")+1);
					if(StringUtils.containsIgnoreCase(gpl_id, keyword))
						filtered.add(d);
				}else{ //not platform match
					if(StringUtils.containsIgnoreCase(combined, keyword)){
						filtered.add(d);
						//for(String gg : sf.gse_index.get(gse_id))
						//	filtered.add(gg);
					}
				}
			}

			Collections.sort(filtered);

			String[] stbuilder = null;
			String print_annot = java.net.URLDecoder.decode(
				req.getParameter("print_annot"), "UTF-8");

			if(print_annot.equals("y")){
				int pageID = Integer.parseInt(java.net.URLDecoder.decode(
					req.getParameter("page"), "UTF-8")) - 1;
				int numPages = (int) filtered.size() / numPerPage;
				if(filtered.size() % numPerPage != 0)
					numPages++;
			
				int start_i = numPerPage * pageID;
				int end_i = start_i + numPerPage;
				if(pageID==numPages-1 || numPages==0)
					end_i = filtered.size();

				stbuilder = new String[end_i - start_i + 1]; //1 more to store total # pages
				stbuilder[0] = Integer.toString(numPages);
				for(int i=start_i; i<end_i; i++){
					String e = filtered.get(i);
					stbuilder[i-start_i+1] = String.format("%s\t%s", e, gse_annot.get(e));
				}
			}else{
				stbuilder = new String[filtered.size()];
				for(int i=0; i<filtered.size(); i++)
					stbuilder[i] = String.format("%s", filtered.get(i));
			}
			String response = StringUtils.join(stbuilder, ";");
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(response);

		//MODE 2 or 3  ===========================================================
		}else if(mode == 2 || mode == 3){ 
			SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
			String dataset_annot = sf.getPath("annot");

			Map<String, Vector<String> > mapDataset = 
			ReadDataset.readDatasetAnnotation(dataset_annot);
		
			Map<String, Vector<String> > mapEntity = 
			ReadDataset.convertDatasetAnnotation(mapDataset);
		
			Vector<String> ent = null;
			//MODE 2 ===========================================================
			if(mode==2){
				String keyword = java.net.URLDecoder.decode(
					req.getParameter("category_keyword"), "UTF-8");

				ent = new Vector<String>();
				for(String e:mapEntity.keySet()){
					if(StringUtils.containsIgnoreCase(e, keyword))
						ent.add(e);
				}
			//MODE 3 ===========================================================
			}else if(mode==3){
				Set<String> entities = mapEntity.keySet();
				ent = new Vector<String>(entities);
			}

			Collections.sort(ent);

			int pageID = Integer.parseInt(java.net.URLDecoder.decode(
				req.getParameter("page"), "UTF-8")) - 1;
			int numPages = (int) ent.size() / numPerPage;
			if(ent.size() % numPerPage != 0)
				numPages++;
			int start_i = numPerPage * pageID;
			int end_i = start_i + numPerPage;
			if(pageID==numPages-1 || numPages==0)
				end_i = ent.size();
			
			String[] stbuilder = new String[end_i - start_i + 1];
			stbuilder[0] = Integer.toString(numPages);
			for(int i=start_i; i<end_i; i++){
				String e = ent.get(i);
				stbuilder[i-start_i+1] = String.format("%s\t%s", e, mapEntity.get(e).size());
			}
			String response = StringUtils.join(stbuilder, ";");
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(response);
		}

		//MODE 4 ============================================================
		else if(mode == 4){
		
			String keyword = req.getParameter("dset_keyword");
			if(keyword==null)
				keyword = "";

			SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
			sf.ReadDatasetMap();	
			sf.ReadDatasetAnnotation();
			sf.ReadGSEDescription();

			int ii = StringUtils.indexOfIgnoreCase(keyword, "platform:");
			boolean matchPlatform = false;
			if(ii!=-1){
				keyword = keyword.substring(ii + 9 +1);
				matchPlatform = true;
			}

			String name_mapping = sf.dataset_platform_file;
			Map<String, Integer> mm = ReadScore.readDatasetMapping(name_mapping);
			Map<String, String> dsetPlat = ReadScore.readDatasetPlatformMap(name_mapping);
	
    		String sessionID = req.getParameter("sessionID");
			File tempFile = new File(sf.getPath(sessionID + "_dweight"));
	
			float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
			Vector<Pair> vp = ReadScore.SortScores(mm, sc, false);

			Map<String, String> gse_annot = new HashMap<String,String>();
			Vector<String> filtered = new Vector<String>();
			Vector<Integer> rank = new Vector<Integer>();

			for(int i=0; i<vp.size(); i++){
				String term = vp.get(i).term;
				String gse_id = ReadScore.getDatasetID(term);
				/*if(term.indexOf("GSE")==0){
					int gid = 0;
					if(term.contains("_"))
						gid = term.indexOf("_");
					else
						gid = term.indexOf(".");
					gse_id = term.substring(0, gid);
				}else{
					gse_id = term;
				}*/
				String combined = sf.mapGSEDescription.get(gse_id);
				gse_annot.put(term, combined);
				if(matchPlatform){
					String gpl_id = dsetPlat.get(term);
					//String gpl_id = term.substring(term.indexOf(".")+1);
					if(StringUtils.containsIgnoreCase(gpl_id, keyword)){
						filtered.add(term);
						rank.add(i+1);
					}
				}else{ //not platform match
					if(StringUtils.containsIgnoreCase(combined, keyword)){
						filtered.add(term);
						rank.add(i+1);
					}
				}
			}

			String print_annot = java.net.URLDecoder.decode(
				req.getParameter("print_annot"), "UTF-8");

			String[] stbuilder = null;
			if(print_annot.equals("y")){
				int pageID = Integer.parseInt(java.net.URLDecoder.decode(
					req.getParameter("page"), "UTF-8")) - 1;
				int numPages = (int) filtered.size() / numPerPage;
				if(filtered.size() % numPerPage != 0)
					numPages++;
		
				int start_i = numPerPage * pageID;
				int end_i = start_i + numPerPage;
				if(pageID==numPages-1 || numPages==0)
					end_i = filtered.size();

				stbuilder = new String[end_i - start_i + 1];
				stbuilder[0] = Integer.toString(numPages);
				for(int i=start_i; i<end_i; i++){
					String e = filtered.get(i);
					stbuilder[i-start_i+1] = String.format("%d\t%s\t%s", rank.get(i), e, gse_annot.get(e));
				}
			}else if(print_annot.equals("n")){
				stbuilder = new String[filtered.size()];
				for(int i=0; i<filtered.size(); i++)
					stbuilder[i] = String.format("%s", filtered.get(i));
			}

			String response = StringUtils.join(stbuilder, ";");
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write(response);
				
		}

	}

}
