package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.*;
import seek.Pair;

public class ReadScore{
	//if sorting by negative correlations
	public static boolean isNegativeCor(String folder, String sessionID) throws IOException{
		String path = folder + "/" + sessionID + "_neg_cor";
		if((new File(path)).exists()){
			return true;
		}else{
			return false;
		}
	}

	//entrez to gene(internal ID) mapping
	public static Map<String, Integer> readGeneMapping(String fileEntrez2ID)
		throws IOException{

		//read entrez to ID mapping
		Map<String, Integer> ent_id = new HashMap<String, Integer>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2ID));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				ent_id.put(ent, Integer.parseInt(id));
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");

		}
		return ent_id;
	}

	public static Map<String, String> readGeneSymbol2EntrezMapping(
		String file){
		Map<String, String> symbol_ent = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				symbol_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file Gene symbol 2 entrez");
		}
		return symbol_ent;
	}

	//returns mapping two ways
	public static void readGeneEntrezSymbolMapping(String fileEntrez2HGNC,
		Map<String, String> ent_hgnc, Map<String, String> hgnc_ent) throws IOException{
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				ent_hgnc.put(entrez, gene_name);
				hgnc_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");
		}
	}

	public static String getDatasetID(String dataset){
		String gse_id = "";
		int gse_loc = dataset.indexOf("GSE");
		int u_id = dataset.indexOf("_");
		int d_id = dataset.indexOf(".");
		gse_id = "";

		if(gse_loc==0){ //if it is a GSE dataset
			if(u_id==-1 && d_id!=-1){
				gse_id = dataset.substring(0, d_id);
			}
			else if(u_id!=-1 && d_id==-1){
				gse_id = dataset.substring(0, u_id);
			}
			else if(u_id!=-1 && d_id!=-1){
				if(u_id<d_id){
					gse_id = dataset.substring(0, u_id);
				}else{
					gse_id = dataset.substring(0, d_id);
				}
			}
			else{ //u_id==-1 && d_id==-1
				gse_id = dataset;
			}
		}else if(dataset.indexOf("TCGA")==0){ // if it is a TCGA dataset
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else if(dataset.indexOf("RNASeq-1")==0 || dataset.indexOf("RNASeq-2")==0){ // if it is a combined RNASEQ dataset (human applicable only)
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else if(dataset.indexOf("WBPaper")==0){
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else{ //if it is not a GSE dataset
			gse_id = dataset;
		}
		return gse_id;
	}

	public static String getDatasetName(String dataset){
		String gse_id = "";
		int gse_loc = dataset.indexOf("GSE");
		int u_id = dataset.indexOf("_");
		int d_id = dataset.indexOf(".GPL");
		gse_id = "";

		if(gse_loc==0 && d_id!=-1){ //if it is a GSE dataset and has a platform ID beginning GPL
			if(u_id==-1 && d_id!=-1){ //example: GSE1234.GPL123
				gse_id = dataset.substring(0, d_id);
			}
			//else if(u_id!=-1 && d_id==-1){ //example: GSE1234_2 or GSE1234_2.gsm (does not happen)
			//	gse_id = dataset.substring(0, u_id);
			//}
			else if(u_id!=-1 && d_id!=-1){ //example: GSE1234_2.GPL123
				if(u_id<d_id){
					gse_id = dataset.substring(0, u_id);
				}else{ //example: GSE1234.GPL1234_2 (rarely happens!)
					gse_id = dataset.substring(0, d_id);
				}
			}
			//else{ //u_id==-1 && d_id==-1
			//	gse_id = dataset;
			//}
		}else if(gse_loc==0 && d_id==-1){ //a GSE dataset, but platform do not begin with GPL
			//example: GSE1234.gsm or GSE1234_gsm
			gse_id = dataset;

		}else if(dataset.indexOf("TCGA")==0){ // if it is a TCGA dataset
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else if(dataset.indexOf("RNASeq-1")==0 || dataset.indexOf("RNASeq-2")==0){ // if it is a combined RNASEQ dataset (human applicable only)
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else if(dataset.indexOf("WBPaper")==0){
			int td_id = dataset.indexOf(".");
			gse_id = dataset.substring(0, td_id);
		}else{ //if it is not a GSE dataset
			gse_id = dataset;
		}
		return gse_id;
	}

	//map dataset name to platform name
	public static Map<String,String> readDatasetPlatformMap(String file) 
		throws IOException{
		Map<String,String> m = new HashMap<String,String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String ss = null;
			while((ss=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(ss, "\t");
				String s1 = st.nextToken();
				String s2 = st.nextToken();
				m.put(s1, s2);
			}
			in.close();
		}catch(IOException e){
			System.out.println("BAD I/O!");
		}
		return m;
	}

	//read dataset mapping three ways
	public static void readDatasetPlatformMap(String file, 
		Map<String,Integer> dset_int, Map<Integer,String> int_dset, 
		Map<String,Vector<String> > gse_index) throws IOException {
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			String gse_id = "";
			while((s=in.readLine())!=null){
				//making dset_int, and int_dset
				StringTokenizer st = new StringTokenizer(s, "\t");
				String dataset = st.nextToken();
				String platform = st.nextToken();
				gse_id = getDatasetID(dataset);
				String dset = dataset; //use full dataset name (incl. ".pcl") as key

				dset_int.put(dset, i);
				int_dset.put(i, dset);
				i++;

				Vector<String> vs;
				if(gse_index.containsKey(gse_id)){ //use solely the GSE id as key
					vs = gse_index.get(gse_id);
				}else{
					vs = new Vector<String>();
				}
				vs.add(dset);
				gse_index.put(gse_id, vs);

			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file");
		}
	}

	//read entrez to ID mapping 2 ways
	public static void readEntrezIntMap(String file, Map<String,Integer> ent_int,
		Map<Integer, String> int_ent) throws IOException{
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				int ii = Integer.parseInt(id);
				ent_int.put(ent, ii);
				int_ent.put(ii, ent);
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");
		}
	}

	public static Map<String, String> readGeneEntrez2Sym(String fileEntrez2HGNC) throws
		IOException{
		Map<String, String> ent_hgnc = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				ent_hgnc.put(entrez, gene_name);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");
		}
		return ent_hgnc;
	}

	//return hgnc to ID mapping
	public static Map<String, Integer> readGeneMapping(String fileEntrez2ID, 
		String fileEntrez2HGNC) throws IOException {
			
		Map<String, Integer> m = new HashMap<String, Integer>();
		
		//read entrez-HGNC mapping
		Map<String, String> hgnc_ent = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2HGNC));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String entrez = st.nextToken();
				String gene_name = st.nextToken();
				hgnc_ent.put(gene_name, entrez);
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 0");

		}
		
		//read entrez to ID mapping
		Map<String, String> ent_id = new HashMap<String, String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(fileEntrez2ID));
			String s = null;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String id = st.nextToken();
				String ent = st.nextToken();
				ent_id.put(ent, id);
			}
		}catch(IOException e){
			System.out.println("Error opening file 1");

		}
		
		for(String key : hgnc_ent.keySet()){
			String val = hgnc_ent.get(key);
			if(ent_id.containsKey(val)){
				m.put(key, Integer.parseInt(ent_id.get(val)));
			}
		}
		return m;
	}

	public static Vector<Vector<String> > readQueryParts(String file)
		throws IOException {
		Vector<Vector<String> > m = new Vector<Vector<String> >();
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, " ");
				Vector<String> vs = new Vector<String>();
				while(st.hasMoreTokens()){
					vs.add(st.nextToken());
				}
				m.add(vs);
				i++;
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 2");

		}	
		return m;
	}
	
	public static Vector<String> readQuery(String file)
		throws IOException {
		Vector<String> m = new Vector<String>();
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = in.readLine();
			StringTokenizer st = new StringTokenizer(s, " ");
			while(st.hasMoreTokens()){
				m.add(st.nextToken());
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 2");

		}	
		return m;
	}
	public static Map<String, Integer> readDatasetMapping(String file) 
		throws IOException {
			
		Map<String, Integer> m = new HashMap<String, Integer>();
		
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String dataset = st.nextToken();
				String platform = st.nextToken();
				String dset = dataset;
				//String[] dd = StringUtils.split(dataset, ".");
				//String dset = dd[0] + "." + dd[1]; //platform and dataset name
				m.put(dset, i);
				i++;
			}
			in.close();
		}catch(IOException e){	
			System.out.println("Error opening file 2");

		}
		
		return m;
	}

	public static float[] ReadScoreBinary(String tempFile) throws IOException{
		float[] sc = null;
	
		try{
			DataInputStream in = new DataInputStream(new BufferedInputStream(
				new FileInputStream(tempFile)));
			byte[] array_size = new byte[8];
			in.read(array_size, 0, 8);
			ByteBuffer buf = null;
			buf = ByteBuffer.wrap(array_size);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			long size = buf.getInt() & 0xffffffffL;
			//System.out.println("Size is " + size);
			
			byte[] ss_tmp = new byte[4*(int) size];
			in.read(ss_tmp, 0, ss_tmp.length);
			buf = ByteBuffer.wrap(ss_tmp);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			sc = new float[(int) size];
			for(int i=0; i<sc.length; i++){
				sc[i] = buf.getFloat();
				//System.out.println(String.format("%.5e", sc[i]));
			}
		
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file 3");
		}

		return sc;
	}

	public static float[] ReadScoreBinaryWithQueryNull(String tempFile, String queryFile, 
	Map<String,Integer> mm, float fNA) throws IOException{
		float[] sc = null;
		Vector<String> query = null;
	
		try{
			sc = ReadScoreBinary(tempFile);
			query = readQuery(queryFile);
			for(String q: query){
				int qid = mm.get(q);
				sc[qid] = fNA;
			}
		}catch(IOException e){
			System.out.println("Error opening file 3");
		}

		return sc;
	}

	//mm is entity mapping (one of ReadDatasetMapping or ReadGeneMapping)
	public static Vector<Pair> SortScores(Map<String, Integer> mm, float[] sc, boolean negativeCor){

		Vector<String> mp = new Vector<String>(mm.keySet());
		Vector<Pair> vp = new Vector<Pair>();
		for(int i=0; i<mp.size(); i++){
			Pair np = new Pair(mp.get(i), sc[mm.get(mp.get(i))], mm.get(mp.get(i)));
			//System.out.printf("%.5f\n", sc[mm.get(mp.get(i))]);
			vp.add(np);
		}

		if(negativeCor){
			Collections.sort(vp);
		}else{
			Collections.sort(vp);
			Collections.reverse(vp);
		}
		Vector<Pair> valid = new Vector<Pair>();
		if(negativeCor){
			for(int i=0; i<vp.size(); i++){
				if(vp.get(i).val>300){
					continue;
				}
				valid.add(new Pair(vp.get(i).term, vp.get(i).val, vp.get(i).index));
			}
		}else{
			for(int i=0; i<vp.size(); i++){
				//System.out.printf("%.5f\n", vp.get(i).val);
				if(vp.get(i).val<-300){
					continue;
					//break;
				}
				valid.add(new Pair(vp.get(i).term, vp.get(i).val, vp.get(i).index));
			}
		}
		return valid;
	}
	
}
