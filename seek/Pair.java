package seek;
import java.io.*;
import java.util.*;


public class Pair implements Comparable<Pair>{
	String term;
	double val;
	int index;

	public Pair(String t, double d, int index){
		term = new String(t);
		val = d;
		this.index = index;
	}

	public int compareTo(Pair p){
		if(this.val < p.val){
			return -1;
		}else if(this.val==p.val){
			return this.term.compareTo(p.term);
		}else{
			return 1;
		}
	}

	public boolean equals(Object o){
		if(this==o) return true;
		if(!(o instanceof Pair)) return false;
		Pair p = (Pair) o;
		if(p.term.equals(this.term) && p.val==this.val){
			return true;
		}else{
			return false;
		}
	}
}
