package seek;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

public class GetExportFile extends HttpServlet {

  public void doGet( HttpServletRequest req, HttpServletResponse res ) throws ServletException, IOException {
    File tempDir = (File) getServletContext().
        getAttribute( "javax.servlet.context.tempdir" );

	String organism = req.getParameter("organism");
	String tempFile = tempDir + "/" + organism + "/" + req.getParameter("file");
	BufferedReader in = new BufferedReader(new FileReader(tempFile));
	String ss = null;
	Vector<String> vs = new Vector<String>();
	while((ss=in.readLine())!=null){
		vs.add(ss);
	}
	in.close();

	res.setContentType("text/plain");
	res.setCharacterEncoding("UTF-8");
	res.getWriter().write(StringUtils.join(vs, "\n"));	
  }

}
