package seek;
import seek.TreeNode;
import java.util.LinkedList;
import java.util.Vector;
import java.util.Map;
import java.util.HashMap;

public class Cluster{

	float[][] d;
	int nelements;

	//temp variables
	int closest_pair_i;
	int closest_pair_j;
	float closest_distance;

	public Cluster(float[][] d){
		this.d = d;
		this.nelements = d.length;
		closest_pair_i = 0;
		closest_pair_j = 0;
		closest_distance = 0;
	}

	private void find_closest_pair(int n){
		int i, j;
		float temp;
		float distance = d[1][0];
		int ip = 1;
		int jp = 0;
		for(i=1; i<n; i++){
			for(j=0; j<i; j++){
				temp = d[i][j];
				if(temp<distance){
					distance = temp;
					ip = i;
					jp = j;
				}
			}
		}
		closest_pair_i = ip;
		closest_pair_j = jp;
		closest_distance = distance;
	}

	private float max(float f1, float f2){
		if(f1>f2) return f1;
		else return f2;
	}

	public TreeNode[] HClusterMax(){
		int j, n;
		int[] clusterid = new int[nelements];
		TreeNode[] result = new TreeNode[nelements-1];

		for(j=0; j<nelements-1; j++){
			result[j] = new TreeNode();
		}
		for(j=0; j<nelements; j++) clusterid[j] = j;

		int js, is;
		for(n=nelements; n>1; n--){
			is = 1;
			js = 0;
			find_closest_pair(n);
			result[nelements-n].distance = closest_distance;
			is = closest_pair_i;
			js = closest_pair_j;

			for(j=0; j<js; j++)
				d[js][j] = max(d[is][j], d[js][j]);
			for(j=js+1; j<is; j++)
				d[j][js] = max(d[is][j], d[j][js]);
			for(j=is+1; j<n; j++)
				d[j][js] = max(d[j][is], d[j][js]);
			for(j=0; j<is; j++)
				d[is][j] = d[n-1][j];
			for(j=is+1; j<n-1; j++)
				d[j][is] = d[n-1][j];
			result[nelements-n].left = clusterid[is];
			result[nelements-n].right = clusterid[js];
			clusterid[js] = n-nelements-1;
			clusterid[is] = clusterid[n-1];
		}
		return result;
	}

	public TreeNode[] HClusterAvg(){
		int j, n;
		int[] clusterid = new int[nelements];
		int[] number = new int[nelements];
		TreeNode[] result = new TreeNode[nelements-1];

		for(j=0; j<nelements-1; j++){
			result[j] = new TreeNode();
		}

		for(j=0; j<nelements; j++){
			clusterid[j] = j;
			number[j] = 1;
		}

		int js, is;
		for(n=nelements; n>1; n--){
			is = 1;
			js = 0;
			find_closest_pair(n);
			result[nelements-n].distance = closest_distance;
			is = closest_pair_i;
			js = closest_pair_j;
			result[nelements-n].left = clusterid[is];
			result[nelements-n].right = clusterid[js];

			int sum = number[is] + number[js];
			
			for(j=0; j<js; j++){
				d[js][j] = d[is][j]*number[is] + d[js][j]*number[js];
				d[js][j] /= sum;
			}

			for(j=js+1; j<is; j++){
				d[j][js] = d[is][j] * number[is] + d[j][js] * number[js];
				d[j][js] /= sum;
			}

			for(j=is+1; j<n; j++){
				d[j][js] = d[j][is] * number[is] + d[j][js]*number[js];
				d[j][js] /= sum;
			}

			for(j=0; j<is; j++)
				d[is][j] = d[n-1][j];

			for(j=is+1; j<n-1; j++)
				d[j][is] = d[n-1][j];

			clusterid[js] = n-nelements-1;
			clusterid[is] = clusterid[n-1];
		}

		return result;
	}

	public Map<Integer, Float> GetDepth(TreeNode[] t){
		LinkedList<Integer> li = new LinkedList<Integer>();
		li.addFirst(t.length-1);
		Map<Integer, Float> depth = new HashMap<Integer, Float>();
		depth.put(t.length*-1, 0f);
		depth = DepthRecursive(li, depth, t);
		return depth;
	}

	public Map<Integer,Float> DepthRecursive(LinkedList<Integer> li, 
		Map<Integer,Float> depth, TreeNode[] t){

		if(li.isEmpty()) return depth;
		int i = li.removeFirst();
		float d = depth.get((i+1)*-1);

		TreeNode tx = t[i];		
		int left = tx.left;
		if(left>=0){
			depth.put(left, d + tx.distance);
		}else{
			li.addFirst((left+1)*-1);
			depth.put(left, d + tx.distance);
			depth = DepthRecursive(li, depth, t);
		}
		int right = tx.right;
		if(right>=0){
			depth.put(right, d + tx.distance);
		}else{
			li.addFirst((right+1)*-1);
			depth.put(right, d + tx.distance);
			depth = DepthRecursive(li, depth, t);
		}
		return depth;
	}

	public Vector<Integer> Linearize(TreeNode[] t){
		LinkedList<Integer> li = new LinkedList<Integer>();
		li.addFirst(t.length-1);
		Vector<Integer> vi = new Vector<Integer>();
		vi = LinearizeRecursive(li, vi, t);
		return vi;
	}

	public Vector<Integer> LinearizeRecursive(LinkedList<Integer> li, Vector<Integer> r, TreeNode[] t){
		if(li.isEmpty()){
			return r;
		}
		int i = li.removeFirst();
		TreeNode tx = t[i];
		int left = tx.left;
		if(left>=0){
			r.add(left);
		}else{
			li.addFirst((left+1)*-1);
			r = LinearizeRecursive(li, r, t);
		}
		int right = tx.right;
		if(right>=0){
			r.add(right);
		}else{
			li.addFirst((right+1)*-1);
			r = LinearizeRecursive(li, r, t);
		}
		return r;
	}
}
