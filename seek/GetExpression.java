package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import org.apache.commons.lang3.StringUtils;
import seek.ReadPacket;
import seek.Network;
import seek.SeekFile;

//Gets Expression and Coexpression Matrices from PCLServer
public class GetExpression {

	boolean enableQuery;
	boolean normalize;

	Vector<String> dset;
	Vector<String> gene;
	Vector<String> query;

	float[][][] qmat;
	float[][][] gmat;
	float[][] comat;
	float[][] qcomat;

	float rbp_p;

	public GetExpression(boolean e, boolean n, Vector<String> d,
	Vector<String> g, Vector<String> q){
		this(e, n, d, g, q, 0.99f);
	}

	public GetExpression(boolean e, boolean n, Vector<String> d, 
	Vector<String> g, Vector<String> q, float r){
		enableQuery = e;
		normalize = n;
		dset = d;
		gene = g;
		query = q;
		rbp_p = r;
		qmat = null;
		gmat = null;
		comat = null;
		qcomat = null;
	}
		

	public float[][][] GetQueryExpression(){
		return qmat;
	}

	public float[][][] GetGeneExpression(){
		return gmat;
	}

	public float[][] GetGeneCoexpression(){
		return comat;
	}

	public float[][] GetQueryCoexpression(){
		return qcomat;
	}

	//Used by MicroarrayImage
	public void PerformQueryExpression(int port) throws IOException{

        byte[] dByte = Network.getStringByte(StringUtils.join(dset, "\t"));
        byte[] gByte = Network.getStringByte(StringUtils.join(gene, "\t"));
		byte[] modeByte = null;
 
		int num_datasets = dset.size();
		int num_genes = gene.size();

		if(normalize)
			modeByte = Network.getStringByte("01100");
		else
			modeByte = Network.getStringByte("00100");

    	Socket kkSocket = new Socket("localhost", port);

    	kkSocket.getOutputStream().write(modeByte, 0, modeByte.length);
    	kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    	kkSocket.getOutputStream().write(dByte, 0, dByte.length);
    		
		ReadPacket st = Network.readSize(kkSocket);
		st = Network.readContent(kkSocket, st);
		int[] datasetSize = new int[num_datasets];
		for(int i = 0; i<num_datasets; i++)
			datasetSize[i] = st.vecFloat.get(i).intValue();

		st = Network.readSize(kkSocket);
		st = Network.readContent(kkSocket, st);
		Vector<Float> geneExpr = st.vecFloat;
		
		gmat = new float[num_datasets][][];
		for(int i=0; i<num_datasets; i++){
			gmat[i] = new float[num_genes][];
   			for(int k=0; k<num_genes; k++)
				gmat[i][k] = new float[datasetSize[i]];
		}

		int kk = 0;
		for(int i=0; i<num_datasets; i++){
    		for(int g=0; g<num_genes; g++){
    			for(int k=0; k<datasetSize[i]; k++){
					float v = geneExpr.get(kk);
					if(v==-9999 || Float.isNaN(v) || Float.isInfinite(v)) 
						v = 327.67f;
					gmat[i][g][k] = v;
					kk++;
    			}
    		}
		}
	}


	public void PerformQueryCoexpression(String dweight_comp_file,
	String query_part_file, String dset_map_file, int port) throws IOException{

		byte[] dByte = Network.getStringByte(StringUtils.join(dset, "\t"));
		byte[] gByte = Network.getStringByte(StringUtils.join(gene, "\t"));
		byte[] qByte = Network.getStringByte(StringUtils.join(query, "\t"));

		byte[] rbpByte = Network.getStringByte(Float.toString(rbp_p));

		//[coexpression?, normalize?, gene-expresison?, query-expression?, query-coexpression?]
		byte[] modeByte = Network.getStringByte("11000"); //11 means coexpression=true, normalized=true

		int[] datasetSize; //don't need
		ReadPacket st = null;
		int num_datasets = dset.size();
		int num_genes = gene.size();
		int num_query = query.size();
		Socket kkSocket = null;

		try{
			kkSocket = new Socket("localhost", port);
			kkSocket.getOutputStream().write(modeByte, 0, modeByte.length);
			kkSocket.getOutputStream().write(qByte, 0, qByte.length);
			kkSocket.getOutputStream().write(gByte, 0, gByte.length);
			kkSocket.getOutputStream().write(dByte, 0, dByte.length);
			kkSocket.getOutputStream().write(rbpByte, 0, rbpByte.length);
    		
			//dataset size - don't need
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			datasetSize = new int[num_datasets];
			for(int i = 0; i<num_datasets; i++){
				datasetSize[i] = st.vecFloat.get(i).intValue();
			}
							
			//coexpression
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			Vector<Float> coExpr = st.vecFloat;

			/*st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			Vector<Float> queryCoExpr = st.vecFloat;
			*/

			comat = new float[num_datasets][num_genes];	
			int kk = 0;
			for(int i=0; i<num_datasets; i++){
    			for(int g=0; g<num_genes; g++){
					float v = coExpr.get(kk);
					if(Float.isNaN(v) || Float.isInfinite(v)){
						v = -9999f;
					}
    				comat[i][g] = v;
					kk++;
    			}
    		}

			float[] qc = ReadScore.ReadScoreBinary(dweight_comp_file);
			Map<String,Integer> mm = ReadScore.readDatasetMapping(dset_map_file);
			Vector<Vector<String> > qp = ReadScore.readQueryParts(query_part_file);

			Map<String,Integer> mg = new HashMap<String,Integer>();
			for(int i=0; i<qp.size(); i++){
				for(String s : qp.get(i)){
					mg.put(s, i);
				}
			}

			qcomat = new float[num_datasets][num_query];
			kk = 0;
			for(int i=0; i<num_datasets; i++){
				//int d_id = mm.get(dset.get(i).substring(0, dset.get(i).indexOf(".pcl.bin")));
				int d_id = mm.get(dset.get(i));

    			for(int g=0; g<num_query; g++){
					int g_id = mg.get(query.get(g));
					float v = qc[d_id*num_query+g_id];
					if(v<0 || Float.isNaN(v) || Float.isInfinite(v)){ 
						v = -9999f;
					}else{
						v = v / num_query * 1000f;
					}
    				qcomat[i][g] = v;
					kk++;
    			}
    		}

		}catch(IOException e){
		}
	}


	//Used by GetExpressionServlet
	public void PerformQuery(int port) throws IOException{
		byte[] modeByte = null;
		byte[] qByte = null;
		byte[] dByte = null;
		byte[] gByte = null;
		byte[] rbpByte = null;	
	
		if(enableQuery){
			//calculating coexpression must involve query
			//[coexpression?, normalize?, gene-expresison?, query-expression?, query-coexpression?]
			if(normalize)
				modeByte = Network.getStringByte("11110");
			else
				modeByte = Network.getStringByte("10110");
			qByte = Network.getStringByte(StringUtils.join(query, "\t"));
			rbpByte = Network.getStringByte(Float.toString(rbp_p));
		}else{
			if(normalize)
				modeByte = Network.getStringByte("01100");
			else
				modeByte = Network.getStringByte("01100");	
		}

		dByte = Network.getStringByte(StringUtils.join(dset, "\t"));
		gByte = Network.getStringByte(StringUtils.join(gene, "\t"));

		int[] datasetSize;
		int num_datasets = dset.size();
		int num_genes = gene.size();
		int num_query = query.size();
		ReadPacket st = null;
    
		Socket kkSocket = null;   
		try{
			kkSocket = new Socket("localhost", port);
			kkSocket.getOutputStream().write(modeByte, 0, modeByte.length);
			if(enableQuery){	
				kkSocket.getOutputStream().write(qByte, 0, qByte.length);
	 		   	kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    			kkSocket.getOutputStream().write(dByte, 0, dByte.length);
    			kkSocket.getOutputStream().write(rbpByte, 0, rbpByte.length);
				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				
				datasetSize = new int[num_datasets];
				for(int i = 0; i<num_datasets; i++)
					datasetSize[i] = st.vecFloat.get(i).intValue();

				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				Vector<Float> geneExpr = st.vecFloat;

				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				Vector<Float> queryExpr = st.vecFloat;
	
				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				Vector<Float> coExpr = st.vecFloat;
	
				//st = Network.readSize(kkSocket);
				//st = Network.readContent(kkSocket, st);
				//Vector<Float> queryCoExpr = st.vecFloat; //not used yet!!

				qmat = new float[num_datasets][][];
				gmat = new float[num_datasets][][];
				comat = new float[num_datasets][num_genes];

				for(int i=0; i<num_datasets; i++){
					qmat[i] = new float[num_query][];
					gmat[i] = new float[num_genes][];
    				for(int k=0; k<num_genes; k++)
						gmat[i][k] = new float[datasetSize[i]];
					for(int k=0; k<num_query; k++)
						qmat[i][k] = new float[datasetSize[i]];
				}
    			
				int kk = 0;
				for(int i=0; i<num_datasets; i++){
    				for(int g=0; g<num_genes; g++){
    					for(int k=0; k<datasetSize[i]; k++){
							float v = geneExpr.get(kk);
							if(v==-9999 || Float.isNaN(v) || Float.isInfinite(v)) 
								v = 327.67f;
							gmat[i][g][k] = v;
							kk++;
    					}
    				}
				}

				kk = 0;
				for(int i=0; i<num_datasets; i++){
    				for(int g=0; g<num_query; g++){
    					for(int k=0; k<datasetSize[i]; k++){
							float v = queryExpr.get(kk);
							if(v==-9999 || Float.isNaN(v) || Float.isInfinite(v)) 
								v = 327.67f;
	    					qmat[i][g][k] = v;
							kk++;	
    					}
    				}
				}
			
				kk = 0;
				for(int i=0; i<num_datasets; i++){
    				for(int g=0; g<num_genes; g++){
						float v = coExpr.get(kk);
						if(v==-9999) 
							v = 327.67f;
    					comat[i][g] = v;
						kk++;
    				}
    			}

			}else{ //enableQuery == false //01100
	 		   	kkSocket.getOutputStream().write(gByte, 0, gByte.length);
    			kkSocket.getOutputStream().write(dByte, 0, dByte.length);
				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				
				datasetSize = new int[num_datasets];
				for(int i = 0; i<num_datasets; i++)
					datasetSize[i] = st.vecFloat.get(i).intValue();

				st = Network.readSize(kkSocket);
				st = Network.readContent(kkSocket, st);
				Vector<Float> geneExpr = st.vecFloat;

				//st = Network.readSize(kkSocket);
				//st = Network.readContent(kkSocket, st);
				//Vector<Float> queryCoExpr = st.vecFloat; //not used yet!!

				gmat = new float[num_datasets][][];

				for(int i=0; i<num_datasets; i++){
					gmat[i] = new float[num_genes][];
    				for(int k=0; k<num_genes; k++)
						gmat[i][k] = new float[datasetSize[i]];
				}
    			
				int kk = 0;
				for(int i=0; i<num_datasets; i++){
    				for(int g=0; g<num_genes; g++){
    					for(int k=0; k<datasetSize[i]; k++){
							float v = geneExpr.get(kk);
							if(v==-9999 || Float.isNaN(v) || Float.isInfinite(v)) 
								v = 327.67f;
							gmat[i][g][k] = v;
							kk++;
    					}
    				}
				}

				//kkSocket.getOutputStream().write(dByte, 0, gByte.length);
				//kkSocket.getOutputStream().write(gByte, 0, dByte.length);
				//throw new IOException("Unsupported operation!");
			}
    		
		}catch(IOException e){
		}


	}


}
