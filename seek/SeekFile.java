package seek;

import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.*;
import java.util.*;
import seek.ReadScore;
import seek.ReadDataset;

public class SeekFile{

	public String tempDirPath;
	public String sessionID;

	public String organism;
	public String gene_map_file;
	public String dataset_platform_file;
	public String guide_dataset_platform_file;

	public String gene_entrez_map_file;
	public String entrez_description_short_file;
	public String entrez_description_dir;
	public String dataset_description_dir;
	public String sample_description_dir;
	public String dataset_description_file;
	public String dataset_title_file;
	public String dataset_size_file;
	public String dataset_annotation_file;
	public String dataset_pubmed_file;

	public int seekserver_port;
	public int seekserver_guide_port;
	public int pclserver_port;
	public int seekpvalue_port;
	public int seekdatasetpvalue_port;

	public String leading_path;

	public Map<String, String> ent_hgnc;
	public Map<String, String> hgnc_ent;
	public Map<String, Integer> dset_int;
	public Map<Integer, String> int_dset;
	public Map<String, Vector<String> > gse_index;

	public Map<String, Integer> guide_dset_int;
	public Map<Integer, String> guide_int_dset;
	public Map<String, Vector<String> > guide_gse_index;

	public Map<String, Integer> ent_int;
	public Map<Integer, String> int_ent;

	public Map<String, String> enrichment;
	public Map<String, String> dataset_enrichment;

	public Map<String, Vector<String> > mapDataset; //Dset annotations by gse
	public Map<String, Vector<String> > mapEntity; //annotation by categories
	public Map<String, String> mapGSEDescription;

	public void ReadSettingFile(String setting_file) throws IOException{
		Map<String,String> s = new HashMap<String, String>();
		Scanner sc = null;
		try{
			sc = new Scanner(new BufferedReader(new FileReader(setting_file)));
			while(sc.hasNext()){
				String s1 = sc.nextLine();
				StringTokenizer st = new StringTokenizer(s1, "\t");
				String s2 = st.nextToken();
				String s3 = st.nextToken();
				s.put(s2, s3);
			}
		}finally{
			sc.close();
		}
		this.organism = s.get("ORGANISM");
		this.leading_path = tempDirPath + "/" + organism;
		if(s.get("LEADING_PATH")!=null){
			this.leading_path = s.get("LEADING_PATH");
		}
		System.out.println("Leading path is " + this.leading_path);
		this.entrez_description_short_file = this.leading_path + "/" + s.get("ENTREZ_DESCRIPTION_SHORT_FILE");
		this.entrez_description_dir = this.leading_path + "/" + s.get("ENTREZ_DESCRIPTION_DIR");
		this.dataset_pubmed_file = this.leading_path + "/" + s.get("DATASET_PUBMED_FILE");
		this.dataset_platform_file = this.leading_path + "/" + s.get("DATASET_PLATFORM_FILE");
		this.gene_map_file = this.leading_path + "/" + s.get("GENE_ID_FILE");
		this.gene_entrez_map_file = this.leading_path + "/" + s.get("GENE_ENTREZ_MAP_FILE");
		this.dataset_description_dir = this.leading_path + "/" + s.get("DATASET_DESCRIPTION_DIR");
		this.sample_description_dir = this.leading_path + "/" + s.get("SAMPLE_DESCRIPTION_DIR");
		this.dataset_description_file = this.leading_path + "/" + s.get("DATASET_DESCRIPTION_FILE");
		this.dataset_title_file = this.leading_path + "/" + s.get("DATASET_TITLE_FILE");
		this.dataset_size_file = this.leading_path + "/" + s.get("DATASET_SIZE_FILE");
		this.dataset_annotation_file = this.leading_path + "/" + s.get("DATASET_ANNOTATION_FILE");		
		this.seekserver_port = Integer.parseInt(s.get("SEEKSERVER_PORT"));
		this.pclserver_port = Integer.parseInt(s.get("PCLSERVER_PORT"));
		this.seekpvalue_port = Integer.parseInt(s.get("SEEKPVALUE_PORT"));
		this.seekdatasetpvalue_port = Integer.parseInt(s.get("SEEKDATASETPVALUE_PORT"));
		if(s.get("GUIDE_DATASET_PORT")!=null){
			this.seekserver_guide_port = Integer.parseInt(s.get("GUIDE_DATASET_PORT"));
		}
		if(s.get("GUIDE_DATASET_PLATFORM_FILE")!=null){
			this.guide_dataset_platform_file = this.leading_path + "/" + s.get("GUIDE_DATASET_PLATFORM_FILE");
		}

		int i=1;
		this.enrichment = new HashMap<String,String>();
		while(true){
			if(s.get("ENRICHMENT_" + i)==null) break;
			String en = s.get("ENRICHMENT_" + i);
			StringTokenizer st = new StringTokenizer(en, "|");
			String category = st.nextToken();
			String filename = st.nextToken();
			this.enrichment.put(category, filename);
			i++;
		}
		i = 1;
		this.dataset_enrichment = new HashMap<String,String>();
		while(true){
			if(s.get("DATASET_ENRICHMENT_" + i)==null) break;
			String en = s.get("DATASET_ENRICHMENT_"+i);
			StringTokenizer st = new StringTokenizer(en, "|");
			String category = st.nextToken();
			String filename = st.nextToken();
			this.dataset_enrichment.put(category, filename);
			i++;
		}
	}

	public String getPath(String fileName){
		return this.leading_path + "/" + fileName;
	}

	public SeekFile(String tempDirPath, String organism) throws IOException{
		this.tempDirPath = tempDirPath;
		sessionID = null;
		String setting_file = tempDirPath + "/" + organism + "/" + 
			"tomcat.path.setting";
		ReadSettingFile(setting_file);
		ent_hgnc = new HashMap<String, String>();
		hgnc_ent = new HashMap<String, String>();
		dset_int = new HashMap<String, Integer>();
		int_dset = new HashMap<Integer, String>();
		gse_index = new HashMap<String, Vector<String> >();

		guide_dset_int = new HashMap<String, Integer>();
		guide_int_dset = new HashMap<Integer, String>();
		guide_gse_index = new HashMap<String, Vector<String> >();

		ent_int = new HashMap<String, Integer>();
		int_ent = new HashMap<Integer, String>();
		mapDataset = new HashMap<String, Vector<String> >();
		mapEntity = new HashMap<String, Vector<String> >();
	}

	public void ReadGeneMap() throws IOException{
		ReadScore.readEntrezIntMap(this.gene_map_file,
			ent_int, int_ent);
		ReadScore.readGeneEntrezSymbolMapping(this.gene_entrez_map_file,
			ent_hgnc, hgnc_ent);
	}

	public void ReadDatasetMap() throws IOException{
		ReadScore.readDatasetPlatformMap(this.dataset_platform_file,
			dset_int, int_dset, gse_index);
	}

	public void ReadGuideDatasetMap() throws IOException{
		ReadScore.readDatasetPlatformMap(this.guide_dataset_platform_file,
			guide_dset_int, guide_int_dset, guide_gse_index);
	}

	public void ReadDatasetAnnotation() throws IOException{
		String dataset_annot = this.dataset_annotation_file;
		mapDataset = ReadDataset.readDatasetAnnotation(dataset_annot);
		mapEntity = ReadDataset.convertDatasetAnnotation(mapDataset);
	}

	public void ReadGSEDescription() throws IOException{
		String dm = this.dataset_description_file;
		mapGSEDescription = new HashMap<String, String>();
		Scanner sc = null;
		try{
			sc = new Scanner(new BufferedReader(new FileReader(dm)));
			while(sc.hasNext()){
				String s1 = sc.nextLine();
				StringTokenizer st = new StringTokenizer(s1, "\t");
				String s2 = st.nextToken();
				String s3 = st.nextToken();
				mapGSEDescription.put(s2, s3);
			}
		}finally{
			sc.close();
		}	
	}
}
