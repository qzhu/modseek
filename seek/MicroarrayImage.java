package seek;
import seek.TreeNode;
import seek.Cluster;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.ReadDataset;
import seek.Network;
import seek.ReadPacket;
import seek.SeekImage;
import seek.GetExpression;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarrayImage extends SeekImage{

	public class ExpressionMatrix{
		float[][][] mat;
		float[][][] gmat;
		public ExpressionMatrix(float[][][] mat, float[][][] gmat){
			this.mat = mat;
			this.gmat = gmat;
		}
	}

	BufferedImage image;
    BufferedImage header_image;
	BufferedImage dendrogram_image;

	String dsetTitleDir;
	String gene_order_file;

	Vector<String> dataset_annot;
	int dataset_start;

	boolean useReference;
	boolean sortSamples;

    float[][][] matrix; //used in init()
    float[][][] norm_matrix; //used in init()
	float[][][] tmp_norm_matrix; //used in init()
	int[][] sample_order;
	int[] gene_order;
	float[][] average_expr;

    int[][][] color_red;
    int[][][] color_green;
    int[][][] color_blue;

	boolean normalizeExpr;
	float upper_clip;
	float lower_clip;

	int column_header_height = 65;

	int dendro_height;
	float[] dendro_max_depth;
	TreeNode[][] dendro_tree; //datasets

	public MicroarrayImage(String tempDir, String organism, String colorChoice)
	throws IOException{
		super(tempDir, organism, colorChoice);
	}

    protected int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }

	public int[] GetDatasetSize(){
		return size;
	}

	public float GetLowerClip(){
		return lower_clip;
	}

	public float GetUpperClip(){
		return upper_clip;
	}
    
    protected int calculateUnitWidth(int s){
    	return horizontalUnitWidth;
    }

    protected void calculateSize(){
    	int x = 5;
    	int y = 0;
    	
    	for(int i=0; i<numDatasetToShow; i++)
    		x+=calculateWidth(size[i]) + horizontalDatasetSpacing;
    	x+=10;
    	
    	y += verticalUnitHeight*numGeneToShow;
    	windowWidth = x;
    	windowHeight = y;
    }
    
	public int[][] GetSampleOrder(){
		return sample_order;
	}

	public float[][] GetAverageExpression(){
		return average_expr; //in sample order
	}

    public void save(String name, String column_header_name){
    	File f = new File(name);
    	File f2 = new File(column_header_name);
    	try{
    		ImageIO.write(image, "png", f);
    		ImageIO.write(header_image, "png", f2);
    	}catch(IOException e){	
    	}
    }
   
	//Requires dset_order_file, and dsets
	public void WriteSampleOrder() throws IOException{
		PrintWriter out1 = new PrintWriter(new FileWriter(dset_order_file));
		int i, j;
		for(i=0; i<sample_order.length; i++){
			out1.print(dsets.get(i) + " ");
			for(j=0; j<sample_order[i].length; j++){
				if(j==sample_order[i].length-1)
					out1.print(sample_order[i][j]);
				else
					out1.print(sample_order[i][j] + " ");
			}
			out1.println();
		}
		out1.close();
		System.out.println("Finished writing sample order");
	}

	//Requires gene_order_file, and genes
	public void WriteGeneOrder() throws IOException{
		PrintWriter out1 = new PrintWriter(new FileWriter(gene_order_file));
		int i;
		for(i=0; i<gene_order.length; i++){
			if(i==gene_order.length-1){
				out1.print(gene_order[i]);
			}else{
				out1.print(gene_order[i] + " ");
			}
		}
		out1.println();
		out1.close();
		System.out.println("Finished writing gene order");
	}

	public void CalculateSampleOrder(float[][][] matrix){
		sample_order = new int[matrix.length][];
		average_expr = new float[matrix.length][];
		int i, j, k;
		for(i=0; i<matrix.length; i++){
			sample_order[i] = new int[matrix[i].length];
			average_expr[i] = new float[matrix[i].length];
		}

        tmp_norm_matrix = new float[matrix.length][][];
		//norm_matrix dimension: dataset, genes, samples        
        for(i=0; i<matrix.length; i++){ //dataset
        	tmp_norm_matrix[i] = new float[matrix[i].length][];
        	for(j=0; j<matrix[i].length; j++) 
        		tmp_norm_matrix[i][j] = new float[matrix[i][j].length];

        	for(j=0; j<matrix[i][0].length; j++){ //number of genes
        		float[] mm = new float[matrix[i].length];
        		for(k=0; k<matrix[i].length; k++){
        			mm[k] = matrix[i][k][j];
        		}
        		for(k=0; k<matrix[i].length; k++){
      		  		tmp_norm_matrix[i][k][j] = mm[k];
        		}
        	}
        }
			
		HierarchicalClusterSampleOrder(matrix); //outputs the sample_order

		//calculates average_expr
		for(i=0; i<matrix.length; i++){
			for(j=0; j<matrix[i].length; j++){
				float sum = 0;
				int num = 0;
				for(k=0; k<matrix[i][0].length; k++){
					if(tmp_norm_matrix[i][j][k]>327.f) continue;
					sum+=tmp_norm_matrix[i][j][k];
					num++;
				}
				average_expr[i][j] = (float) sum / (float) num;
			}
		}
	
		/*for(i=0; i<matrix.length; i++){
			Vector<Pair> np = new Vector<Pair>();
			int num = 0;
			for(j=0; j<matrix[i].length; j++){
				float sum = 0;
				num = 0;
				for(k=0; k<matrix[i][0].length; k++){
        			if(tmp_norm_matrix[i][j][k]>327.0f) continue;
					sum+=tmp_norm_matrix[i][j][k];
					num++;
				}
				Pair p = new Pair(Integer.toString(j), (double) sum, -1);
				np.add(p);
			}
			Collections.sort(np);
			for(j=0; j<np.size(); j++){
				sample_order[i][j] = Integer.parseInt(np.get(j).term);
				average_expr[i][j] = (float) np.get(j).val / (float) num;
			}
		}*/
		System.out.println("Finished calculating sample order");
	}

	public void CalculateGeneOrder(float[][][] gene_matrix){
		gene_order = new int[gene_matrix[0].length];
		if(gene_order.length==1){
			gene_order[0] = 0;
		}else{
			HierarchicalClusterGeneOrder(gene_matrix); //outputs gene_order
		}
		System.out.println("Finished calculating gene order");
	}

    //need norm_matrix, tmp_norm_matrix, gene_order
	public void SortGenesSamples(float[][][] matrix){
		int i, j, k;
		for(i=0; i<sample_order.length; i++){ //dataset
			for(j=0; j<matrix[i].length; j++){ //samples
				int trans = sample_order[i][j];
				for(k=0; k<matrix[i][0].length; k++){ //genes
					int g_trans = gene_order[k];
					norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][g_trans];
				}
			}
		}
	}

	public void SortSamples(float[][][] matrix){
		int i, j, k;
		for(i=0; i<sample_order.length; i++){ //dataset
			for(j=0; j<matrix[i].length; j++){ //samples
				int trans = sample_order[i][j]; 
				for(k=0; k<matrix[i][0].length; k++){ //genes
					norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][k];
				}
			}
		}
	}

	public float EuclideanDistance(float[] d1, float[] d2){
		float d = 0;
		for(int i=0; i<d1.length; i++){
			if(d1[i]>327.0f) continue;
			float f = (float) d1[i] - (float) d2[i];
			/*if(Float.isNaN(f) || Float.isInfinite(f)){
				continue;
			}*/
			d+=f*f;
		}

		float xd = (float) Math.sqrt(d);

		if(d==0){
			return 0f;
		}
		return xd;
	}

	public void HierarchicalClusterSampleOrder(float[][][] matrix){
		int i, j, k;
	
		dendro_max_depth = new float[matrix.length];
        size = new int[matrix.length];
		dendro_tree = new TreeNode[matrix.length][];
	
		for(i=0; i<matrix.length; i++){ //dataset
			float[][] distance = 
				new float[matrix[i].length][matrix[i].length];
			String[] names = new String[matrix[i].length];
			size[i] = matrix[i].length;

			for(j=0; j<matrix[i].length; j++){
				names[j] = Integer.toString(j);
				for(k=j+1; k<matrix[i].length; k++){
					float d = EuclideanDistance(matrix[i][j], matrix[i][k]);
					distance[j][k] = d;
					distance[k][j] = d;
				}
			}

			Cluster c = new Cluster(distance);
			TreeNode[] t = c.HClusterAvg();
			Vector<Integer> vi = c.Linearize(t);
			Map<Integer,Float> mm = c.GetDepth(t);

			dendro_tree[i] = t;
			Vector<Float> dist = new Vector<Float>();
			for(TreeNode tr : t){
				dist.add(tr.distance);
			}
			dendro_max_depth[i] = Collections.max(dist);

			for(j=0; j<matrix[i].length; j++){
				sample_order[i][j] = vi.get(j);
			}
		}
	}

	//assumes that gene_matrix has perfect dimension (same number of genes across datasets)
	 public void HierarchicalClusterGeneOrder(float[][][] gene_matrix){
        int i, j, k;

        int mat_genes = gene_matrix[0].length;
        System.out.println("Mat genes" + mat_genes);
        float[][] final_distance = new float[mat_genes][mat_genes];
        int[][] final_weight = new int[mat_genes][mat_genes];

        for(j=0; j<mat_genes; j++){ //genes
            for(k=0; k<mat_genes; k++){ //genes
                final_distance[j][k] = 0; //initialization
                final_weight[j][k] = 0;
            }
        }

        for(i=0; i<gene_matrix.length; i++){ //dataset
            int num_samples = gene_matrix[i][0].length;
            for(j=0; j<mat_genes; j++){
                if(gene_matrix[i][j][0]>327.0f){
                    continue;
                }
                for(k=j+1; k<mat_genes; k++){
                    if(gene_matrix[i][k][0]>327.0f){
                        continue;
                    }
					float d = EuclideanDistance(gene_matrix[i][j], gene_matrix[i][k]);
                    d = d * (float) num_samples;
                    final_distance[j][k] += d;
                    final_distance[k][j] += d;
                    final_weight[j][k] += num_samples;
                    final_weight[k][j] += num_samples;
                }
            }
        }

        for(j=0; j<mat_genes; j++){ //genes
            for(k=j+1; k<mat_genes; k++){ //genes
                final_distance[j][k] /= (float) final_weight[j][k];
                final_distance[k][j] /= (float) final_weight[k][j];
            }
        }

        Cluster c = new Cluster(final_distance);
        TreeNode[] t = c.HClusterAvg();
        Vector<Integer> vi = c.Linearize(t);
        Map<Integer,Float> mm = c.GetDepth(t);

        //float g_dendro_max_depth = 0;
		TreeNode[] g_dendro_tree = t;
		Vector<Float> dist = new Vector<Float>();
		for(TreeNode tr : t){
			dist.add(tr.distance);
		}
		//g_dendro_max_depth = Collections.max(dist);
		gene_order = new int[mat_genes];

		for(j=0; j<mat_genes; j++){
			gene_order[j] = vi.get(j);
		}
	}

	public float[] GetDendrogramMaxDepth(){
		return dendro_max_depth;
	}
 
    public void init(ExpressionMatrix em, boolean isQuery) throws IOException{
        matrix = em.mat;
        int i, j, k;
       
        color_red = new int[matrix.length][][];
        color_green = new int[matrix.length][][];
        color_blue = new int[matrix.length][][];
        tmp_norm_matrix = new float[matrix.length][][];
        norm_matrix = new float[matrix.length][][];
        size = new int[matrix.length];

		//norm_matrix dimension: dataset, genes, samples        
        for(i=0; i<matrix.length; i++){ //dataset
        	tmp_norm_matrix[i] = new float[matrix[i].length][];
        	norm_matrix[i] = new float[matrix[i].length][];
        	color_red[i] = new int[matrix[i].length][];
        	color_green[i] = new int[matrix[i].length][];
        	color_blue[i] = new int[matrix[i].length][];
        	size[i] = matrix[i].length;  //number of column
        	for(j=0; j<matrix[i].length; j++){ 
        		color_red[i][j] = new int[matrix[i][j].length];
        		color_green[i][j] = new int[matrix[i][j].length];
        		color_blue[i][j] = new int[matrix[i][j].length];
        		tmp_norm_matrix[i][j] = new float[matrix[i][j].length];
        		norm_matrix[i][j] = new float[matrix[i][j].length];
        	}

        	for(j=0; j<matrix[i][0].length; j++){ //number of genes
        		float[] mm = new float[size[i]];
        		for(k=0; k<size[i]; k++){
        			mm[k] = matrix[i][k][j];
        		}
        		for(k=0; k<size[i]; k++){
      		  		tmp_norm_matrix[i][k][j] = mm[k];
        		}
        	}
        }

		if(isQuery){
			gene_order = new int[matrix[0][0].length]; //number of query genes
			BufferedReader bb = new BufferedReader(new FileReader(gene_order_file));
			String s = bb.readLine();;
			StringTokenizer st = new StringTokenizer(s, " ");
			int ki = 0;
			while(st.hasMoreTokens()){
				int order = Integer.parseInt(st.nextToken());
				gene_order[ki] = order;
				ki++;
			}
			bb.close();
		}

		if(sortSamples){
			//sample_order is dset, sample_order dimension
			sample_order = new int[matrix.length][];
			for(i=0; i<matrix.length; i++){
				sample_order[i] = new int[matrix[i].length];
			}

			BufferedReader bb = new BufferedReader(new FileReader(dset_order_file));
			String s = null;
			int i_ind = 0;
			while((s=bb.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, " ");
				st.nextToken();
				int ki = 0;
				while(st.hasMoreTokens()){
					int order = Integer.parseInt(st.nextToken());
					sample_order[i_ind][ki] = order;
					ki++;
				}
				i_ind++;
			}
			bb.close();
			if(isQuery){
				SortGenesSamples(matrix);
			}else{
				SortSamples(matrix);
			}
		}
		else{
			sample_order = new int[matrix.length][];
			average_expr = new float[matrix.length][];
			for(i=0; i<matrix.length; i++){
				sample_order[i] = new int[matrix[i].length];
				average_expr[i] = new float[matrix[i].length];
			}
			for(i=0; i<matrix.length; i++){
				Vector<Pair> np = new Vector<Pair>();
				Vector<Pair> np2 = new Vector<Pair>();
				int num = 0;
				for(j=0; j<matrix[i].length; j++){
					float sum = 0;
					num = 0;
					for(k=0; k<matrix[i][0].length; k++){
        				if(tmp_norm_matrix[i][j][k]>327.0f) continue;
						sum+=tmp_norm_matrix[i][j][k];
						num++;
					}
					Pair p = new Pair(Integer.toString(j), (double) sum, -1);
					Pair p2 = new Pair(Integer.toString(j), (double) j, -1);
					np.add(p);
					np2.add(p2);
				}
				for(j=0; j<np.size(); j++){
					sample_order[i][j] = Integer.parseInt(np2.get(j).term);
					average_expr[i][j] = (float) np.get(j).val / (float) num;
				}
			}

			if(isQuery){
				SortGenesSamples(matrix);
			}else{
				SortSamples(matrix);
			}
		}


		int max_size = red.length;
		float unit_length = (upper_clip - lower_clip) / (float) max_size;

		float NaN = 9999;
		for(i=0; i<matrix.length; i++){
        	for(j=0; j<matrix[i].length; j++){
        		for(k=0; k<matrix[i][j].length; k++){        	
        			color_red[i][j][k] = neutral_red;
        			color_green[i][j][k] = neutral_green;
        			color_blue[i][j][k] = neutral_blue;

        			if(norm_matrix[i][j][k]>327.0f){
        				color_green[i][j][k] = neutral_red;
        				color_red[i][j][k] = neutral_green;
        				color_blue[i][j][k] = neutral_blue;
        				continue;
        			}

					if(norm_matrix[i][j][k]>upper_clip){
						color_red[i][j][k] = red[max_size-1];
						color_green[i][j][k] = green[max_size-1];
						color_blue[i][j][k] = blue[max_size-1];
						continue;
					}

					if(norm_matrix[i][j][k]<lower_clip){
						color_red[i][j][k] = red[0];
						color_green[i][j][k] = green[0];
						color_blue[i][j][k] = blue[0];
						continue;
					}
					
					int level = (int) Math.round((float)(1.0f*(norm_matrix[i][j][k] - lower_clip) / unit_length));

					if(level<0) level=0;
					else if(level>=max_size) level=max_size-1;

					color_red[i][j][k] = red[level];
					color_green[i][j][k] = green[level];
					color_blue[i][j][k] = blue[level];

        		}
        	}
        }
       
        names = new String[genes.size()];
        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }
       
		if(isQuery){
			String[] new_names = new String[genes.size()];
			Vector<String> new_genes = new Vector<String>();
			for(i=0; i<new_names.length; i++){
				new_names[i] = names[gene_order[i]];
				new_genes.add(genes.get(gene_order[i]));
			}
			names = new_names;
			genes = new_genes;
		}
 
        description = new String[numDatasetToShow];

		for(k=0; k<numDatasetToShow; k++){
			//String stem = getStem(dsets.get(k));
			String stem = ReadScore.getDatasetID(dsets.get(k));
			description[k] = map_datasets.get(stem);
		}

        calculateSize();
        image = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
        header_image = new BufferedImage(windowWidth, column_header_height, BufferedImage.TYPE_INT_RGB);
    }

	public void paint_dendrogram(String dendrogram_file){
		calculateSize();
		float temp_height = dendro_max_depth[0];
		for(int i=1; i < numDatasetToShow; i++){
			if(temp_height < dendro_max_depth[i]){
				temp_height = dendro_max_depth[i];
			}
		}
		dendro_height = (int) (3 * temp_height + 2); 
		dendrogram_image = new BufferedImage(windowWidth, dendro_height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g4 = dendrogram_image.createGraphics();
		g4.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		int x=5;
		int y=15;
		int i, j, k;

		int[] unitWidth = new int[size.length];
		for(i=0; i<size.length; i++){ //dataset
			unitWidth[i] = calculateUnitWidth(size[i]);
		}
	
		//Color dendro_color = new Color(168, 168, 168);//dark bg
		Color dendro_color = fg; //light bg	
		g4.setPaint(bg);
		g4.fill(new Rectangle2D.Double(0,0, windowWidth, dendro_height));
		g4.setPaint(dendro_color);
		x=5 + unitWidth[0] / 2;
		y=12;
		g4.setColor(dendro_color);
		for(k=0; k<dsets.size(); k++){
			int maxWidth = calculateWidth(size[k]);
			float maxDepth = dendro_max_depth[k];
			int[] reverse = new int[size[k]];
			for(i=0; i<size[k]; i++){
				reverse[sample_order[k][i]] = i;
			}

			int orig_x = x;
			Map<Integer,Float> coord_x = new HashMap<Integer,Float>();
			Map<Integer,Float> coord_y = new HashMap<Integer,Float>();
			Map<Integer,Integer> map_m = new HashMap<Integer,Integer>();
			for(i=0; i<dendro_tree[k].length; i++){
				map_m.put(dendro_tree[k][i].left, i);
				map_m.put(dendro_tree[k][i].right, i);
			}

			for(i=0; i<size[k]; i++){
				int a = sample_order[k][i];
				TreeNode tr = dendro_tree[k][map_m.get(a)];
				coord_x.put(a, (float) x);
				coord_y.put(a, (float) dendro_height);
				x+=unitWidth[k];
			}

			//x = orig_x;
			for(i=0; i<size[k]-1; i++){
				TreeNode tr = dendro_tree[k][i];
				int left = tr.left;
				int right = tr.right;
				float loc_x = (coord_x.get(left) + coord_x.get(right)) / 2.0f;
				float loc_y_left = coord_y.get(left);
				float loc_y_right = coord_y.get(right);
				g4.drawLine((int) coord_x.get(left).floatValue(), (int) loc_y_left, (int) coord_x.get(left).floatValue(), 
					(int) (dendro_height - tr.distance*3));
				g4.drawLine((int) coord_x.get(right).floatValue(), (int) loc_y_right, (int) coord_x.get(right).floatValue(), 
					(int) (dendro_height - tr.distance*3));
				
				float loc_y = dendro_height - tr.distance*3;
				g4.drawLine((int) (coord_x.get(left).floatValue()), (int) loc_y, (int) coord_x.get(right).floatValue(), (int) loc_y);
				coord_x.put((i+1)*-1, loc_x);
				coord_y.put((i+1)*-1, (dendro_height - tr.distance*3));
			}
				
			x+=horizontalDatasetSpacing;
		}

    	File f = new File(dendrogram_file);
    	try{
    		ImageIO.write(dendrogram_image, "png", f);
    	}catch(IOException e){
    	}

	}


    public void paint() {
        Graphics2D g2 = image.createGraphics();
        Graphics2D g3 = header_image.createGraphics();

		Color link_color = new Color(0,0,102); //blue on white, light background
		//Color link_color = new Color(255, 255, 200); //yellow on black, dark background

        g3.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);

        int x=5;
        int y=15;
        int i, j, k;

        int[] unitWidth = new int[size.length];
        for(i=0; i<size.length; i++){ //dataset
        	unitWidth[i] = calculateUnitWidth(size[i]);
        }

		//drawing header image=========================
        g3.setPaint(bg);
        g3.fill(new Rectangle2D.Double(0, 0, windowWidth, column_header_height));
        g3.setPaint(fg);
		Font font = g3.getFont();
		int fontSize = font.getSize();
		String fontName = "Arial";
		int fontStyle = font.getStyle();
		g3.setFont(new Font(fontName, fontStyle, fontSize-2));

		x = 5;
		y = 12;
		g3.setColor(Color.BLACK);
		for(k=0; k<dsets.size(); k++){
			int maxWidth = calculateWidth(size[k]);
			String[] tok = StringUtils.split(description[k], " ");
			String dsetWeight = String.format("%.2f", dataset_score.get(k)*1000.0);
			g3.setPaint(bg);
			g3.fill(new Rectangle2D.Double(x, 0, maxWidth, column_header_height));
			g3.setColor(fg);
			g3.drawString((dataset_start + k+1) + ". (" + dsetWeight + ")", x, y);
			String tmp = tok[0];
			int row = 2;
			g3.setColor(link_color);
			if(tok.length==1){
				int strWidth = g3.getFontMetrics().stringWidth(tmp);
				if(strWidth<=maxWidth){
					g3.drawString(tmp, x, y*row);
				}else{
					g3.drawString("...", x, y*row);
				}
			}

			for(i=1; i<tok.length; i++){
				int strWidth = g3.getFontMetrics().stringWidth(tmp + " " + tok[i]);
				if(i==tok.length-1){
					if(strWidth<=maxWidth){
						g3.drawString(tmp + " " + tok[i], x, y*row);
					}else{
						g3.drawString(tmp, x, y*row);
						row++;
						g3.drawString(tok[i], x, y*row);
					}
					break;
				}
				if(strWidth<=maxWidth){
					tmp += " " + tok[i];
					continue;
				}
				g3.drawString(tmp, x, y*row);
				row++;
				tmp = tok[i];
			}
			x += maxWidth + horizontalDatasetSpacing;
		}

		y = 0;

        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, windowWidth, windowHeight));
        g2.setPaint(fg);
        
		Color hatching_color = null;
		if(neutral_red>200 && neutral_green>200 && neutral_blue>200){
			hatching_color = new Color(36, 36, 36); //light bg
		}else{
			hatching_color = new Color(128, 128, 128); //dark bg
		}

        for(k=0; k<numGeneToShow; k++){
        	x = 5;
			System.out.println(names[k]);
        	for(i=0; i<size.length; i++){ //dataset
				int orig_x = x;
        		for(j=0; j<size[i]; j++){ //column in dataset
        			if(matrix[i][j][k]>327f){
        				g2.setPaint(new Color(neutral_red, neutral_green, neutral_blue));
        			}else{
        				g2.setPaint(new Color(
        					color_red[i][j][k], color_green[i][j][k], color_blue[i][j][k]));
        			}
        			g2.fill(new Rectangle2D.Double(x, y, unitWidth[i], 
        				verticalUnitHeight));
        			x+=unitWidth[i];
        		}
				if(matrix[i][0][k]>327f){
					g2.setPaint(hatching_color);
					int xt = orig_x;
					while(true){
						if(xt+verticalUnitHeight>orig_x+unitWidth[i]*size[i]) break;
						g2.drawLine(xt, y+verticalUnitHeight, xt+verticalUnitHeight, y);
						xt+=verticalUnitHeight/2;
					}
				}
        		x+=horizontalDatasetSpacing;
        	}
        	y+=verticalUnitHeight;
        }
    }


	public boolean readQuery(
		String query_path, String dataset_path, String pval_path,
		int gStart, int gEnd, int dStart, int dEnd,
		boolean filter_by_pval, float pvalCutoff, boolean negative_cor) 
		throws IOException{

		Map<String, Integer> mm = null;
		dataset_start = dStart;
		int numD = dEnd - dStart + 1;
		int numG = gEnd - gStart + 1;

		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();
		dataset_annot = new Vector<String>(); //seems to be doing nothing

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		Scanner ss = null;
		float[] sc_pval = null;
		try{
			//map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(this.sf.gene_entrez_map_file);
			name_mapping = this.sf.dataset_platform_file;
			mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc, false);
                        
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart+i).val;
				//System.out.println("QD" + i + " : " + fval);
				if(fval==0.0f) continue;
				dataset_annot.add(ReadScore.getDatasetID(vp.get(dStart+i).term));
				//dataset_annot.add(StringUtils.split(vp.get(dStart+i).term, "_")[0]);
				//dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dsets.add(vp.get(dStart + i).term);
				dataset_score.add(new Float(vp.get(dStart + i).val));
				//System.out.println("Dataset " + vp.get(dStart + i).term);
			}

			ss = new Scanner(new BufferedReader(new FileReader(query_path)));
			String s1 = ss.nextLine();
			StringTokenizer st = new StringTokenizer(s1);
			while(st.hasMoreTokens()){
				genes.add(st.nextToken());
			}
			ss.close();

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

		this.numDatasetToShow = dsets.size();
		this.numGeneToShow = genes.size();

		return true;    
	}   


	//for query image, set sortSamples = true, and useReference = false 
	//(which will generate a reference)
 	//for coexpressed image, set sortSamples = true, and useReference = true
	//(will use an existing reference)
	public boolean read(
	String query_path, String gene_path, String dataset_path, String pval_path,
	int gStart, int gEnd, int dStart, int dEnd,
	boolean filter_by_pval, float pvalCutoff, 
	Vector<String> onlyGenes,  //include only these genes (can be null, i.e. user specified genes) 
	boolean negative_cor) throws IOException{
    		
		Map<String, Integer> mm = null;
		int numD = dEnd - dStart + 1;
		int numG = gEnd - gStart + 1;

		dataset_start = dStart;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;
		Vector<String> query = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		pval_score = new Vector<Float>();

		dataset_annot = new Vector<String>();

		float[] sc_pval = null;
		float NA_VALUE = -320.0f;
		if(negative_cor){
			NA_VALUE = 320.0f;
		}

		try{
			//map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(this.sf.gene_entrez_map_file);
			name_mapping = this.sf.dataset_platform_file;
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc, false);
			
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart+i).val;
				//System.out.println("D" + i + " : " + fval);
				if(fval==0.0f) continue;
				//dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dsets.add(vp.get(dStart + i).term);
				dataset_score.add(new Float(vp.get(dStart + i).val));
				dataset_annot.add(ReadScore.getDatasetID(vp.get(dStart+i).term));
				//dataset_annot.add(StringUtils.split(vp.get(dStart+i).term, "_")[0]);
				//System.out.println("Dataset " + vp.get(dStart + i).term);
			}
			
			name_mapping = this.sf.gene_map_file;
    		mm = ReadScore.readGeneMapping(name_mapping);
			sc = ReadScore.ReadScoreBinaryWithQueryNull(gene_path, query_path, mm, NA_VALUE);

			//filter by pval
			if(filter_by_pval){
				File test_file = new File(pval_path);
				if(test_file.exists()){
					sc_pval = ReadScore.ReadScoreBinaryWithQueryNull(pval_path, query_path, mm, 0.99f);
				}
				if(sc_pval!=null){
					for(int i=0; i<sc.length; i++){
						if(sc_pval[i]>pvalCutoff){
							sc[i] = NA_VALUE;
						}
					}
				}
			}

			//filter by user-specified genes
			if(onlyGenes!=null && onlyGenes.size()>0){
				Set<Integer> include_id = new HashSet<Integer>();
				for(int i=0; i<onlyGenes.size(); i++){
					int internal_id = mm.get(onlyGenes.get(i));
					include_id.add(internal_id);
				}
				for(int i=0; i<sc.length; i++){
					if(!include_id.contains(i)){
						sc[i] = NA_VALUE;
					}
				}
			}

			vp = ReadScore.SortScores(mm, sc, negative_cor);

			for(int i=0; i<numG; i++){
				if(gStart+i>=vp.size()) break;
				genes.add(vp.get(gStart + i).term);
				gene_score.add(new Float(vp.get(gStart + i).val));
				if(filter_by_pval && sc_pval!=null){
					pval_score.add(new Float(sc_pval[vp.get(gStart+i).index]));
				}else{
					pval_score.add(0f);
				}
				System.out.println("Gene " + vp.get(gStart + i).term);
			}
		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}
    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = genes.size();
    	
		return true;    
    }
    
	public ExpressionMatrix Common(String sessionID, boolean isQuery,
	int gStart, int gEnd, int dStart, int dEnd,
	boolean filterByPValue, float PValue, Vector<String> onlyGenes, 
	boolean negative_cor) throws IOException{

    	Socket kkSocket = null;

		this.dset_order_file = this.sf.getPath(sessionID + "_dset_order");
		this.gene_order_file = this.sf.getPath(sessionID + "_gene_order");
		this.dsetTitleDir = this.sf.dataset_description_dir;

		if(isQuery){
			if(!this.readQuery(
				this.sf.getPath(sessionID+"_query"),
				this.sf.getPath(sessionID+"_dweight"),
				this.sf.getPath(sessionID+"_pval"),
				gStart, gEnd, dStart, dEnd,
				filterByPValue, PValue, negative_cor)){
				return null;
			}
		}else{
			if(!this.read( 
				this.sf.getPath(sessionID+"_query"),
				this.sf.getPath(sessionID+"_gscore"),
				this.sf.getPath(sessionID+"_dweight"),
				this.sf.getPath(sessionID+"_pval"),
				gStart, gEnd, dStart, dEnd,
				filterByPValue, PValue, onlyGenes, negative_cor)){
				return null;
			}
		}

		GetExpression ge = new GetExpression(false, normalizeExpr, dsets, genes, null);
		ge.PerformQueryExpression(sf.pclserver_port);
		float[][][] gmat = ge.GetGeneExpression();
		System.out.println("Datasets " + dsets + " " + genes);
		System.out.println("Number of datasets " + gmat.length);

		int num_datasets = dsets.size();
		int num_genes = genes.size();
		float[][][] mat = new float[num_datasets][][];

    	for(int i=0; i<num_datasets; i++){	
    		mat[i] = new float[gmat[i][0].length][];
			System.out.println("Dataset " + i + " " + gmat[i][0].length);
    		for(int k=0; k<gmat[i][0].length; k++) //num arrays per dataset
    			mat[i][k] = new float[num_genes];
    		for(int j=0; j<gmat[i][0].length; j++){ //num arrays
				for(int k=0; k<gmat[i].length; k++){ //num genes
					mat[i][j][k] = gmat[i][k][j];
				}
			}
		}

		System.out.println("Finished " + num_datasets + " " + num_genes);

		return new ExpressionMatrix(mat, gmat);
	}		

	public boolean SampleOrder(String sessionID, String dendrogramFile,
	int gStart, int gEnd, int dStart, int dEnd,
	boolean isQuery,
	boolean normalizeExpr, 
	boolean filterByPValue, float PValue, 
	Vector<String> onlyGenes, boolean negative_cor) throws IOException{
	
		this.normalizeExpr = normalizeExpr;
		ExpressionMatrix exp_matrix = Common(sessionID, isQuery,
			gStart, gEnd, dStart, dEnd, filterByPValue, PValue, onlyGenes, negative_cor);
		CalculateSampleOrder(exp_matrix.mat);
		WriteSampleOrder();
		this.paint_dendrogram(dendrogramFile);
		return true;
	}

    public boolean LoadQuery(String sessionID, String imageFile, String headerImageFile,
	int gStart, int gEnd, int dStart, int dEnd,
	float lower_clip, float upper_clip,
	boolean normalizeExpr,
	boolean sortSamples,
	boolean filterByPValue, float PValue, boolean negative_cor) 
	throws IOException{
		
		this.sortSamples = sortSamples;
		this.lower_clip = lower_clip;
		this.upper_clip = upper_clip;  	
		this.normalizeExpr = normalizeExpr;

		ExpressionMatrix exp_matrix = Common(sessionID, true,
			gStart, gEnd, dStart, dEnd, filterByPValue, PValue, null, negative_cor);

		CalculateGeneOrder(exp_matrix.gmat);
		WriteGeneOrder();

    	this.init(exp_matrix, true);
    	this.paint();
    	this.save(imageFile, headerImageFile);
    		
		return true;
    }

    public boolean Load(String sessionID, 
	String imageFile, String headerImageFile,
	int gStart, int gEnd, int dStart, int dEnd,
	float lower_clip, float upper_clip,
	boolean normalizeExpr,
	boolean sortSamples,
	boolean filterByPValue, float PValue, 
	Vector<String> onlyGenes, boolean negative_cor) 
	throws IOException{
		
		this.sortSamples = sortSamples;
		this.lower_clip = lower_clip;
		this.upper_clip = upper_clip;  	
		this.normalizeExpr = normalizeExpr;

		ExpressionMatrix exp_matrix = Common(sessionID, false,
			gStart, gEnd, dStart, dEnd, filterByPValue, PValue, onlyGenes, negative_cor);

    	this.init(exp_matrix, false);
    	this.paint();
    	this.save(imageFile, headerImageFile);
    		
		return true;
    }
}
