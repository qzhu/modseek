package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import seek.Pair;
import seek.ReadScore;

public class GetPageLimit extends HttpServlet {

	public Map<String, Integer> ReadDatasetSize(String f) throws IOException{
		BufferedReader in = null;
		Map<String, Integer> mm = new HashMap<String, Integer>();

		try{
			in = new BufferedReader(new FileReader(f));
			String s = null;
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String t1 = st.nextToken();
				int id = Integer.parseInt(st.nextToken());
				mm.put(t1, id);
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error reading dataset size file");
		}
		return mm;
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res) throws
		ServletException, IOException {

		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		
		String sessionID = req.getParameter("sessionID");
		String mode = req.getParameter("mode");
		String organism = req.getParameter("organism");
		boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
		float pvalCutoff = Float.parseFloat(req.getParameter("pval_cutoff"));

		Vector<String> onlyGenes = null;
		String specifyGenes = null;
		if(req.getParameter("specify_genes")!=null){
			specifyGenes = java.net.URLDecoder.decode(
				req.getParameter("specify_genes"), "UTF-8");
			String[] xs = StringUtils.split(specifyGenes);
			if(xs.length>0){
				onlyGenes = new Vector<String>();
				for(String x : xs)
					onlyGenes.add(x);
			}
		}

		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);

		String ext = null;
		String name_mapping = null;
		String name_mapping_2 = null;
		Map<String, Integer> mm = null;
		float[] sc = null;
		float[] sc_pval = null;
		File tempFile = null;
		Vector<Pair> vp = null;
		Map<String, Integer> dataset_size = 
			ReadDatasetSize(sf.dataset_size_file);
		
		ext = "_dweight";
		name_mapping = sf.dataset_platform_file;
		mm = ReadScore.readDatasetMapping(name_mapping);
		tempFile = new File(sf.getPath(sessionID+ext));
		sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		boolean negative_cor = ReadScore.isNegativeCor(sf.leading_path, sessionID);
		float NA_VALUE = -320.0f;
		if(negative_cor){
			NA_VALUE = 320.0f;
		}

		vp = ReadScore.SortScores(mm, sc, false);
		Vector<Integer> dsetRange = new Vector<Integer>();
		if(mode.equals("coexpression")){
			int limit = 50;
			int totAvailable = 0;	
			for(int i=0; i<vp.size(); i++){
				if(vp.get(i).val==0f) break;
				totAvailable++;
			}
			int numDatasetPage = totAvailable / limit;
			if(totAvailable%limit>0)
				numDatasetPage++;
			for(int i=0; i<numDatasetPage; i++)
				if(i==numDatasetPage-1)
					if(totAvailable%limit>0) dsetRange.add(totAvailable%limit);
					else dsetRange.add(limit);
				else
					dsetRange.add(limit);	
		}else{
			int limit = 200;
			int runningTotal = 0;
			int num = 1;
			for(int i=0; i<vp.size(); i++){
				if(vp.get(i).val==0f){
					dsetRange.add(num);
					break;
				}
				int dSize = dataset_size.get(vp.get(i).term);
				if(Math.abs(runningTotal + dSize - limit) < 
				Math.abs(runningTotal - limit)){
					runningTotal += dSize;
					num++;
				}else{
					dsetRange.add(num);
					runningTotal = dSize;
					num=1;
				}
			}
		}

		ext = "_gscore";
		name_mapping = sf.gene_map_file;
		//name_mapping_2 = sf.gene_entrez_map_file;
		mm = ReadScore.readGeneMapping(name_mapping);
		tempFile = new File(sf.getPath(sessionID + ext));
		String query_path = sf.getPath(sessionID + "_query");
		sc = ReadScore.ReadScoreBinaryWithQueryNull(tempFile.getAbsolutePath(), query_path, mm, NA_VALUE);
		String pval_path = sf.getPath(sessionID + "_pval");

		File test_file = new File(pval_path);
		if(test_file.exists()){
			sc_pval = ReadScore.ReadScoreBinaryWithQueryNull(pval_path, query_path, mm, 0.99f);
		}

		//filter by pval
		if(filter_by_pval && sc_pval!=null){
			for(int i=0; i<sc.length; i++){
				if(sc_pval[i]>pvalCutoff){
					sc[i] = -320.0f;
				}
			}
		}

		Vector<String> absentGenes = new Vector<String>();
		//filter by user-specified genes
		if(onlyGenes!=null && onlyGenes.size()>0){
			Set<Integer> include_id = new HashSet<Integer>();
			for(int i=0; i<onlyGenes.size(); i++){
				if(mm.containsKey(onlyGenes.get(i))){
					int internal_id = mm.get(onlyGenes.get(i));
					include_id.add(internal_id);
				}else{
					absentGenes.add(onlyGenes.get(i));
				}
			}
			for(int i=0; i<sc.length; i++){
				if(!include_id.contains(i)){
					sc[i] = -320.0f;
				}
			}
		}

		if(absentGenes.size()>0){
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write("Error: specified genes absent, please remove: " + 
				StringUtils.join(absentGenes, " "));
			return ;
		}
			

		vp = ReadScore.SortScores(mm, sc, negative_cor);

		Vector<Integer> geneRange = new Vector<Integer>();
		int limit = 100;
		int totAvailable = 0;	
		for(int i=0; i<vp.size(); i++){
			if(vp.get(i).val==0f) break;
			totAvailable++;
		}
		if(totAvailable<limit){
			limit = totAvailable;
		}
		if(totAvailable==0){
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			res.getWriter().write("Error: Empty!");
			return ;
		}

		int numGenePage = totAvailable / limit;
		if(totAvailable%limit>0)
			numGenePage++;

		for(int i=0; i<numGenePage; i++)
			if(i==numGenePage-1)
				if(totAvailable%limit>0) geneRange.add(totAvailable%limit);
				else geneRange.add(limit);	
			else
				geneRange.add(limit);

		String[] stbuilder = new String[2];
		Vector<String> dsetRangeStr = new Vector<String>();
		for(Integer i : dsetRange)
			dsetRangeStr.add(i.toString());
		
		stbuilder[0] = StringUtils.join(dsetRangeStr, " ");
		Vector<String> geneRangeStr = new Vector<String>();
		for(Integer i : geneRange)
			geneRangeStr.add(i.toString());
		
		stbuilder[1] = StringUtils.join(geneRangeStr, " ");
		String responseString = StringUtils.join(stbuilder,  ";");

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write(responseString);

	
	}

}
