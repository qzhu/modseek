package seek;
import java.util.*;
import java.net.*;
import java.io.*;
import java.nio.*;
import seek.ReadPacket;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class Network{
	
	public static byte[] intToByteArray(int value) {
    	ByteBuffer buf = ByteBuffer.allocate(10);
    	buf.order(ByteOrder.LITTLE_ENDIAN);
    	buf.putInt(value);
    	return new byte[] {
    		buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
    }
 
	public static byte[] floatToByteArray(float value){
		ByteBuffer buf = ByteBuffer.allocate(10);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		buf.putFloat(value);
		return new byte[] {
			buf.get(0), buf.get(1), buf.get(2), buf.get(3) };
	}

	//CAN BE USED FOR CONVERTING GENES, DSETS, OR OUTPUT_DIR TO BYTE FORM
	//gene must be in entrez gene id
	//dset must be in format of first column of the dataset.platform.jul7 
	//space delimited for genes in a query
	//"|" delimited between queries
	public static byte[] getStringByte(String str) throws IOException{
    	int i, j, k;
		int length = str.length() + 1;
    	byte[] b = new byte[length + 4]; //string (length) and 4 for size    	
    	byte[] ii = intToByteArray(length);
    	k = 0;
    	for(i=0; i<4; i++){
    		b[k] = ii[i];
    		k++;
    	}
		for(i=0; i<str.length(); i++){
			b[k] = (byte) str.charAt(i);
			k++;
		}
		b[k] = (byte) '\0'; //terminating character at 4+strDset.length() position
		return b;
    }

	public static byte[] getStringByte(Vector<Float> vf) throws IOException{
		int i, j, k;
		int length = vf.size();
		byte[] b = new byte[4*length + 4];
		byte[] ii = intToByteArray(length);
		k = 0;
		for(i=0; i<4; i++){
			b[k] = ii[i];
			k++;
		}
		for(i=0; i<length; i++){
			byte[] ba = floatToByteArray(vf.get(i));
			for(j=0; j<4; j++){
				b[k] = ba[j];
				k++;
			}
		}
		return b;
	}


	public static ReadPacket readSize(Socket kkSocket) throws IOException{
		ReadPacket r = new ReadPacket();
		try{
			byte[] val = null;
			ByteBuffer buf = null;
			val = new byte[8];
			int tmp_size = 8;
			int receive_size = -1;
			int index = 0;
			while(true){
				receive_size = kkSocket.getInputStream().read(val, index, tmp_size);
				if(receive_size==tmp_size) break;
				else if(receive_size==-1){
					System.err.println("server error");
					break;
				}
				tmp_size -= receive_size;
				index+=receive_size;
			}
				
			buf = ByteBuffer.wrap(val);
			buf.order(ByteOrder.LITTLE_ENDIAN);

			r.element_size = (int) ((long) buf.getInt() & 0xffffffffL);
			r.num_element = (int) ((long) buf.getInt() & 0xffffffffL);
			r.isString = false;
			//System.out.println(r.element_size + " " + r.num_element);

			if(r.element_size==1){
				r.isString = true;
			}else{
				r.isString = false;
			}
		}catch(IOException e){
			System.out.println("Bad IO readSize");
		}
		return r;
	}

	public static ReadPacket readContent(Socket kkSocket, ReadPacket rp) 
		throws IOException{

		try{
			byte[] val = null;
			ByteBuffer buf = null;
			val = new byte[rp.num_element*rp.element_size];
			int tmp_size = val.length;
			int receive_size = -1;
			int index = 0;
			while(true){
				receive_size = kkSocket.getInputStream().read(val, index, tmp_size);
				if(receive_size==tmp_size) break;
				else if(receive_size==-1){
					System.err.println("server error");
					break;
				}
				tmp_size -= receive_size;
				index+=receive_size;
			}

			buf = ByteBuffer.wrap(val);
			buf.order(ByteOrder.LITTLE_ENDIAN);

			if(rp.isString){
				int length = val.length;
				if((char) val[length-1]=='\0'){
					length--;
				}
				char[] ca = new char[length];
				for(int i=0; i<length; i++)
					ca[i] = (char) val[i];
				rp.strVal = new String(ca);
			}else{
				Vector<Float> fl = new Vector<Float>();
				for(int i=0; i<rp.num_element; i++){
					fl.add(buf.getFloat());
				}
				rp.vecFloat = fl;
			}
			rp.originalByte = val;
		}catch(IOException e){
			System.out.println("Bad IO readContent");
		}

		return rp;
	}
}

