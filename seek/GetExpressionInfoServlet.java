package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import seek.ReadPacket;
import seek.Network;
import seek.GetExpression;
import seek.SeekFile;
import seek.Cluster;

public class GetExpressionInfoServlet extends HttpServlet {

	public class ArrayCluster{
		int num_arrays;
		int num_genes;
		float dendro_max_depth = 0;
		TreeNode[] dendro_tree;
		int[] sample_order;
		public ArrayCluster(int na, int ng, float dmd, TreeNode[] dt, int[] so){
			num_arrays = na;
			num_genes = ng;
			dendro_max_depth = dmd;
			dendro_tree = dt;
			sample_order = so;
		}
	}

	public class GeneCluster{
		int[] gene_order;
		TreeNode[] dendro_tree;
		int num_arrays;
		int num_genes;
		float dendro_max_depth = 0;
		public GeneCluster(int na, int ng, float dmd, TreeNode[] dt, int[] go){
			num_arrays = na;
			num_genes = ng;
			dendro_max_depth = dmd;
			dendro_tree = dt;
			gene_order = go;
		}
	}

	public float EuclideanDistance(float[] d1, float[] d2){
		float d = 0;
		for(int i=0; i<d1.length; i++){
			if(d1[i]>327.0f) continue;
			float f = (float) d1[i] - (float) d2[i];
			d+=f*f;
		}
		float xd = (float) Math.sqrt(d);
		if(d==0) return 0f;
		return xd;
	}

	public ArrayCluster HierarchicalClusterSampleOrder(float[][] matrix){
		int i, j, k;
	
		int num_arrays = matrix.length;
		int num_genes = matrix[0].length;

		float dendro_max_depth = 0;
        int size = 0;
		TreeNode[] dendro_tree = null;
		int[] sample_order = new int[num_arrays];
	
		float[][] distance = new float[num_arrays][num_arrays];
		String[] names = new String[num_arrays];
		size = num_arrays;

		for(j=0; j<num_arrays; j++){
			names[j] = Integer.toString(j);
			for(k=j+1; k<num_arrays; k++){
				float d = EuclideanDistance(matrix[j], matrix[k]);
				distance[j][k] = d;
				distance[k][j] = d;
			}
		}

		Cluster c = new Cluster(distance);
		TreeNode[] t = c.HClusterAvg();
		Vector<Integer> vi = c.Linearize(t);
		Map<Integer,Float> mm = c.GetDepth(t);
		dendro_tree = t;
		Vector<Float> dist = new Vector<Float>();
		for(TreeNode tr : t){
			dist.add(tr.distance);
		}
		dendro_max_depth = Collections.max(dist);

		for(j=0; j<num_arrays; j++){
			sample_order[j] = vi.get(j);
		}
		return new ArrayCluster(num_arrays, num_genes, dendro_max_depth, dendro_tree, sample_order);
	}

	public GeneCluster HierarchicalClusterGeneOrder(float[][] matrix){
		int i, j, k;
	
		int num_genes = matrix.length;
		int num_arrays = matrix[0].length;

		float dendro_max_depth = 0;
        int size = 0;
		TreeNode[] dendro_tree = null;
		int[] gene_order = new int[num_genes];
	
		float[][] distance = new float[num_genes][num_genes];
		String[] names = new String[num_genes];
		size = num_genes;

		if(num_genes==1){
			gene_order[0] = 0;
			return new GeneCluster(num_arrays, num_genes, dendro_max_depth, dendro_tree, gene_order);
		}else{
			for(j=0; j<num_genes; j++){
				names[j] = Integer.toString(j);
				for(k=j+1; k<num_genes; k++){
					float d = EuclideanDistance(matrix[j], matrix[k]);
					distance[j][k] = d;
					distance[k][j] = d;
				}
			}
		}

		Cluster c = new Cluster(distance);
		TreeNode[] t = c.HClusterAvg();
		Vector<Integer> vi = c.Linearize(t);
		Map<Integer,Float> mm = c.GetDepth(t);
		dendro_tree = t;
		Vector<Float> dist = new Vector<Float>();
		for(TreeNode tr : t){
			dist.add(tr.distance);
		}
		dendro_max_depth = Collections.max(dist);

		for(j=0; j<num_genes; j++){
			gene_order[j] = vi.get(j);
		}
		return new GeneCluster(num_arrays, num_genes, dendro_max_depth, dendro_tree, gene_order);
	}

	public Map<String,String> ReadMap(String file) throws IOException{
		Map<String,String> m = new HashMap<String,String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String ss = null;
			while((ss=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(ss, "\t");
				String s1 = st.nextToken();
				String s2 = st.nextToken();
				m.put(s1, s2);
			}
			in.close();
		}catch(IOException e){
			System.out.println("BAD I/O!");
		}
		return m;
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException {

		boolean cluster = false; //if multiple datasets, cluster within each dataset
		if(req.getParameter("cluster")!=null && 
		req.getParameter("cluster").equals("1")){
			cluster = true;
		}

		boolean displaySampleOrder = false; //if multiple datasets, show sample order of dset1, then
		//sample order of dset2, etc
		if(req.getParameter("show_sample_order")!=null && 
		req.getParameter("show_sample_order").equals("1")){
			displaySampleOrder = true;
		}

		boolean displayGeneOrder = false; //if multiple datasets, show gene order of dset1, then gene order
		//of dset2, etc
		if(req.getParameter("show_gene_order")!=null && 
		req.getParameter("show_gene_order").equals("1")){
			displayGeneOrder = true;
		}

		boolean displaySampleDescription = false; //if multiple datasets, show sample description 
		//of dset1, then sample description of dset2, etc (in the same order specified by 
		//displaySampleOrder)
		if(req.getParameter("show_sample_description")!=null && 
		req.getParameter("show_sample_description").equals("1")){
			displaySampleDescription = true;
		}
		
		boolean normalize = false;
		if(req.getParameter("normalize").equals("1")){
			normalize = true;
		}

		String sessionID = req.getParameter("sessionID");
		File tempDir = (File) getServletContext().
			getAttribute( "javax.servlet.context.tempdir" );
		String organism = req.getParameter("organism");
		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);

		Vector<String> dset = new Vector<String>();
		Vector<String> gene = new Vector<String>();

		String[] dsetS;
		String[] geneS;

		dsetS = StringUtils.split(req.getParameter("dataset"), "+");
		geneS = StringUtils.split(req.getParameter("gene"), "+");

		for(int i=0; i<dsetS.length; i++){
			if(organism.equals("human")){
				dset.add(dsetS[i] + ".bin");
			}else{
				dset.add(dsetS[i]);
			}
		}
		for(int i=0; i<geneS.length; i++)
			gene.add(geneS[i]);

		System.out.println(dset + " " + gene);

		GetExpression ge = new GetExpression(false, normalize, dset, gene, 
		new Vector<String>(), 0.99f);
	
		try{
			ge.PerformQuery(sf.pclserver_port);	
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");

			float[][][] gmat = ge.GetGeneExpression();
			int num_genes = gmat[0].length;
			int num_datasets = gmat.length;
			//gmat: by dataset, then by gene, then by arrays
			int[][] sample_order = new int[num_datasets][];
			int[][] gene_order = new int[num_datasets][];
			int[] dset_size = new int[num_datasets];

			if(cluster){
				for(int i=0; i<num_datasets; i++){
					int num_arrays = gmat[i][0].length;
					float[][] cl = new float[num_arrays][num_genes];
					for(int k=0; k<num_arrays; k++){
						for(int j=0; j<num_genes; j++){
							cl[k][j] = gmat[i][j][k];
						}
					}
					ArrayCluster ac = HierarchicalClusterSampleOrder(cl);
					GeneCluster gc = HierarchicalClusterGeneOrder(gmat[i]);
					sample_order[i] = ac.sample_order;
					gene_order[i] = gc.gene_order;
					dset_size[i] = num_arrays;
				}
			}else{ //no clustering
				//do nothing
			}

			//Return string!!!
			Vector<String> returnStr = new Vector<String>();

			String dsetSizeStr = null;
			Vector<String> tmp = new Vector<String>();
			for(int i=0; i<dset.size(); i++){
				tmp.add(String.format("%d", dset_size[i]));
			}
			dsetSizeStr = StringUtils.join(tmp, "==");
			returnStr.add(dsetSizeStr); //first return string: dataset size for all datasets
			
			String sampleOrderStr = null;
			if(displaySampleOrder){
				Vector<String> mm = new Vector<String>();
				for(int i=0; i<dset.size(); i++){
					Vector<String> ll = new Vector<String>();
					for(int j=0; j<sample_order[i].length; j++){
						ll.add(String.format("%d", sample_order[i][j]));
					}
					mm.add(StringUtils.join(ll, ";;"));
				}
				sampleOrderStr = StringUtils.join(mm, "==");
				returnStr.add(sampleOrderStr); //2nd return str: sample order
			}

			String geneOrderStr = null;
			if(displayGeneOrder){
				Vector<String> mm = new Vector<String>();
				for(int i=0; i<dset.size(); i++){
					Vector<String> ll = new Vector<String>();
					for(int j=0; j<gene_order[i].length; j++){
						ll.add(String.format("%d", gene_order[i][j]));
					}
					mm.add(StringUtils.join(ll, ";;"));
				}
				geneOrderStr = StringUtils.join(mm, "==");
				returnStr.add(geneOrderStr); //2nd return str: gene order
			}
	
			String gsmStr = null;
			if(displaySampleDescription){
				Vector<String> mm = new Vector<String>();
				Map<String,String> dset_plat = ReadMap(sf.dataset_platform_file);
				String parsedGSM = sf.sample_description_dir;
				for(int i=0; i<dset.size(); i++){
					String s = dset.get(i);
					if(organism.equals("human")){
						int ii = s.indexOf(".bin");
						s = s.substring(0, ii);
					}
					String dset_platform = dset_plat.get(s);
					String dset_name = ReadScore.getDatasetName(s);
					String dsetPath = parsedGSM + "/" + dset_platform + "/" + dset_name;
					try{
						BufferedReader in = new BufferedReader(new FileReader(dsetPath));
						String ss = null;
						Vector<String> ll = new Vector<String>();
						while((ss=in.readLine())!=null){
							ll.add(ss);
						}
						in.close();
						mm.add(StringUtils.join(ll, ";;"));
					}catch(IOException e){
						mm.add("Array not found" + ";;" + "Array not found");
						System.out.println("BAD I/O");
					}
				}
				gsmStr = StringUtils.join(mm, "==");
				returnStr.add(gsmStr); //third return str: gsm description
			}

			String valStr = null;	
			Vector<String> gmat_str = new Vector<String>();
			//by gene, then by dataset, then by arrays
			for(int k=0; k<num_genes; k++){
				for(int j=0; j<num_datasets; j++){
					for(int i=0; i<gmat[j][k].length; i++){ //dataset size
						gmat_str.add(String.format("%.2f", gmat[j][k][i]));
					}
				}
			}
			valStr = StringUtils.join(gmat_str, ",");
			returnStr.add(valStr); //fourth return str: expression values

			String send_str = StringUtils.join(returnStr, "^^^");
			res.getWriter().write(send_str);
			

		}catch(IOException e){
		}

  	}

}
