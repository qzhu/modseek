package seek;

import java.io.*;
import java.util.*;

public class ReadDataset{

	public static Map<String, Vector<String> > convertDatasetAnnotation(
		Map<String, Vector<String> > mapDataset){

		Map<String, Vector<String> > m = 
			new HashMap<String, Vector<String> >();
		int i = 0;
		Set<String> datasets = mapDataset.keySet();
		for(String d : datasets){
			Vector<String> vs = mapDataset.get(d);
			for(String s : vs){
				if(m.containsKey(s)){
					Vector<String> annot = m.get(s);
					annot.add(d);
					m.put(s, annot);
				}
				else{
					Vector<String> annot = new Vector<String>();
					annot.add(d);
					m.put(s, annot);
				}
			}
		}
		return m;
	}

	public static Map<String, Vector<String> > readDatasetAnnotation(String file)
		throws IOException {

		Map<String, Vector<String> > m = new HashMap<String, Vector<String> >();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			String gse_id = "";
			while((s=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				gse_id = st.nextToken();
				String entity_name = st.nextToken();
				String entity_id = st.nextToken();
				String type = st.nextToken();
				Vector<String> vs;
				if(m.containsKey(gse_id)){
					vs = m.get(gse_id);
				}else{
					vs = new Vector<String>();
				}
				vs.add(entity_name);
				m.put(gse_id, vs);
			}
		}catch(IOException e){
			System.out.println("Error opening file");
		}
	
		return m;
	}

	public static String readDatasetTitle(String file)
		throws IOException {

		String s = null;
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			in.readLine();
			s = in.readLine();
		}catch(IOException e){
			System.out.println("Error opening file");
		}
	
		return s;
	}

	public static Map<String, Vector<String> > readDatasetPlatformMap(String file)
		throws IOException {
		
		Map<String, Vector<String> > m = new HashMap<String, Vector<String> >();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			int i = 0;
			String gse_id = "";
			while((s=in.readLine())!=null){
				int gid = 0;
				if(s.contains("_")){
					gid = s.indexOf("_");
				}else{
					gid = s.indexOf(".");
				}
				gse_id = s.substring(0, gid);

				Vector<String> vs;
				if(m.containsKey(gse_id)){
					vs = m.get(gse_id);
				}else{
					vs = new Vector<String>();
				}
				vs.add(s);
				m.put(gse_id, vs);
			}
		}catch(IOException e){
			System.out.println("Error opening file");
		}
	
		return m;
	}

	public static void WriteVector(Vector<String> vs, String file)
		throws IOException {
		PrintWriter out = new PrintWriter(new File(file));
		for(String s : vs){
			out.println(s);
		}
		out.close();
	}

}
