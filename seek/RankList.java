package seek;
import java.util.*;

public class RankList{
	Vector<String> vs;
	Map<String, Integer> rank;
	Set<String> vs_set;
	public RankList(Vector<String> vs){
		this.vs = new Vector<String>(vs);
		this.vs_set = new HashSet<String>(vs);
		this.rank = new HashMap<String,Integer>();
		for(int i=0; i<this.vs.size(); i++)
			this.rank.put(this.vs.get(i), i+1);
	}
	public RankList(){
		this.vs = new Vector<String>();
		this.vs_set = new HashSet<String>();
		this.rank = new HashMap<String,Integer>();
	}
	public void add(String s){
		this.vs.add(s);
		this.vs_set.add(s);
		this.rank.put(s, this.vs.size());
	}
	public String get(int i){
		return this.vs.get(i);
	}
	public int getRank(String s){
		return this.rank.get(s);
	}
	public boolean contains(String s){
		return this.vs_set.contains(s);
	}
	public int size(){
		return this.vs.size();
	}
	public Vector<String> list(){
		return this.vs;
	}
}
