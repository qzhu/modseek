package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import seek.*;

public class GetOrthologGroupServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
	
		String sessionID = java.net.URLDecoder.decode(req.getParameter("sessionID"), "UTF-8");
		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		String organism1 = java.net.URLDecoder.decode(req.getParameter("organism1"), "UTF-8");
		String organism2 = java.net.URLDecoder.decode(req.getParameter("organism2"), "UTF-8");
		SeekFile sf1 = new SeekFile(tempDir.getAbsolutePath(), organism1);
		SeekFile sf2 = new SeekFile(tempDir.getAbsolutePath(), organism2);

		sf1.ReadGeneMap();
	
		String[] queryS = StringUtils.split(
			java.net.URLDecoder.decode(req.getParameter("query"), "UTF-8"), " "); //in gene-symbol

		System.out.println("org1 is " + organism1);
		System.out.println("org2 is " + organism2);
		System.out.println("query[0] is " + queryS[0]);

		Map<String,Map<String,OrthologGroup> > m = OrthologReader.Prepare(tempDir.getAbsolutePath(), 
			organism1, organism2);	

		Map<String,OrthologGroup> ms = m.get(organism1 + "_" + organism2);
		
		Set<String> query = new HashSet<String>();
		for(String g : queryS){
			if(sf1.hgnc_ent.containsKey(g)){
				query.add(sf1.hgnc_ent.get(g));
			}
		}

		
		System.out.println("query is " + query);
		

		Set<String> query2 = new HashSet<String>(); //organism 2's query
		Set<String> reduced_query = new HashSet<String>(); //organism 1's reduced query (filtered to those in orthologgroups)

		Set<String> ortho_expand_query = new HashSet<String>(); //organism 1's ortho-expanded query (first filtered to those in ortholog groups, then add the rest of members in the ortholog group)

		for(String g : query){
			OrthologGroup ss = null;
			if((ss=ms.get(g))==null) continue;
			for(String sl : ss.getOrthologs(1)){
				query2.add(sl);
			}
			reduced_query.add(g);
			for(String sl : ss.getOrthologs(0)){
				ortho_expand_query.add(sl);
			}
		}
		System.out.println("query2 is " + query2);
		System.out.println("ortho_expand_query is " + ortho_expand_query);
	
		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");

		String return_str = StringUtils.join(reduced_query, ",") + "\n" + 
			StringUtils.join(ortho_expand_query, ",") + "\n" + 
			StringUtils.join(query2, ",");

		res.getWriter().write(return_str);


	}
}
