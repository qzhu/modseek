package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import seek.ReadScore;

public class GetQuery extends HttpServlet {

	public Vector<String> getQuery(SeekFile sf, String sessionID, boolean bEntrez)
	throws IOException{
		Vector<String> vs = new Vector<String>();
		String ext = "_query";
		Map<String, String> ent_symbol = new HashMap<String, String>();
		Map<String, String> symbol_ent = new HashMap<String, String>();
		ReadScore.readGeneEntrezSymbolMapping(sf.gene_entrez_map_file, ent_symbol, symbol_ent);
		File tempFile = new File(sf.getPath(sessionID + ext));
		try{
			BufferedReader in = new BufferedReader(new FileReader(tempFile));
			String s = in.readLine();
			StringTokenizer st = new StringTokenizer(s, " ");
			while(st.hasMoreTokens()){
				String x = st.nextToken();
				if(bEntrez){ //need entrez gene id
					vs.add(x);
				}else{ //just need gene symbol
					vs.add(ent_symbol.get(x));
				}
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + tempFile);
		}
		return vs;
	}

	public void doPost( HttpServletRequest req, HttpServletResponse res) throws
		ServletException, IOException {

		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		
		String sessionID = req.getParameter("sessionID");
		String organism = req.getParameter("organism");

		boolean bEntrez = false;
		String needEntrez = req.getParameter("entrez");
		if(needEntrez!=null){
			if(needEntrez.equals("1")){
				bEntrez = true;
			}else if(needEntrez.equals("0")){
				bEntrez = false;
			}else{
				System.out.println("Invalid string!");
			}
		}

		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);
		Vector<String> query_genes = getQuery(sf, sessionID, bEntrez);

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		res.getWriter().write(StringUtils.join(query_genes, " "));
	}

}
