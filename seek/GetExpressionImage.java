package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.awt.Color;
import seek.Pair;
import seek.ReadScore;
import seek.MicroarrayImage;

public class GetExpressionImage extends HttpServlet {
  public Vector<String> getStringVector(float[] f){
	Vector<String> vs = new Vector<String>();
	for(int i=0; i<f.length; i++){
		vs.add(Float.toString(f[i]));
	}
	return vs;
  }
  public Vector<String> getStringVector(Vector<Float> vf){
	Vector<String> vs = new Vector<String>();
	for(Float f : vf){
		vs.add(Float.toString(f));
	}
	return vs;
  }

  public Vector<String> getStringVector(int[] ii){
	Vector<String> vs = new Vector<String>();
	for(int i=0; i<ii.length; i++){
		vs.add(Integer.toString(ii[i]));
	}
	return vs;
  }

  public Vector<String> getStringVector(Color c){
	Vector<String> vs = new Vector<String>();
	vs.add(Integer.toString(c.getRed()));
	vs.add(Integer.toString(c.getGreen()));
	vs.add(Integer.toString(c.getBlue()));
	return vs;
  }

  public Vector<String> getStringVector(Color[] c){
	Vector<String> vs = new Vector<String>();
	for(Color x : c){
		vs.add(Integer.toString(x.getRed()));
		vs.add(Integer.toString(x.getGreen()));
		vs.add(Integer.toString(x.getBlue()));
	}
	return vs;
  }

	public Vector<String> doCoexpression(String sessionID, String dir, String organism, 
		String colorChoice,
		int gStart, int gEnd, int dStart, int dEnd,
		float up, float down, boolean filter_by_pval, float pvalCutoff, 
		float rbpP, boolean multiplyDatasetWeight, Vector<String> onlyGenes,
		boolean displayDatasetKeywords, boolean negative_cor) throws IOException{

		CoexpressionImage ci = new CoexpressionImage(dir, organism, colorChoice);

		String coexpressionFile = ci.sf.getPath(sessionID+"_coexpress_image.png");	
		String coexpressionQueryFile = ci.sf.getPath(sessionID+"_coexpress_query_image.png");	
		String coexpressionHeaderFile = ci.sf.getPath(sessionID+"_coexpress_header_image.png");	
		int imageWidth = 0;

		boolean normalize = true;

		Color[] gradient = null;	
		Color[] query_gradient = null;
		float query_upper_clip = -1;
		float query_lower_clip = -1;

		Color neutral = null;
		float[][] comat = null;
		int vertUnit = -1;
		try{
			ci.ReadMap(displayDatasetKeywords);
			ci.Load(sessionID,
				coexpressionFile, coexpressionQueryFile, coexpressionHeaderFile, 
				gStart, gEnd, dStart, dEnd,
				down, up, 
				filter_by_pval, pvalCutoff, rbpP, multiplyDatasetWeight, onlyGenes, negative_cor);
			imageWidth = ci.getWindowWidth();
			vertUnit = ci.GetVerticalUnitHeight();
			gradient = ci.GetColorGradient();
			query_gradient = ci.GetQueryColorGradient();
			query_upper_clip = ci.GetQueryUpperClip();
			query_lower_clip = ci.GetQueryLowerClip();
			neutral = ci.GetNeutralColor();
			comat = ci.GetMatrix();
		}catch(IOException e){
			System.out.println("Error producing image");
		}
		
		Vector<Float> dset_score = ci.getDatasetScore();
		Vector<Float> gene_score = ci.getGeneScore();	
		Vector<Float> pval_score = ci.getPValueScore();	

		//get dset names in GSEID
		//get gene names in entrezID
		Vector<String> dsets = ci.getDatasets();
		Vector<String> genes = ci.getGenes();
		Vector<String> query = ci.getQuery();
		String[] geneHGNC = ci.getGeneNames(); //gene name in gene symbol
		String[] queryHGNC = ci.getQueryNames();
		//special case: sizes[x] always equal to unitWidth
		int horizUnit=1;
		int horizSpacing=0;
		int[] sizes = ci.GetDatasetSize();

		Vector<String> str_comat = new Vector<String>();
		for(int i=0; i<dsets.size(); i++)
			str_comat.addAll(getStringVector(comat[i]));

		Vector<String> response = new Vector<String>();
		response.add("dset_score:" + StringUtils.join(getStringVector(dset_score), ","));
		response.add("gene_score:" + StringUtils.join(getStringVector(gene_score), ","));
		response.add("pval_score:" + StringUtils.join(getStringVector(pval_score), ","));
		response.add("dsets:" + StringUtils.join(dsets, ","));
		response.add("genes:" + StringUtils.join(genes, ","));
		response.add("query:" + StringUtils.join(query, ","));
		response.add("geneHGNC:" + StringUtils.join(geneHGNC, ","));
		response.add("queryHGNC:" + StringUtils.join(queryHGNC, ","));
		response.add("sizes:" + StringUtils.join(getStringVector(sizes), ","));
		response.add("vertUnit:" + Integer.toString(vertUnit));
		response.add("horizUnit:" + Integer.toString(horizUnit));
		response.add("horizSpacing:" + Integer.toString(horizSpacing));
		response.add("imageWidth:" + Integer.toString(imageWidth));
		response.add("neutralColor:" + StringUtils.join(getStringVector(neutral), ","));

		response.add("mainColors:" + StringUtils.join(getStringVector(gradient), ","));
		response.add("queryColors:" + StringUtils.join(getStringVector(query_gradient), ","));

		response.add("queryUpperClip:" + Float.toString(query_upper_clip));
		response.add("queryLowerClip:" + Float.toString(query_lower_clip));
		response.add("coexprMatrix:" + StringUtils.join(str_comat, ","));

		//no sample order, or average_expr
		return response;
	}


	//returns a vector of strings
	public Vector<String> doExpression(String sessionID, String dir, String organism, 
		String colorChoice,
		int gStart, int gEnd, int dStart, int dEnd,
		float up, float down, boolean normalize, boolean sortSample, boolean bClusterQuery, 
		boolean filter_by_pval, float pvalCutoff, Vector<String> onlyGenes, 
		boolean displayDatasetKeywords, boolean negativeCor) throws IOException{

		MicroarrayImage mi = new MicroarrayImage(dir, organism, colorChoice);
		String imageFile = mi.sf.getPath(sessionID+"_image.png");
		String headerFile = mi.sf.getPath(sessionID+"_header_image.png");
		String dendrogramFile = mi.sf.getPath(sessionID+"_dendrogram_image.png");
		String queryimageFile = mi.sf.getPath(sessionID+"_query_image.png");

		MicroarrayImage mi_query = new MicroarrayImage(dir, organism, colorChoice);
		String qFile = mi_query.sf.getPath(sessionID+"_query");

		Color[] gradient = null;	
		Color neutral = null;
		int imageWidth = 0;
		int vertUnit = 0;

		//TO BE PHASED OUT!!==========================
		float[][] average_expr = null; //1- dataset, 2- average expr of samples listed in sample order
		int[][] sample_order = null; //1- dataset, 2- sample order
		//============================================

		try{
			mi.ReadMap(displayDatasetKeywords);
			mi_query.ReadMap(displayDatasetKeywords);

			//Cluster by query or by coexpressed genes
			//boolean bClusterQuery = true;
			if(bClusterQuery){
				mi_query.SampleOrder(sessionID, dendrogramFile, 
					0, 50, dStart, dEnd,
					true, //is query or not
					normalize, //normalize expression or not
					filter_by_pval, pvalCutoff, onlyGenes, negativeCor);
				average_expr = mi_query.GetAverageExpression();
				sample_order = mi_query.GetSampleOrder();
			}else{
				mi.SampleOrder(sessionID, dendrogramFile,
					0, 50, dStart, dEnd,
					false, //is query or not
					normalize, //normalize expression or not
					filter_by_pval, pvalCutoff, onlyGenes, negativeCor);
				average_expr = mi.GetAverageExpression();
				sample_order = mi.GetSampleOrder();
			}

			mi.Load(sessionID, 
				imageFile, 
				headerFile,
				gStart, gEnd, dStart, dEnd,
				down, up,
				normalize, //normalize expression or not
				sortSample, //sort Samples 
				filter_by_pval, pvalCutoff, onlyGenes, negativeCor);

			imageWidth = mi.getWindowWidth();
			vertUnit = mi.GetVerticalUnitHeight();
			gradient = mi.GetColorGradient();
			neutral = mi.GetNeutralColor();

			mi_query.LoadQuery(sessionID,
				queryimageFile, 
				headerFile,
				gStart, gEnd, dStart, dEnd,
				down, up,
				normalize, //normalize expression or not
				sortSample, //sort Samples
				false, -1.0f, //disable pval filter
				negativeCor); //true: sort by negative correlations

		}catch(IOException e){
			System.out.println("Error producing image");
		}

		Vector<Float> dset_score = mi.getDatasetScore();
		Vector<Float> gene_score = mi.getGeneScore();	
		Vector<Float> pval_score = mi.getPValueScore();	
		//get dset names in GSEID
		//get gene names in entrezID
		Vector<String> dsets = mi.getDatasets();
		Vector<String> genes = mi.getGenes();
		Vector<String> query = mi_query.getGenes();
		String[] geneHGNC = mi.getGeneNames(); //gene name in gene symbol
		String[] queryHGNC = mi_query.getGeneNames();
		int horizUnit=mi.GetHorizontalUnitWidth();
		int horizSpacing=mi.GetHorizontalDatasetSpacing();

		int[] sizes = mi.GetDatasetSize();

		Vector<String> response = new Vector<String>();
		response.add("dset_score:" + StringUtils.join(getStringVector(dset_score), ","));
		response.add("gene_score:" + StringUtils.join(getStringVector(gene_score), ","));
		response.add("pval_score:" + StringUtils.join(getStringVector(pval_score), ","));
		response.add("dsets:" + StringUtils.join(dsets, ","));
		response.add("genes:" + StringUtils.join(genes, ","));
		response.add("query:" + StringUtils.join(query, ","));
		response.add("geneHGNC:" + StringUtils.join(geneHGNC, ","));
		response.add("queryHGNC:" + StringUtils.join(queryHGNC, ","));
		response.add("sizes:" + StringUtils.join(getStringVector(sizes), ","));
		response.add("vertUnit:" + Integer.toString(vertUnit));
		response.add("horizUnit:" + Integer.toString(horizUnit));
		response.add("horizSpacing:" + Integer.toString(horizSpacing));
		response.add("imageWidth:" + Integer.toString(imageWidth));

		response.add("neutralColor:" + StringUtils.join(getStringVector(neutral), ","));

		response.add("mainColors:" + StringUtils.join(getStringVector(gradient), ","));
//		response.add("positiveColors:" + StringUtils.join(getStringVector(gradient_pos), ","));
//		response.add("negativeColors:" + StringUtils.join(getStringVector(gradient_neg), ","));

		Vector<String> str_sample_order = new Vector<String>();
		for(int i=0; i<dsets.size(); i++)
			str_sample_order.addAll(getStringVector(sample_order[i]));

		Vector<String> str_average_expr = new Vector<String>();
		for(int i=0; i<dsets.size(); i++)
			str_average_expr.addAll(getStringVector(average_expr[i]));

		response.add("sampleOrder:" + StringUtils.join(str_sample_order, ","));
		response.add("averageExpr:" + StringUtils.join(str_average_expr, ","));

		return response;
	}

	
  public void doPost( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException {

    // get the web applications temporary directory
	File tempDir = (File) getServletContext().getAttribute( "javax.servlet.context.tempdir" );
    
    String sessionID = req.getParameter("sessionID");
	String dir = tempDir.getAbsolutePath();
	String organism = req.getParameter("organism");

	int dStart = Integer.parseInt(req.getParameter("d_start")); //starts at 1
	int gStart = Integer.parseInt(req.getParameter("g_start")); //starts at 1
	int dEnd = Integer.parseInt(req.getParameter("d_end")); //default: 10
	int gEnd = Integer.parseInt(req.getParameter("g_end")); //default: 100
	float up = Float.parseFloat(req.getParameter("gradient_up"));
	float down = Float.parseFloat(req.getParameter("gradient_low"));
	boolean normalize = Boolean.parseBoolean(req.getParameter("norm"));
	boolean sortSample = Boolean.parseBoolean(req.getParameter("sort_sample_by_expr"));
	String mode = req.getParameter("mode"); //"expression" or "coexpression"
	boolean filter_by_pval = Boolean.parseBoolean(req.getParameter("filter_by_pval"));
	float pvalCutoff = Float.parseFloat(req.getParameter("pval_cutoff"));
	boolean displayDatasetKeywords = true;

	boolean negativeCor = ReadScore.isNegativeCor(dir + "/" + organism, sessionID);

	if(!(req.getParameter("display_dset_keywords")==null ||
	req.getParameter("display_dset_keywords").equals("null"))){
		displayDatasetKeywords = Boolean.parseBoolean(req.getParameter("display_dset_keywords"));
	}

	boolean bClusterQuery = false;
	if(req.getParameter("cluster_by_query")!=null){
		bClusterQuery = Boolean.parseBoolean(req.getParameter("cluster_by_query"));
	}

	float rbpP = Float.parseFloat(req.getParameter("rbp_p"));
	boolean multiplyDatasetWeight = 
		Boolean.parseBoolean(req.getParameter("multiply_d_weight"));

	Vector<String> response = null;
	String colorChoice = req.getParameter("color_choice");

	Vector<String> onlyGenes = null;
	String specifyGenes = null;
	if(req.getParameter("specify_genes")!=null){
		specifyGenes = java.net.URLDecoder.decode(
			req.getParameter("specify_genes"), "UTF-8");
		String[] xs = StringUtils.split(specifyGenes);
		if(xs.length>0){
			onlyGenes = new Vector<String>();
			for(String x : xs)
				onlyGenes.add(x);
		}
	}

	if(mode.equals("expression")){
		response = doExpression(sessionID, dir, organism, colorChoice,
		gStart, gEnd, dStart, dEnd,
		up, down, normalize, sortSample, bClusterQuery, filter_by_pval, pvalCutoff, onlyGenes, 
		displayDatasetKeywords, negativeCor);
	}else if(mode.equals("coexpression")){
		response = doCoexpression(sessionID, dir, organism, colorChoice,
		gStart, gEnd, dStart, dEnd,
		up, down, filter_by_pval, pvalCutoff, rbpP, multiplyDatasetWeight, onlyGenes, 
		displayDatasetKeywords, negativeCor);
	}else{
		throw new IOException("Must specify a mode");
	}

	res.setContentType("text/plain");
	res.setCharacterEncoding("UTF-8");

	res.getWriter().write(StringUtils.join(response, "|"));
  }
}
