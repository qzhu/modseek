package seek;
import seek.Distance;
import seek.TreeNode;
import seek.Cluster;
import java.util.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.ReadDataset;
import seek.Network;
import seek.ReadPacket;
import seek.SeekImage;
import seek.GetExpression;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarraySamples {

	float[] dendro_max_depth;
	TreeNode[][] dendro_tree;
	int[] size; //number of conditions per dataset

	int[][] sample_order;
	float[][] average_expr;
    float[][][] norm_matrix; //used in init()
	float[][][] tmp_norm_matrix; //used in init()

	public MicroarraySamples(){
	}

	public TreeNode[][] GetDendroTree(){
		return this.dendro_tree;
	}
	public float[] GetDendrogramMaxDepth(){
		return this.dendro_max_depth;
	}
	public int[][] GetSampleOrder(){
		return this.sample_order;
	}
	public float[][] GetAverageExpression(){
		return this.average_expr; //in sample order
	}

	public void HierarchicalClusterSampleOrder(float[][][] matrix){
		int i, j, k;
	
		this.dendro_max_depth = new float[matrix.length];
        this.size = new int[matrix.length];
		this.dendro_tree = new TreeNode[matrix.length][];
	
		for(i=0; i<matrix.length; i++){ //dataset
			float[][] distance = new float[matrix[i].length][matrix[i].length];
			String[] names = new String[matrix[i].length];
			this.size[i] = matrix[i].length;

			for(j=0; j<matrix[i].length; j++){
				names[j] = Integer.toString(j);
				for(k=j+1; k<matrix[i].length; k++){
					float d = Distance.EuclideanDistance(matrix[i][j], matrix[i][k]);
					distance[j][k] = d;
					distance[k][j] = d;
				}
			}

			Cluster c = new Cluster(distance);
			TreeNode[] t = c.HClusterAvg();
			Vector<Integer> vi = c.Linearize(t);
			Map<Integer,Float> mm = c.GetDepth(t);

			this.dendro_tree[i] = t;
			Vector<Float> dist = new Vector<Float>();
			for(TreeNode tr : t){
				dist.add(tr.distance);
			}
			this.dendro_max_depth[i] = Collections.max(dist);

			for(j=0; j<matrix[i].length; j++){
				this.sample_order[i][j] = vi.get(j);
			}
		}
	}

	//Requires dset_order_file, and dsets
	public void WriteSampleOrder(String dset_order_file, Vector<String> dsets) throws IOException{
		PrintWriter out1 = new PrintWriter(new FileWriter(dset_order_file));
		int i, j;
		for(i=0; i<this.sample_order.length; i++){
			out1.print(dsets.get(i) + " ");
			for(j=0; j<this.sample_order[i].length; j++){
				if(j==this.sample_order[i].length-1)
					out1.print(this.sample_order[i][j]);
				else
					out1.print(this.sample_order[i][j] + " ");
			}
			out1.println();
		}
		out1.close();
		System.out.println("Finished writing sample order");
	}

	//sort samples as according to the sample order specified in matrix
	public static float[][][] SortSamples(int[][] sample_order, float[][][] tmp_norm_matrix){
		int i, j, k;
		float[][][] norm_matrix = new float[tmp_norm_matrix.length][][];
		for(i=0; i<tmp_norm_matrix.length; i++){ //dataset
			norm_matrix[i] = new float[tmp_norm_matrix[i].length][];
        	for(j=0; j<tmp_norm_matrix[i].length; j++){ 
        		norm_matrix[i][j] = new float[tmp_norm_matrix[i][j].length];
        	}
        }
		for(i=0; i<sample_order.length; i++){ //dataset
			for(j=0; j<tmp_norm_matrix[i].length; j++){ //samples
				int trans = sample_order[i][j]; 
				for(k=0; k<tmp_norm_matrix[i][0].length; k++){ //genes
					norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][k];
				}
			}
		}
		return norm_matrix;
	}
 
    //need norm_matrix, tmp_norm_matrix, gene_order
	//sort genes as according to the gene order specified in matrix
	//and sort samples simultaneously
	public static float[][][] SortGenesSamples(int[][] sample_order, int[] gene_order, float[][][] tmp_norm_matrix){
		int i, j, k;
		float[][][] norm_matrix = new float[tmp_norm_matrix.length][][];
		for(i=0; i<tmp_norm_matrix.length; i++){ //dataset
			norm_matrix[i] = new float[tmp_norm_matrix[i].length][];
        	for(j=0; j<tmp_norm_matrix[i].length; j++){ 
        		norm_matrix[i][j] = new float[tmp_norm_matrix[i][j].length];
        	}
        }
		for(i=0; i<sample_order.length; i++){ //dataset
			for(j=0; j<norm_matrix[i].length; j++){ //samples
				int trans = sample_order[i][j];
				for(k=0; k<norm_matrix[i][0].length; k++){ //genes
					int g_trans = gene_order[k];
					norm_matrix[i][j][k] = tmp_norm_matrix[i][trans][g_trans];
				}
			}
		}
		return norm_matrix;
	}

	//lead function
	public void CalculateSampleOrder(float[][][] matrix){
		this.sample_order = new int[matrix.length][];
		this.average_expr = new float[matrix.length][];
		int i, j, k;
		for(i=0; i<matrix.length; i++){
			this.sample_order[i] = new int[matrix[i].length];
			this.average_expr[i] = new float[matrix[i].length];
		}

        this.tmp_norm_matrix = new float[matrix.length][][];
		//norm_matrix dimension: dataset, genes, samples        
        for(i=0; i<matrix.length; i++){ //dataset
        	this.tmp_norm_matrix[i] = new float[matrix[i].length][];
        	for(j=0; j<matrix[i].length; j++) 
        		this.tmp_norm_matrix[i][j] = new float[matrix[i][j].length];

        	for(j=0; j<matrix[i][0].length; j++){ //number of genes
        		float[] mm = new float[matrix[i].length];
        		for(k=0; k<matrix[i].length; k++){
        			mm[k] = matrix[i][k][j];
        		}
        		for(k=0; k<matrix[i].length; k++){
      		  		this.tmp_norm_matrix[i][k][j] = mm[k];
        		}
        	}
        }

		HierarchicalClusterSampleOrder(matrix); //outputs the sample_order
		//calculates average_expr
		for(i=0; i<matrix.length; i++){
			for(j=0; j<matrix[i].length; j++){
				float sum = 0;
				int num = 0;
				for(k=0; k<matrix[i][0].length; k++){
					if(this.tmp_norm_matrix[i][j][k]>327.f) continue;
					sum+=this.tmp_norm_matrix[i][j][k];
					num++;
				}
				this.average_expr[i][j] = (float) sum / (float) num;
			}
		}
	
		/*for(i=0; i<matrix.length; i++){
			Vector<Pair> np = new Vector<Pair>();
			int num = 0;
			for(j=0; j<matrix[i].length; j++){
				float sum = 0;
				num = 0;
				for(k=0; k<matrix[i][0].length; k++){
        			if(tmp_norm_matrix[i][j][k]>327.0f) continue;
					sum+=tmp_norm_matrix[i][j][k];
					num++;
				}
				Pair p = new Pair(Integer.toString(j), (double) sum, -1);
				np.add(p);
			}
			Collections.sort(np);
			for(j=0; j<np.size(); j++){
				sample_order[i][j] = Integer.parseInt(np.get(j).term);
				average_expr[i][j] = (float) np.get(j).val / (float) num;
			}
		}*/
		System.out.println("Finished calculating sample order");
	}

	//matrix is used for dimension
	public static int[][] ReadSampleOrder(String dset_order_file, float[][][] matrix) throws IOException{
		//sample_order is dset, sample_order dimension
		int[][] sample_order = new int[matrix.length][];
		for(int i=0; i<matrix.length; i++){
			sample_order[i] = new int[matrix[i].length];
		}

		BufferedReader bb = new BufferedReader(new FileReader(dset_order_file));
		String s = null;
		int i_ind = 0;
		while((s=bb.readLine())!=null){
			StringTokenizer st = new StringTokenizer(s, " ");
			st.nextToken();
			int ki = 0;
			while(st.hasMoreTokens()){
				int order = Integer.parseInt(st.nextToken());
				sample_order[i_ind][ki] = order;
				ki++;
			}
			i_ind++;
		}
		bb.close();
		return sample_order;
	}
	public int[] GetSize(){
		return this.size;
	}

}
