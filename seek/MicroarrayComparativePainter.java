package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarrayComparativePainter{

	public static void paint_dendrogram_core(int offset_x, int offset_y, int dendro_canvas_height,
	Graphics2D g4, MicroarrayComparativeImage mi2){
		g4.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		TreeNode[][] dendro_tree = mi2.microarray_samples.GetDendroTree();
		float[] dendro_max_depth = mi2.microarray_samples.GetDendrogramMaxDepth();
		mi2.sample_order = mi2.microarray_samples.GetSampleOrder();
		int dendro_height = dendro_canvas_height;
		int i, j, k;

		int[] unitWidth = new int[mi2.size.length];
		for(i=0; i<mi2.size.length; i++){ //dataset
			unitWidth[i] = mi2.calculateUnitWidth(mi2.size[i]);
		}
	
		//Color dendro_color = new Color(168, 168, 168);//dark bg
		Color dendro_color = mi2.fg; //light bg	
		g4.setPaint(mi2.bg);
		g4.fill(new Rectangle2D.Double(offset_x, offset_y, mi2.windowWidth, dendro_height));
		g4.setPaint(dendro_color);
		//x = 5 + unitWidth[0] / 2; //offset_x
		int x = offset_x + 5 + unitWidth[0] / 2;
		int y = offset_y + 12;
		if(mi2.bPrintGeneName){
			x += mi2.geneNameWidth; 
		}

		//y=12; //offset_y
		g4.setColor(dendro_color);
		for(k=0; k<mi2.dsets.size(); k++){
			int maxWidth = mi2.calculateWidth(mi2.size[k]);
			float maxDepth = dendro_max_depth[k];
			int[] reverse = new int[mi2.size[k]];
			for(i=0; i<mi2.size[k]; i++){
				reverse[mi2.sample_order[k][i]] = i;
			}
			int orig_x = x;
			Map<Integer,Float> coord_x = new HashMap<Integer,Float>();
			Map<Integer,Float> coord_y = new HashMap<Integer,Float>();
			Map<Integer,Integer> map_m = new HashMap<Integer,Integer>();
			for(i=0; i<dendro_tree[k].length; i++){
				map_m.put(dendro_tree[k][i].left, i);
				map_m.put(dendro_tree[k][i].right, i);
			}

			for(i=0; i<mi2.size[k]; i++){
				int a = mi2.sample_order[k][i];
				TreeNode tr = dendro_tree[k][map_m.get(a)];
				coord_x.put(a, (float) x);
				coord_y.put(a, (float) (offset_y + dendro_height));
				x += unitWidth[k];
			}

			//x = orig_x;
			for(i=0; i<mi2.size[k]-1; i++){
				TreeNode tr = dendro_tree[k][i];
				int left = tr.left;
				int right = tr.right;
				float loc_x = (coord_x.get(left) + coord_x.get(right)) / 2.0f;
				float loc_y_left = coord_y.get(left);
				float loc_y_right = coord_y.get(right);
				g4.drawLine((int) coord_x.get(left).floatValue(), (int) loc_y_left, (int) coord_x.get(left).floatValue(), 
					(int) (offset_y + dendro_height - tr.distance*3));
				g4.drawLine((int) coord_x.get(right).floatValue(), (int) loc_y_right, (int) coord_x.get(right).floatValue(), 
					(int) (offset_y + dendro_height - tr.distance*3));
				
				float loc_y = offset_y + dendro_height - tr.distance*3;
				g4.drawLine((int) (coord_x.get(left).floatValue()), (int) loc_y, (int) coord_x.get(right).floatValue(), (int) loc_y);
				coord_x.put((i+1)*-1, loc_x);
				coord_y.put((i+1)*-1, (offset_y + dendro_height - tr.distance*3));
			}
				
			x += mi2.horizontalDatasetSpacing;
		}

	}

	//mi2 specifies where to get the expression image from
	public static void paint_header(int offset_x, int offset_y, Graphics2D g3, MicroarrayComparativeImage mi2){
		g3.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//drawing header image=========================
        g3.setPaint(mi2.bg);
        g3.fill(new Rectangle2D.Double(offset_x, offset_y, mi2.windowWidth, mi2.column_header_height));
        g3.setPaint(mi2.fg);
		Font font = g3.getFont();
		//int fontSize = font.getSize();
		int fontSize = 10;
		String fontName = "Arial";
		int fontStyle = font.getStyle();
		g3.setFont(new Font(fontName, fontStyle, fontSize));

		//create header
		g3.setColor(Color.BLACK);
		int x = offset_x + 5;
		//int y = offset_y + 12;
		int y_row = 12;
		int i, j, k;
		Color link_color = new Color(0, 0, 102); //blue on white, light background
		if(mi2.bPrintGeneName){
			x += mi2.geneNameWidth;
		}
		for(k=0; k<mi2.dsets.size(); k++){
			int maxWidth = mi2.calculateWidth(mi2.size[k]);
			String[] tok = StringUtils.split(mi2.description[k], " ");
			String dsetWeight = String.format("%.2f", mi2.dataset_score.get(k)*1000.0);
			g3.setPaint(mi2.bg);
			g3.fill(new Rectangle2D.Double(x, offset_y, maxWidth, mi2.column_header_height));
			g3.setColor(mi2.fg);
			String gse = ReadScore.getDatasetID(mi2.dsets.get(k));
			g3.drawString((mi2.dataset_start + k+1) + ". " + gse + " (" + dsetWeight + ")", x, offset_y + 12);
			String tmp = tok[0];
			int row = 2;
			g3.setColor(link_color);
			if(tok.length==1){
				int y_coord = offset_y + y_row * row;
				int strWidth = g3.getFontMetrics().stringWidth(tmp);
				if(strWidth<=maxWidth){
					g3.drawString(tmp, x, y_coord);
				}else{
					g3.drawString("...", x, y_coord);
				}
			}
			for(i=1; i<tok.length; i++){
				int y_coord = offset_y + y_row * row;
				int strWidth = g3.getFontMetrics().stringWidth(tmp + " " + tok[i]);
				if(i==tok.length-1){
					if(strWidth<=maxWidth){
						g3.drawString(tmp + " " + tok[i], x, y_coord);
					}else{
						g3.drawString(tmp, x, y_coord);
						row++;
						y_coord += y_row;
						g3.drawString(tok[i], x, y_coord);
					}
					break;
				}
				if(strWidth<=maxWidth){
					tmp += " " + tok[i];
					continue;
				}
				g3.drawString(tmp, x, y_coord);
				row++;
				tmp = tok[i];
			}
			x += maxWidth + mi2.horizontalDatasetSpacing;
		}
	}

	public static void paint_label(int offset_vert, int offset_horiz, Graphics2D g2, MicroarrayComparativeImage mi2){
		Vector<Vector<String> > vs_tmp = null;
		Vector<Vector<String> > vs2 = null;
		int[][] sample_order = mi2.GetSampleOrder();
		try{
			vs2 = new Vector<Vector<String> >();
			vs_tmp = mi2.GetSampleAnnotation();
			for(int i=0; i<vs_tmp.size(); i++){
				Vector<String> t = new Vector<String>();
				for(int j=0; j<vs_tmp.get(i).size(); j++){
					int trans = sample_order[i][j];
					t.add(vs_tmp.get(i).get(trans));
				}
				vs2.add(t);
			}
		}catch(IOException e){
		}

		int fontSize = 10;
		g2.setFont(new Font("Arial", g2.getFont().getStyle(), fontSize));
		AffineTransform orig = g2.getTransform();
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI/2);
		g2.setTransform(at);
		g2.setColor(Color.BLACK);

		//int label_height = -1 * (dendro_canvas_height + column_header_height);
		int label_height = offset_vert; // should be negative
		//offset_horiz 
		int x;
		if(mi2.bPrintGeneName){
			x = offset_horiz + 3 + mi2.geneNameWidth + fontSize;
		}else{
			x = offset_horiz + 3 + fontSize;
		}
		for(Vector<String> this_dset : vs2){
			for(String td : this_dset){
				String[] chars = StringUtils.split(td, "|");
				String str = chars[1].replace("_", " ");
				int width = g2.getFontMetrics().stringWidth(str);
				if(width<mi2.sampleLabelWidth){
					g2.drawString(str, label_height, x);
				}
				for(int i=str.length(); i>=1; i-=3){
					String newStr = str.substring(0, i);
					if(g2.getFontMetrics().stringWidth(newStr)<mi2.sampleLabelWidth){
						g2.drawString(newStr, label_height, x);
						break;
					}
				}
				//int strWidth = g2.getFontMetrics().stringWidth(chars[1]);
				//if(strWidth<=this.sampleLabelWidth){
				//	g2.drawString(chars[1], label_height, x);
				x+=mi2.horizontalUnitWidth;
			}
			x+=mi2.horizontalDatasetSpacing;
		}
		g2.setTransform(orig);
	}

	public static void paint_header_vertical(int offset_vert, int offset_horiz, Graphics2D g2, MicroarrayComparativeImage mi2){
		int fontSize = 10;
		g2.setFont(new Font("Arial", g2.getFont().getStyle(), fontSize));
		AffineTransform orig = g2.getTransform();
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI/2);
		g2.setTransform(at);
		g2.setColor(Color.BLACK);
		//int label_height = -1 * (dendro_canvas_height + column_header_height);
		int label_height = offset_vert; // should be negative
		//offset_horiz 
		int x; 
		if(mi2.bPrintGeneName){
			x = offset_horiz + 3 + mi2.geneNameWidth + fontSize;
		}else{
			x = offset_horiz + 3 + fontSize;
		}
		int i, j, k;
		Color link_color = new Color(0, 0, 102); //blue on white, light background
		for(k=0; k<mi2.dsets.size(); k++){
			int orig_x = x;
			int sampleWidth = mi2.calculateWidth(mi2.size[k]);
			int maxWidth = mi2.headerVerticalLength;
			//int numLines = 0;
			String[] tok = StringUtils.split(mi2.description[k], " ");
			String dsetWeight = String.format("%.2f", mi2.dataset_score.get(k)*1000.0);

			g2.setPaint(mi2.bg);
			g2.fill(new Rectangle2D.Double(label_height, x, maxWidth, sampleWidth + mi2.horizontalDatasetSpacing));
			g2.setColor(mi2.fg);

			String gse = ReadScore.getDatasetID(mi2.dsets.get(k));
			String s1 = (mi2.dataset_start + k + 1) + ". " + gse  + " (" + dsetWeight + ")";
			g2.drawString(s1, label_height, x); //first line: dataset weight information
			x+=fontSize; //next line

			String tmp = tok[0];
			//int row = 2;
			g2.setColor(link_color);

			if(tok.length==1){
				//int y_coord = offset_y + y_row * row;
				int strWidth = g2.getFontMetrics().stringWidth(tmp);
				if(strWidth<=maxWidth){
					g2.drawString(tmp, label_height, x);
					//g2.drawString(tmp, x, y_coord);
				}else{
					g2.drawString("...", label_height, x);
					//g2.drawString("...", x, y_coord);
				}
			}
			for(i=1; i<tok.length; i++){
				//int y_coord = offset_y + y_row * row;
				int strWidth = g2.getFontMetrics().stringWidth(tmp + " " + tok[i]);
				if(i==tok.length-1){
					if(strWidth<=maxWidth){
						g2.drawString(tmp + " " + tok[i], label_height, x);
						//g3.drawString(tmp + " " + tok[i], x, y_coord);
					}else{
						g2.drawString(tmp, label_height, x);
						x+=fontSize;
						//g3.drawString(tmp, x, y_coord);
						//row++;
						//y_coord += y_row;
						//g3.drawString(tok[i], x, y_coord);
						g2.drawString(tok[i], label_height, x);
					}
					break;
				}
				if(strWidth<=maxWidth){
					tmp += " " + tok[i];
					continue;
				}
				g2.drawString(tmp, label_height, x);
				x+=fontSize;
				//g3.drawString(tmp, x, y_coord);
				//row++;
				tmp = tok[i];
			}
			x = orig_x + sampleWidth + mi2.horizontalDatasetSpacing;
		}
		g2.setTransform(orig);
	}
	public static void paint_expression(int offset_x, int offset_y, Graphics2D g2, MicroarrayComparativeImage mi2){
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(mi2.bg);
        g2.fill(new Rectangle2D.Double(offset_x, offset_y, mi2.windowWidth, mi2.windowHeight));
        g2.setPaint(mi2.fg);        
		Font font = g2.getFont();
		int fontStyle = font.getStyle();
		int fontSize = 12;
		String fontName = "Arial";
		g2.setFont(new Font(fontName, fontStyle, fontSize));

		int i, j, k;
		int[] unitWidth = new int[mi2.size.length];
        for(i=0; i<mi2.size.length; i++){ //dataset
			unitWidth[i] = mi2.calculateUnitWidth(mi2.size[i]);
		}
		float scale = 1.0f;
		Color hatching_color = null;
		if(mi2.neutral_red>200 && mi2.neutral_green>200 && mi2.neutral_blue>200){
			hatching_color = new Color(36, 36, 36); //light bg
		}else{
			hatching_color = new Color(128, 128, 128); //dark bg
		}
		int y = offset_y;
        for(k=0; k<mi2.numGeneToShow; k++){
        	int x = 5 + offset_x;
			if(mi2.bPrintGeneName){
				g2.setPaint(mi2.fg);
				g2.drawString(mi2.names[k], x, y + mi2.verticalUnitHeight - 2 * scale);
				x += mi2.geneNameWidth;
			}
			System.out.println(mi2.names[k]);
        	for(i=0; i<mi2.size.length; i++){ //dataset
				int orig_x = x;
        		for(j=0; j<mi2.size[i]; j++){ //column in dataset
        			if(mi2.norm_matrix[i][j][k]>327f){
        				g2.setPaint(new Color(mi2.neutral_red, mi2.neutral_green, mi2.neutral_blue));
        			}else{
        				g2.setPaint(new Color(
        					mi2.color_red[i][j][k], mi2.color_green[i][j][k], mi2.color_blue[i][j][k]));
        			}
					g2.fill(new Rectangle2D.Double(x, y, unitWidth[i], mi2.verticalUnitHeight));
        			x += unitWidth[i];
        		}
				if(mi2.norm_matrix[i][0][k]>327f){
					g2.setPaint(hatching_color);
					int xt = orig_x;
					while(true){
						if(xt + mi2.verticalUnitHeight > orig_x + unitWidth[i] * mi2.size[i]) break;
						g2.drawLine(xt, y + mi2.verticalUnitHeight, xt + mi2.verticalUnitHeight, y);
						xt += mi2.verticalUnitHeight / 2;
					}
				}
        		x += mi2.horizontalDatasetSpacing;
        	}
        	y += mi2.verticalUnitHeight;
		}
	}
	public static void paint_gene_name(int offset_x, int offset_y, Graphics2D g2, MicroarrayComparativeImage mi2){
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(mi2.bg);
        g2.fill(new Rectangle2D.Double(offset_x, offset_y, mi2.geneNameWidth, mi2.windowHeight));
        g2.setPaint(mi2.fg);        
		Font font = g2.getFont();
		int fontStyle = font.getStyle();
		int fontSize = 12;
		String fontName = "Arial";
		g2.setFont(new Font(fontName, fontStyle, fontSize));
		int i, j, k;
		int y = offset_y;
		int x = offset_x;
		float scale = 1.0f;
        for(k=0; k<mi2.numGeneToShow; k++){
			g2.setPaint(mi2.fg);
			g2.drawString(mi2.names[k], x, y + mi2.verticalUnitHeight - 2 * scale);
			y+=mi2.verticalUnitHeight;
		}

	}

	public static void paint_ortholog_boundary(int offset_x, int offset_y, Graphics2D g2, MicroarrayComparativeImage mi2, int width){
		for(int k=0, y=offset_y; k<mi2.numGeneToShow; k++){
			if(k<mi2.numGeneToShow-1 && !mi2.geneWithSpace.get(k+1).equals("empty") &&
			mi2.mapOrthologGroup.get(k+1)!=mi2.mapOrthologGroup.get(k)){
				g2.setPaint(mi2.fg);
				g2.drawLine(offset_x, y + mi2.verticalUnitHeight, width, 
					y + mi2.verticalUnitHeight);
			}
			y += mi2.verticalUnitHeight;
        }
	}


}
