package seek;
import seek.TreeNode;
import seek.Distance;
import seek.Cluster;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.OrthologGroup;
import seek.Pair;
import seek.ReadScore;
import seek.ReadDataset;
import seek.Network;
import seek.ReadPacket;
import seek.SeekImage;
import seek.GetExpression;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarraySimpleComparativeImage extends SeekImage{
	public class ExpressionMatrix{
		float[][][] mat;
		float[][][] gmat;
		public ExpressionMatrix(float[][][] mat, float[][][] gmat){
			this.mat = mat;
			this.gmat = gmat;
		}
	}

	String organism1;
	String organism2;

	Vector<String> dsets;
	Vector<String> dataset_annot;
	Vector<OrthologGroup> genes;
	Map<String, Map<String,Integer> > m;


	public boolean read(int oStart, int oEnd, 
	int dStart1, int dEnd1, int dStart2, int dEnd2,
	Vector<OrthologGroup> onlyGenes, 
	Vector<String> rDataset1, Vector<String> rDataset2) throws IOException{

		int numD1 = dEnd1 - dStart1 + 1;
		int numD2 = dEnd2 - dStart2 + 1;
		int numG = oEnd - oStart + 1;

		this.dsets_1 = new Vector<String>();
		this.dataset_annot_1 = new Vector<String>();

		this.dsets_2 = new Vector<String>();
		this.dataset_annot_2 = new Vector<String>();

		for(int i=0; i<numD1; i++){
			dsets_1.add(rDataset1.get(dStart1 + i));
			dataset_annot_1.add(ReadScore.getDatasetID(rDataset1.get(dStart1 + i)));
		}

		for(int i=0; i<numD2; i++){
			dsets_2.add(rDataset2.get(dStart2 + i));
			dataset_annot_2.add(ReadScore.getDatasetID(rDataset2.get(dStart2 + i)));
		}
		
		this.genes = new Vector<OrthologGroup>();
		for(int i=0; i<numG; i++){
			if(oStart+i>=onlyGenes.size()) break;
			this.genes.add(onlyGenes.get(oStart + i));
		}
		

		
		

	}


}
