package seek;
import java.io.*;
import java.util.*;


public class OrthologGroup{
	Vector<String> grp1;
	Vector<String> grp2;
	String org1;
	String org2;
	int ID;
	public OrthologGroup(int ID, String org1, String org2, Vector<String> vs1, Vector<String> vs2){
		this.ID = ID;
		this.org1 = org1;
		this.org2 = org2;
		grp1 = new Vector<String>();
		grp2 = new Vector<String>();
		for(String s : vs1){
			grp1.add(s);
		}
		for(String s : vs2){
			grp2.add(s);
		}
	}
	public Vector<String> getOrthologs(int index){
		if(index==0){
			return grp1;
		}else if(index==1){
			return grp2;
		}else{
			System.err.println("Invalid return ortholog!");
			return null; //BAD
		}
	}
	public int getID(){
		return this.ID;
	}
	public String getOrganism(int index){
		if(index==0){
			return this.org1;
		}else if(index==1){
			return this.org2;
		}else{
			System.err.println("Invalid return organism!\n");
			return null;
		}
	}
	public String toString(){
		String str = grp1.toString() + "|" + grp2.toString();
		return str;

	}
}
