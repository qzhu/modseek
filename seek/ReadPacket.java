package seek;
import java.util.Vector;

public class ReadPacket{
	public int num_element;
	public int element_size;
	public boolean isString;
	public String strVal;
	public Vector<Float> vecFloat;
	public byte[] originalByte;
	public ReadPacket(){
		originalByte = null;
		num_element = -1;
		element_size = -1;
		isString = false;
		strVal = null;
		vecFloat = null;
	}
}

