package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class GetSearchMessage extends HttpServlet {
	
	public void doGet( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException {

		HttpSession session = req.getSession();
		String parameter = req.getParameter("param");
		String sessionID = req.getParameter("sessionID");
		Vector<String> message = (Vector<String>) session.getAttribute(sessionID + "_" + parameter);
		
		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");
		
		if(message==null){
			res.getWriter().write("Waiting...\n");
		}else{
			for(String s : message){
				res.getWriter().write(s + "\n");
			}
		}
  }

}
