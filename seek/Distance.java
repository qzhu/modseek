package seek;
public class Distance{
	public static float EuclideanDistance(float[] d1, float[] d2){
		float d = 0;
		for(int i=0; i<d1.length; i++){
			if(d1[i]>327.0f) continue;
			float f = (float) d1[i] - (float) d2[i];
			/*if(Float.isNaN(f) || Float.isInfinite(f)){
				continue;
			}*/
			d+=f*f;
		}
		float xd = (float) Math.sqrt(d);
		if(d==0){
			return 0f;
		}
		return xd;
	}
}
