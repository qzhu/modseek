package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.*;
import seek.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class CompareList2{
	String organism1;
	String organism2;

	RankList list1;
	RankList list2;
	RankList ll1;
	RankList ll2;

	Map<String, Map<String,OrthologGroup> > m;

	Vector<String> singleton_1;
	Vector<String> singleton_2;

	Vector<OrthologGroup> v_ortho;
	Vector<OrthologGroup> q_ortho;

	Set<Integer> all_ortho_1;
	Set<Integer> all_ortho_2;
	Set<Integer> top_ortho_1;
	Set<Integer> top_ortho_2;
	Set<Integer> overlap_ortho;

	float singleton_per;
	float ortholog_per;
	float overlap_per;
	float nonconserved_per;
	int num_overlap;	

	float threshold;

	public Vector<String> query1;
	public Vector<String> query2;
	
	RankingScore dataset_list1; 
	RankingScore dataset_list2; 

	//For doing enrichment
	public Vector<String> final_list;
	public Vector<String> final_background;
	public String final_organism;

	//Return string for GetDistanceServlet
	public Vector<String> stt;

	public CompareList2(){
		final_list = new Vector<String>();
		final_background = new Vector<String>();
		final_organism = "";
	}

	public class RankingScore{
		public Vector<String> entities;
		public Vector<Double> scores;
	}

	public RankingScore GetGeneRanking(SeekFile sf, String sessionID)
	throws IOException{
		RankingScore rs = new RankingScore();
		String ext = "_gscore";
		String name_mapping = sf.gene_map_file;
		//String name_mapping_2 = sf.gene_entrez_map_file;
		System.out.println("Name mapping 1 is " + name_mapping);
		//System.out.println("Name mapping 2 is " + name_mapping_2);
		//Map<String,Integer> mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
		Map<String,Integer> mm = ReadScore.readGeneMapping(name_mapping);
		File tempFile = new File(sf.getPath(sessionID + ext));
		System.out.println("tempFile: " + sessionID + " " + tempFile.getAbsolutePath());
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		boolean negativeCor = ReadScore.isNegativeCor(sf.leading_path, sessionID);
		Vector<Pair> vp = ReadScore.SortScores(mm, sc, negativeCor);
		Vector<String> allGenes = new Vector<String>();
		Vector<Double> allScores = new Vector<Double>();
		for(int i=0; i<vp.size(); i++){
			String term = vp.get(i).term;
			allGenes.add(term); //will be in Entrez
			allScores.add(vp.get(i).val); //will be score
		}
		rs.entities = allGenes;
		rs.scores = allScores;
		return rs;
	}

	public RankingScore GetDatasetRanking(SeekFile sf, String sessionID)
	throws IOException{
		RankingScore rs = new RankingScore();
		String ext = "_dweight";
		String name_mapping = sf.dataset_platform_file;
		Map<String,Integer> mm = ReadScore.readDatasetMapping(name_mapping);
		File tempFile = new File(sf.getPath(sessionID + ext));	
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		Vector<Pair> vp = ReadScore.SortScores(mm, sc, false);
		Vector<String> allDatasets = new Vector<String>();
		Vector<Double> allScores = new Vector<Double>();

		for(int i=0; i<vp.size(); i++){
			String term = vp.get(i).term;
			allDatasets.add(term);
			allScores.add(vp.get(i).val);
		}
		rs.entities = allDatasets;
		rs.scores = allScores;
		return rs;		
	}

	/*
	public Vector<String> GetQuery(SeekFile sf, String sessionID)
	throws IOException{ //in symbol
		Vector<String> vs = new Vector<String>();
		String ext = "_query";
		Map<String, String> ent_symbol = new HashMap<String, String>();
		Map<String, String> symbol_ent = new HashMap<String, String>();
		ReadScore.readGeneEntrezSymbolMap(sf.gene_entrez_map_file, ent_symbol, symbol_ent);
		File tempFile = new File(sf.getPath(sessionID + ext));
		try{
			BufferedReader in = new BufferedReader(new FileReader(tempFile));
			String s = in.readLine();
			StringTokenizer st = new StringTokenizer(s, " ");
			while(st.hasMoreTokens()){
				String x = st.nextToken();
				vs.add(ent_symbol.get(x));
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + tempFile);
		}
		return vs;
	}*/
	
	public Vector<String> GetQueryEntrez(SeekFile sf, String sessionID)
	throws IOException{ //in entrez
		Vector<String> vs = new Vector<String>();
		String ext = "_query";
		File tempFile = new File(sf.getPath(sessionID + ext));
		try{
			BufferedReader in = new BufferedReader(new FileReader(tempFile));
			String s = in.readLine();
			StringTokenizer st = new StringTokenizer(s, " ");
			while(st.hasMoreTokens()){
				String x = st.nextToken();
				vs.add(x);
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + tempFile);
		}
		return vs;
	}

	public boolean isWithinThreshold(String option, float k, float threshold){
		if(option.equals("up_up") || option.equals("up_down")){
			if(k>threshold) return false;
		}else if(option.equals("down_down") || option.equals("down_up")){
			if(k<1.0f-threshold) return false;
		}
		return true;
	}

	public void Compare(String organism1, String organism2,
	String sessionID1, String sessionID2, String tempDir, 
	String option, String show_list, boolean shuffle) 
	throws IOException{

		this.organism1 = organism1;
		this.organism2 = organism2;
		
		SeekFile sf1 = new SeekFile(tempDir, organism1);
		SeekFile sf2 = new SeekFile(tempDir, organism2);

		boolean isCrossSpecies = false;
		if(!organism1.equals(organism2))
			isCrossSpecies = true;

		RankingScore rs1 = GetGeneRanking(sf1, sessionID1);
		RankingScore rs2 = GetGeneRanking(sf2, sessionID2);
		if(shuffle){
			Collections.shuffle(rs1.entities);
			Collections.shuffle(rs1.scores);
			Collections.shuffle(rs2.entities);
			Collections.shuffle(rs2.scores);
		}

		this.dataset_list1 = GetDatasetRanking(sf1, sessionID1);
		this.dataset_list2 = GetDatasetRanking(sf2, sessionID2);

		this.list1 = new RankList(rs1.entities);
		this.list2 = new RankList(rs2.entities);

		Vector<Double> score1 = rs1.scores;
		Vector<Double> score2 = rs2.scores;

		this.ll1 = null;
		this.ll2 = null;
		//String option = req.getParameter("option");
		//String show_list = req.getParameter("show_list"); //shared, exclusive_to_left, or exclusive_to_right

		this.threshold = 0.1f;
		this.query1 = GetQueryEntrez(sf1, sessionID1);
		this.query2 = GetQueryEntrez(sf2, sessionID2);

		//TO DO: query orthologGroup, query singleton
		this.m = OrthologReader.Prepare(tempDir, organism1, organism2);

		if(!isCrossSpecies){ //show_list == shared OR isCrossSpecies == false

			if(option.equals("sum") || option.equals("diff")){ //get the sum or diff of two lists (within species)
				Map<String,Double> sd = new HashMap<String,Double>();
				Set<String> temp1 = new HashSet<String>(list1.list());
				Set<String> temp2 = new HashSet<String>(list2.list());
				Set<String> geneShared = new HashSet<String>();
				geneShared.addAll(temp1);
				geneShared.retainAll(temp2);

				for(int i=0; i<list1.size(); i++){
					if(geneShared.contains(list1.get(i))){
						sd.put(list1.get(i), score1.get(i));
					}
				}
				for(int i=0; i<list2.size(); i++){
					if(geneShared.contains(list2.get(i))){
						double dx = 0;
						if(sd.containsKey(list2.get(i))){
							dx = sd.get(list2.get(i));
						}
						if(option.equals("sum")){
							sd.put(list2.get(i), dx + score2.get(i));
						}else if(option.equals("diff")){
							sd.put(list2.get(i), dx - score2.get(i));
						}
					}
				}
				Vector<Pair> vp = new Vector<Pair>();
				Vector<String> gShared = new Vector<String>(geneShared);
				for(int i=0; i<gShared.size(); i++){
					String gi = gShared.get(i);
					vp.add(new Pair(gi, sd.get(gi), i));
				}

				Collections.sort(vp);
				Collections.reverse(vp);

				//prepare the output
				int len = (int)(this.threshold * (float) vp.size());
				this.stt = new Vector<String>();
				stt.add(Integer.toString(len));
				stt.add("-1"); //l2 list size is -1 (NA), since there is only one list outputed
				Vector<String> stu = new Vector<String>();
				for(int i=0; i<len; i++){
					final_background.add(vp.get(i).term);
					String gg = vp.get(i).term;
					String fx = String.format("%.3f", (float)(vp.get(i).val));
					String rs = gg + " " + fx;
					stu.add(rs);
					final_list.add(gg);
				}
				final_organism = organism1;
				stt.add("-1"); //average sequence similarity (-1: NA)
				stt.addAll(stu);

			}

			//rank based comparison of two lists (within species)
			else if(option.equals("up_up") || option.equals("up_down") ||
			option.equals("down_down") || option.equals("down_up")){
				ll1 = list1;
				ll2 = list2;
				this.stt = new Vector<String>(); //return string;
				stt.add(Integer.toString(list1.size())); //output ll1 size
				stt.add(Integer.toString(list2.size())); //output ll2 size
				Vector<String> stu = new Vector<String>();
				for(int i=0; i<ll1.size(); i++){
					float k = (float)(i+1) / (float)(ll1.size());
					final_background.add(ll1.get(i));
					if(!isWithinThreshold(option, k, this.threshold)){
						continue;
					}
					if(!list2.contains(ll1.get(i))) continue;
					float k2 = (float)(list2.getRank(ll1.get(i))) / (float)(ll2.size());
					if(!isWithinThreshold(option, k2, this.threshold)){
						continue;
					}
					String gg = ll1.get(i);
					String fx = String.format("%.3f", (float)(list2.getRank(gg)) / (float)(ll2.size()));
					String fy = String.format("%.0f", 100f);
					String sd = gg + " " + fx + " " + fy;
					String rs = gg + " " + String.format("%.3f", k) + "|" + sd;
					stu.add(rs);
					final_list.add(gg);
				}
				final_organism = organism1;
				if(option.equals("down_down") || option.equals("down_up")){
					Collections.reverse(stu);
					Collections.reverse(final_list);
				}
				stt.add("-1"); //average sequence similarity (-1: NA)
				stt.addAll(stu);
			}
		}
		else if(isCrossSpecies && (show_list.equals("exclusive_to_left") || show_list.equals("exclusive_to_right"))){
			this.ll1 = new RankList();
			this.ll2 = new RankList();

			//filter list to those in orthologGroups
			Map<String, OrthologGroup> ms = m.get(organism1 + "_" + organism2);
			for(String g : this.list1.list()){
				if(ms.get(g)==null){ //no orthologGroup exist for this gene
					this.ll1.add(g);
				}
			}
			//filter list to those in orthologGroups
			ms = m.get(organism2 + "_" + organism1);
			for(String g : this.list2.list()){
				if(ms.get(g)==null){ //no orthologGroup exist for this gene
					this.ll2.add(g);
				}
			}
			RankList lt = null;
			RankList list_t = null;
			if(show_list.equals("exclusive_to_left")){
				lt = this.ll1;
				list_t = this.list1;
				final_organism = organism1;
			}else if(show_list.equals("exclusive_to_right")){
				lt = this.ll2;
				list_t = this.list2;
				final_organism = organism2;
			}

			for(String g : lt.list()){
				float k = (float)list_t.getRank(g) / (float) list_t.size();
				//final_background.add(lt.get(i));
				if(option.equals("up")){
					if(k>this.threshold) continue;
				}else if(option.equals("down")){
					if(k<1.0f-this.threshold) continue;
				}
				final_list.add(g);
			}
			if(option.equals("down")){
				Collections.reverse(final_list);
			}

		}else{ //cross species comparison, and do shared ortholog group comparison

			this.ll1 = new RankList();
			this.ll2 = new RankList();

			this.all_ortho_1 = new HashSet<Integer>();
			this.all_ortho_2 = new HashSet<Integer>();

			this.q_ortho = new Vector<OrthologGroup>();

			//filter to ortholog groups
			Map<String, OrthologGroup> ms = m.get(organism1 + "_" + organism2);
			for(String g : this.list1.list()){
				OrthologGroup ss = null;
				if((ss=ms.get(g))==null) continue; //get corresponding orthologgroup
				for(String sl : ss.getOrthologs(1)){ //the corresponding list in other organism
					if(this.list2.contains(sl)){ //if the other organism's result list contains it
						this.ll1.add(g); //good gene
						this.all_ortho_1.add(ss.getID());
						break;
					}
				}
			}
			//filter to ortholog groups
			ms = m.get(organism2 + "_" + organism1);
			for(String g : this.list2.list()){
				OrthologGroup ss = null;
				if((ss=ms.get(g))==null) continue;
				for(String sl : ss.getOrthologs(1)){
					if(this.list1.contains(sl)){
						this.ll2.add(g);
						this.all_ortho_2.add(ss.getID());
						break;
					}
				}
			}

			//get Query OrthologGroup
			ms = m.get(organism1 + "_" + organism2);
			Set<Integer> overlap_query = new HashSet<Integer>();
			Set<String> set_query1 = new HashSet<String>(this.query1);
			Set<String> set_query2 = new HashSet<String>(this.query2);
			for(String g : this.query1){
				OrthologGroup ss = null;
				if((ss=ms.get(g))==null) continue;
				int this_ID = ss.getID();
				if(overlap_query.contains(this_ID)) continue;

				Vector<String> ortho_1 = new Vector<String>();
				Vector<String> ortho_2 = new Vector<String>();
				for(String sl : ss.getOrthologs(1)){
					if(set_query2.contains(sl)){
						ortho_2.add(sl);
					}
				}
				if(ortho_2.size()==0) continue;
				ortho_1.add(g);
				for(String sl : ss.getOrthologs(0)){
					if(sl.equals(g)) continue;
					if(set_query1.contains(sl)){
						ortho_1.add(sl);
					}
				}
				overlap_query.add(this_ID);	
				this.q_ortho.add(new OrthologGroup(this_ID, ss.getOrganism(0), ss.getOrganism(1), 
					ortho_1, ortho_2));
			}

			//real thing
			ms = m.get(organism1 + "_" + organism2);
			this.v_ortho = new Vector<OrthologGroup>();
			this.overlap_ortho = new HashSet<Integer>();
			for(int i=0; i<this.ll1.size(); i++){
				String g = this.ll1.get(i);
				//float k = (float)(i+1) / (float)(ll1.size());
				float k = (float) this.list1.getRank(g) / (float) (this.list1.size());

				//immediate filtering based on rank
				if(!isWithinThreshold(option, k, this.threshold))	continue;

				OrthologGroup ss = ms.get(g);
				if(this.overlap_ortho.contains(ss.getID()))	continue;

				String morg1 = ss.getOrganism(0);
				String morg2 = ss.getOrganism(1);
				int this_ID = ss.getID();
				Vector<String> this_ortholog_list_1 = new Vector<String>();
				Vector<String> this_ortholog_list_2 = new Vector<String>();

				for(String g2 : ss.getOrthologs(1)){ //search in other organism's list
					if(this.list2.contains(g2)){
						float k2 = (float) this.list2.getRank(g2) / (float) this.list2.size();
						//float k2 = (float)(rank2.get(g2)) / (float)(ll2.size());
						if(!isWithinThreshold(option, k2, this.threshold))	continue;
						this_ortholog_list_2.add(g2);
					}
				}
				if(this_ortholog_list_2.size()==0){
					continue;
				}
				this_ortholog_list_1.add(g);
				for(String g1 : ss.getOrthologs(0)){
					if(g1.equals(g))	continue;
					if(this.list1.contains(g1)){
						float k1 = (float) this.list1.getRank(g1) / (float) this.list1.size();
						if(!isWithinThreshold(option, k1, this.threshold))	continue;
						this_ortholog_list_1.add(g1);
					}
				}
				this.overlap_ortho.add(this_ID);
				this.v_ortho.add(new OrthologGroup(this_ID, morg1, morg2, 
					this_ortholog_list_1, this_ortholog_list_2));
			}

			this.top_ortho_1 = new HashSet<Integer>();
			this.top_ortho_2 = new HashSet<Integer>();

			ms = m.get(organism1 + "_" + organism2);
			for(String g : this.ll1.list()){
				float k = (float) this.list1.getRank(g) / (float) (this.list1.size());
				if(!isWithinThreshold(option, k, this.threshold))	continue;
				top_ortho_1.add(ms.get(g).getID());
			}

			ms = m.get(organism2 + "_" + organism1);
			for(String g : this.ll2.list()){
				float k = (float) this.list2.getRank(g) / (float) (this.list2.size());
				if(!isWithinThreshold(option, k, this.threshold))	continue;
				top_ortho_2.add(ms.get(g).getID());
			}

			this.singleton_1 = new Vector<String>();
			ms = m.get(organism1 + "_" + organism2);
			for(String g : this.list1.list()){
				float k = (float) this.list1.getRank(g) / (float) (this.list1.size());
				if(!isWithinThreshold(option, k, this.threshold))	continue;
				if(ms.get(g)==null)	this.singleton_1.add(g);
			}

			this.singleton_2 = new Vector<String>();
			ms = m.get(organism2 + "_" + organism1);
			for(String g : this.list2.list()){
				float k = (float) this.list2.getRank(g) / (float) (this.list2.size());
				if(!isWithinThreshold(option, k, this.threshold)) 	continue;
				if(ms.get(g)==null)	this.singleton_2.add(g);
			}

			//output statistics - percent ortholog groups overlap among top
			this.num_overlap = this.overlap_ortho.size();
			this.overlap_per = (float) (this.num_overlap * 2.0) / (float) 
				(this.top_ortho_1.size() + this.top_ortho_2.size());
			//ortholog groups in top, but do not overlap - represents divergence
			this.nonconserved_per = 1.0f - this.overlap_per;

			//total singleton fraction - number of species-specific genes
			this.singleton_per = (float) (this.singleton_1.size() + this.singleton_2.size()) / (float) (this.threshold * 
				(list1.size() + list2.size()));
			//total ortholog fraction - number of genes among top that map to ortholog groups
			this.ortholog_per = 1.0f - this.singleton_per;

			System.out.println("Number of conserved orthogroups " + this.num_overlap);
			System.out.println("Conserved orthogroup percentage " + this.overlap_per);
			System.out.println("Nonconserved orthogroup percentage " + this.nonconserved_per);
			System.out.println("Singleton percentage " + this.singleton_per);
			System.out.println("Ortholog percentage " + this.ortholog_per);
			/*System.out.println("Conserved orthogroups: ");
			for(OrthologGroup or : this.v_ortho){
				System.out.println("Group ==========");
				System.out.println(or);
			}*/
		}
	}

	public Vector<OrthologGroup> GetConservedOrthologGroups(){
		return this.v_ortho;
	}

	public Vector<OrthologGroup> GetQueryOrthologGroups(){
		return this.q_ortho;
	}

	public Vector<String> GetRankedDatasets(int i){
		if(i==0){
			return this.dataset_list1.entities;
		}
		return this.dataset_list2.entities;
	}

	public static void main(String[] argv) throws IOException{
		String organism1 = argv[0];
		String organism2 = argv[1];
		String sessionID1 = argv[2];
		String sessionID2 = argv[3];
		boolean toShuffle = false;
		int zoomLevel = Integer.parseInt(argv[4]);
		//boolean toShuffle = Boolean.parseBoolean(argv[4]);
		//String tempDir = "/var/lib/tomcat6/work/Catalina/localhost/_";
		String tempDir = "/home/qzhu/modSeek.may26/tomcat_mirror/_";
		String option = "up_up";
		String show_list = "shared";	

		if(toShuffle){
			Vector<Integer> numConserved = new Vector<Integer>();
			for(int i=0; i<100; i++){
				CompareList2 cp = new CompareList2();
				cp.Compare(organism1, organism2, sessionID1, sessionID2, tempDir, option, show_list, toShuffle);
				numConserved.add(cp.num_overlap);
			}
			Collections.sort(numConserved);
			for(int i=100; i>0; i-=5){
				System.out.println("p = " + (float) (100-i) / 100.0f + " " + numConserved.get(i-1));
			}
			return ;
		}

		CompareList2 cp = new CompareList2();
		cp.Compare(organism1, organism2, sessionID1, sessionID2, tempDir, option, show_list, toShuffle);
		
		MicroarrayComparativeImage mi1 = new MicroarrayComparativeImage(
			tempDir, organism1, "blue-black-yellow");
		
		String imageFile1 = mi1.sf.getPath(sessionID1+"_image.png");
		String headerFile1 = mi1.sf.getPath(sessionID1+"_header_image.png");
		String dendrogramFile1 = mi1.sf.getPath(sessionID1+"_dendrogram_image.png");
		String sampleLabelFile1 = mi1.sf.getPath(sessionID1+"_sampleLabel_image.png");
		String geneNameFile1 = mi1.sf.getPath(sessionID1+"_geneName_image.png");
		boolean displayDatasetKeywords = false;
		int dStart1, dEnd1, dStart2, dEnd2;
		int gStart1, gEnd1, gStart2, gEnd2;
		dStart1 = 0;
		dEnd1 = 2;
		boolean normalize=true;

		mi1.ReadMap(displayDatasetKeywords);
		mi1.SampleOrder(sessionID1, 
			0, 50, dStart1, dEnd1,
			false, //is query or not
			normalize, //normalize expression or not
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);

		gStart1 = 0;
		gEnd1 = 100;
		boolean sortSample = true;
		float down = -2.0f; //clip
		float up = 2.0f; //clip
		mi1.Load(sessionID1,
			gStart1, gEnd1, dStart1, dEnd1,
			down, up,
			normalize, //normalize expression or not
			sortSample, //sort Samples 
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);
		mi1.paint(headerFile1, dendrogramFile1, geneNameFile1, imageFile1, sampleLabelFile1, zoomLevel);
	
		MicroarrayComparativeImage mi2 = new MicroarrayComparativeImage(
			tempDir, organism2, "blue-black-yellow");
		
		String imageFile2 = mi2.sf.getPath(sessionID2+"_image.png");
		String headerFile2 = mi2.sf.getPath(sessionID2+"_header_image.png");
		String dendrogramFile2 = mi2.sf.getPath(sessionID2+"_dendrogram_image.png");
		String sampleLabelFile2 = mi2.sf.getPath(sessionID2+"_sampleLabel_image.png");
		String geneNameFile2 = mi2.sf.getPath(sessionID2+"_geneName_image.png");
		displayDatasetKeywords = false;
		dStart2 = 0;
		dEnd2 = 2;
		normalize=true;

		mi2.ReadMap(displayDatasetKeywords);
		mi2.SampleOrder(sessionID2,
			0, 50, dStart2, dEnd2,
			false, //is query or not
			normalize, //normalize expression or not
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);

		gStart2 = 0;
		gEnd2 = 100;
		sortSample = true;
		down = -2.0f; //clip
		up = 2.0f; //clip
		mi2.Load(sessionID2,
			gStart2, gEnd2, dStart2, dEnd2,
			down, up,
			normalize, //normalize expression or not
			sortSample, //sort Samples 
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);
		mi2.paint(headerFile2, dendrogramFile2, geneNameFile2, imageFile2, sampleLabelFile2, zoomLevel);

		String combinedImageFile = mi1.sf.getPath(sessionID1 + "_combined_image.png");
		String combinedHeaderFile = mi1.sf.getPath(sessionID1 + "_combined_header_image.png");
		String combinedDendroFile = mi1.sf.getPath(sessionID1 + "_combined_dendrogram_image.png");
		mi1.paint(mi2, combinedImageFile, zoomLevel);


		MicroarrayComparativeImage mi1_q = new MicroarrayComparativeImage(
			tempDir, organism1, "blue-black-yellow");
		MicroarrayComparativeImage mi2_q = new MicroarrayComparativeImage(
			tempDir, organism2, "blue-black-yellow");
		mi1_q.ReadMap(displayDatasetKeywords);
		mi2_q.ReadMap(displayDatasetKeywords);
		mi1_q.LoadQuery(sessionID1, 
			dStart1, dEnd1, 
			down, up, 
			normalize, 
			sortSample, false, -1.0f, cp.GetQueryOrthologGroups(), true, false);

		imageFile1 = mi1.sf.getPath(sessionID1+"_query_image.png");
		headerFile1 = mi1.sf.getPath(sessionID1+"_query_header_image.png");
		dendrogramFile1 = mi1.sf.getPath(sessionID1+"_query_dendrogram_image.png");
		sampleLabelFile1 = mi1.sf.getPath(sessionID1+"_query_sampleLabel_image.png");
		geneNameFile1 = mi1.sf.getPath(sessionID1+"_query_geneName_image.png");
		mi1_q.paint(headerFile1, dendrogramFile1, geneNameFile1, imageFile1, 
			sampleLabelFile1, zoomLevel);

		mi2_q.LoadQuery(sessionID2, 
			dStart2, dEnd2, 
			down, up, 
			normalize, 
			sortSample, false, -1.0f, cp.GetQueryOrthologGroups(), true, false);

		imageFile2 = mi2.sf.getPath(sessionID2+"_query_image.png");
		headerFile2 = mi2.sf.getPath(sessionID2+"_query_header_image.png");
		dendrogramFile2 = mi2.sf.getPath(sessionID2+"_query_dendrogram_image.png");
		sampleLabelFile2 = mi2.sf.getPath(sessionID2+"_query_sampleLabel_image.png");
		geneNameFile2 = mi2.sf.getPath(sessionID2+"_query_geneName_image.png");
		mi2_q.paint(headerFile2, dendrogramFile2, geneNameFile2, imageFile2, 
			sampleLabelFile2, zoomLevel);

		//String combinedQueryFile = mi1.sf.getPath(sessionID1 + "_combined_query_image.png");
		//mi1_q.paint(mi2_q, combinedQueryFile, zoomLevel);


	}
}
