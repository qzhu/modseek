package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.*;
import seek.Pair;
import seek.ReadScore;

public class CompareList{
	String file_worm_fly;
	String file_worm_yeast;
	String file_fly_yeast;
	String file_zebrafish_worm;
	String file_zebrafish_fly;
	String file_zebrafish_yeast;
	String file_human_yeast;
	String file_human_worm;
	String file_human_fly;
	String file_human_zebrafish;
	String file_mouse_yeast;
	String file_mouse_worm;
	String file_mouse_fly;
	String file_mouse_zebrafish;
	String file_mouse_human;

	String organism1;
	String organism2;

	Vector<String> list1;
	Vector<String> list2;

	public Vector<String> query1;
	public Vector<String> query2;

	//For doing enrichment
	public Vector<String> final_list;
	public Vector<String> final_background;
	public String final_organism;

	//Return string for GetDistanceServlet
	public Vector<String> stt;

	public CompareList(){
		final_list = new Vector<String>();
		final_background = new Vector<String>();
		final_organism = "";
	}

	public class RankingScore{
		public Vector<String> genes;
		public Vector<Double> scores;
	}

	public class Similarity{
		public String gene;
		public float value1; //similarity value
		public float value2; //similarity value
		public Similarity(String a, float b, float c){
			this.gene = a;
			this.value1 = b;
			this.value2 = c;
		}
	}

	public class MyComparator implements Comparator<Similarity>{
		public int compare(Similarity a, Similarity b){
			if(a.value1<b.value1){
				return 1;
			}else if(a.value1>b.value1){
				return -1;
			}else if(a.value2<b.value2){
				return 1;
			}else if(a.value2>b.value2){
				return -1;
			}else{
				return 0;
			}
		}
	}

	public class MyComparatorDecreasing implements Comparator<Similarity>{
		public int compare(Similarity a, Similarity b){
			if(a.value1>b.value1){
				return 1;
			}else if(a.value1<b.value1){
				return -1;
			}else if(a.value2>b.value2){
				return 1;
			}else if(a.value2<b.value2){
				return -1;
			}else{
				return 0;
			}
		}
	}

	Map<String, Map<String,Vector<Similarity> > > m;

	public void ReadMap(String file, String org1, String org2) throws IOException{
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			Map<String,Vector<Similarity> > fmap = new HashMap<String,Vector<Similarity> >();
			Map<String,Vector<Similarity> > rmap = new HashMap<String,Vector<Similarity> >();
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "\t");
				String g1 = st.nextToken();
				String g2 = st.nextToken();
				float f1 = Float.parseFloat(st.nextToken());
				float f2 = Float.parseFloat(st.nextToken());
				Vector<Similarity> vs1 = null;
				Vector<Similarity> vs2 = null;
				if((vs1 = fmap.get(g1))==null)
					vs1 = new Vector<Similarity>();
				if((vs2 = rmap.get(g2))==null)
					vs2 = new Vector<Similarity>();
				vs1.add(new Similarity(g2, f1, f2));
				vs2.add(new Similarity(g1, f2, f1));
				fmap.put(g1, vs1);
				rmap.put(g2, vs2);
			}
			m.put(org1 + "_" + org2, fmap);
			m.put(org2 + "_" + org1, rmap);
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + file);
		}
	}
	
	public void Prepare(String tempDir, String org1, String org2) throws IOException{
		String t = tempDir;
		file_worm_fly = t + "/" + "correct.celegans.fly.txt";
		file_worm_yeast = t + "/" + "correct.celegans.yeast.txt";
		file_fly_yeast = t + "/" + "correct.fly.yeast.txt";
		file_zebrafish_worm = t + "/" + "correct.zebrafish.celegans.txt";
		file_zebrafish_fly = t + "/" + "correct.zebrafish.fly.txt";
		file_zebrafish_yeast = t + "/" + "correct.zebrafish.yeast.txt";
		file_human_yeast = t + "/" + "correct.human.yeast.txt";
		file_human_worm = t + "/" + "correct.human.celegans.txt";
		file_human_fly = t + "/" + "correct.human.fly.txt";
		file_human_zebrafish = t + "/" + "correct.human.zebrafish.txt";
		file_mouse_yeast = t + "/" + "correct.mouse.yeast.txt";
		file_mouse_worm = t + "/" + "correct.mouse.celegans.txt";
		file_mouse_fly = t + "/" + "correct.mouse.fly.txt";
		file_mouse_zebrafish = t + "/" + "correct.mouse.zebrafish.txt";
		file_mouse_human = t + "/" + "correct.mouse.human.txt";
		m = new HashMap<String, Map<String,Vector<Similarity> > >();
		Set<String> orgSet = new HashSet<String>();
		orgSet.add(org1);
		orgSet.add(org2);
		if(orgSet.contains("worm") && orgSet.contains("fly"))
			ReadMap(file_worm_fly, "worm", "fly");
		else if(orgSet.contains("worm") && orgSet.contains("yeast"))
			ReadMap(file_worm_yeast, "worm", "yeast");
		else if(orgSet.contains("fly") && orgSet.contains("yeast"))
			ReadMap(file_fly_yeast, "fly", "yeast");
		else if(orgSet.contains("zebrafish") && orgSet.contains("worm"))
			ReadMap(file_zebrafish_worm, "zebrafish", "worm");
		else if(orgSet.contains("zebrafish") && orgSet.contains("fly"))
			ReadMap(file_zebrafish_fly, "zebrafish", "fly");
		else if(orgSet.contains("zebrafish") && orgSet.contains("yeast"))
			ReadMap(file_zebrafish_yeast, "zebrafish", "yeast");
		else if(orgSet.contains("human") && orgSet.contains("yeast"))
			ReadMap(file_human_yeast, "human", "yeast");
		else if(orgSet.contains("human") && orgSet.contains("worm"))
			ReadMap(file_human_worm, "human", "worm");
		else if(orgSet.contains("human") && orgSet.contains("fly"))
			ReadMap(file_human_fly, "human", "fly");
		else if(orgSet.contains("human") && orgSet.contains("zebrafish"))
			ReadMap(file_human_zebrafish, "human", "zebrafish");
		else if(orgSet.contains("mouse") && orgSet.contains("yeast"))
			ReadMap(file_mouse_yeast, "mouse", "yeast");
		else if(orgSet.contains("mouse") && orgSet.contains("worm"))
			ReadMap(file_mouse_worm, "mouse", "worm");
		else if(orgSet.contains("mouse") && orgSet.contains("fly"))
			ReadMap(file_mouse_fly, "mouse", "fly");
		else if(orgSet.contains("mouse") && orgSet.contains("zebrafish"))
			ReadMap(file_mouse_zebrafish, "mouse", "zebrafish");
		else if(orgSet.contains("mouse") && orgSet.contains("human"))
			ReadMap(file_mouse_human, "mouse", "human");
	}

	public RankingScore GetGeneRanking(SeekFile sf, String sessionID)
	throws IOException{
		RankingScore rs = new RankingScore();
		String ext = "_gscore";
		String name_mapping = sf.gene_map_file;
		String name_mapping_2 = sf.gene_entrez_map_file;
		//System.out.println("Name mapping 1 is " + name_mapping);
		//System.out.println("Name mapping 2 is " + name_mapping_2);
		Map<String,Integer> mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
		File tempFile = new File(sf.getPath(sessionID + ext));
		//System.out.println("tempFile: " + sessionID + " " + tempFile.getAbsolutePath());
		float[] sc = ReadScore.ReadScoreBinary(tempFile.getAbsolutePath());
		boolean negativeCor = ReadScore.isNegativeCor(sf.leading_path, sessionID);
		Vector<Pair> vp = ReadScore.SortScores(mm, sc, negativeCor);
		Vector<String> allGenes = new Vector<String>();
		Vector<Double> allScores = new Vector<Double>();
		for(int i=0; i<vp.size(); i++){
			String term = vp.get(i).term;
			allGenes.add(term);
			allScores.add(vp.get(i).val);
		}
		rs.genes = allGenes;
		rs.scores = allScores;
		return rs;
	}

	public Vector<String> GetQuery(SeekFile sf, String sessionID)
	throws IOException{
		Vector<String> vs = new Vector<String>();
		String ext = "_query";
		Map<String, String> ent_symbol = new HashMap<String, String>();
		Map<String, String> symbol_ent = new HashMap<String, String>();
		ReadScore.readGeneEntrezSymbolMapping(sf.gene_entrez_map_file, ent_symbol, symbol_ent);
		File tempFile = new File(sf.getPath(sessionID + ext));
		try{
			BufferedReader in = new BufferedReader(new FileReader(tempFile));
			String s = in.readLine();
			StringTokenizer st = new StringTokenizer(s, " ");
			while(st.hasMoreTokens()){
				String x = st.nextToken();
				vs.add(ent_symbol.get(x));
			}
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + tempFile);
		}
		return vs;
	}

	public void Compare(String organism1, String organism2,
		String sessionID1, String sessionID2, String tempDir, 
		String option, String show_list, boolean show_all, boolean show_other_not_significant) 
		throws IOException{

		this.organism1 = organism1;
		this.organism2 = organism2;
		
		SeekFile sf1 = new SeekFile(tempDir, organism1);
		SeekFile sf2 = new SeekFile(tempDir, organism2);

		//sessionID1 = sessionID1;
		//sessionID2 = sessionID2;

		boolean isCrossSpecies = false;
		if(!organism1.equals(organism2))
			isCrossSpecies = true;

		RankingScore rs1 = GetGeneRanking(sf1, sessionID1);
		RankingScore rs2 = GetGeneRanking(sf2, sessionID2);

		list1 = rs1.genes;
		list2 = rs2.genes;

		Vector<Double> score1 = rs1.scores;
		Vector<Double> score2 = rs2.scores;

		Map<String,Integer> rank_orig1 = new HashMap<String,Integer>();
		Map<String,Integer> rank_orig2 = new HashMap<String,Integer>();

		for(int i=0; i<list1.size(); i++)
			rank_orig1.put(list1.get(i), i+1);
		for(int i=0; i<list2.size(); i++)
			rank_orig2.put(list2.get(i), i+1);


		Set<String> s1 = new HashSet<String>(list1);
		Set<String> s2 = new HashSet<String>(list2);

		Vector<String> ll1 = null;
		Vector<String> ll2 = null;

		Map<String,Integer> rank1 = new HashMap<String,Integer>();
		Map<String,Integer> rank2 = new HashMap<String,Integer>();

		//String option = req.getParameter("option");
		//String show_list = req.getParameter("show_list"); //shared, exclusive_to_left, or exclusive_to_right

		float threshold = 0.1f;

		this.query1 = GetQuery(sf1, sessionID1);
		this.query2 = GetQuery(sf2, sessionID2);


		if(isCrossSpecies && (show_list.equals("exclusive_to_left") || show_list.equals("exclusive_to_right"))){
			Prepare(tempDir, organism1, organism2);
			ll1 = new Vector<String>();
			ll2 = new Vector<String>();

			String mname = organism1 + "_" + organism2;
			Map<String, Vector<Similarity> > ms = m.get(mname);
			for(int i=0; i<list1.size(); i++){
				String g = list1.get(i);
				Vector<Similarity> ss = null;
				if((ss=ms.get(g))==null){
					ll1.add(g);
				}
			}
			mname = organism2 + "_" + organism1;
			ms = m.get(mname);
			for(int i=0; i<list2.size(); i++){
				String g = list2.get(i);
				Vector<Similarity> ss = null;
				if((ss=ms.get(g))==null){
					ll2.add(g);
				}
			}
			for(int i=0; i<ll1.size(); i++)
				rank1.put(ll1.get(i), i+1);
			for(int i=0; i<ll2.size(); i++)
				rank2.put(ll2.get(i), i+1);

			this.stt = new Vector<String>(); //return string;
			Vector<String> lt = null;
			if(show_list.equals("exclusive_to_left")){
				stt.add(Integer.toString(ll1.size())); //output ll1 size
				lt = ll1;
				final_organism = organism1;
			}else if(show_list.equals("exclusive_to_right")){
				stt.add(Integer.toString(ll2.size())); //output ll2 size
				lt = ll2;
				final_organism = organism2;
			}

			Vector<String> stu = new Vector<String>();
			for(int i=0; i<lt.size(); i++){
				float k = (float)(i+1) / (float)(lt.size());
				final_background.add(lt.get(i));
				if(option.equals("up")){
					if(k>threshold) continue;
				}else if(option.equals("down")){
					if(k<1.0f-threshold) continue;
				}
				String g = lt.get(i);
				String rs = g + " " + String.format("%.3f", k);
				stu.add(rs);
				final_list.add(g);
			}
			if(option.equals("down")){
				Collections.reverse(stu);
				Collections.reverse(final_list);
			}
			stt.add("-1"); //average sequence similarity (-1: NA)
			stt.addAll(stu);
		}
		else if(!isCrossSpecies){ //show_list == shared OR isCrossSpecies == false

			if(option.equals("sum") || option.equals("diff")){ //get the sum or diff of two lists (within species)
				Map<String,Double> sd = new HashMap<String,Double>();
				Set<String> temp1 = new HashSet<String>(list1);
				Set<String> temp2 = new HashSet<String>(list2);
				Set<String> geneShared = new HashSet<String>();
				geneShared.addAll(temp1);
				geneShared.retainAll(temp2);

				for(int i=0; i<list1.size(); i++){
					if(geneShared.contains(list1.get(i))){
						sd.put(list1.get(i), score1.get(i));
					}
				}
				for(int i=0; i<list2.size(); i++){
					if(geneShared.contains(list2.get(i))){
						double dx = 0;
						if(sd.containsKey(list2.get(i))){
							dx = sd.get(list2.get(i));
						}
						if(option.equals("sum")){
							sd.put(list2.get(i), dx + score2.get(i));
						}else if(option.equals("diff")){
							sd.put(list2.get(i), dx - score2.get(i));
						}
					}
				}
				Vector<Pair> vp = new Vector<Pair>();
				Vector<String> gShared = new Vector<String>(geneShared);
				for(int i=0; i<gShared.size(); i++){
					String gi = gShared.get(i);
					vp.add(new Pair(gi, sd.get(gi), i));
				}

				Collections.sort(vp);
				Collections.reverse(vp);

				//prepare the output
				int len = (int)(threshold * (float) vp.size());
				this.stt = new Vector<String>();
				stt.add(Integer.toString(len));
				stt.add("-1"); //l2 list size is -1 (NA), since there is only one list outputed
				Vector<String> stu = new Vector<String>();
				for(int i=0; i<len; i++){
					final_background.add(vp.get(i).term);
					String gg = vp.get(i).term;
					String fx = String.format("%.3f", (float)(vp.get(i).val));
					String rs = gg + " " + fx;
					stu.add(rs);
					final_list.add(gg);
				}
				final_organism = organism1;
				stt.add("-1"); //average sequence similarity (-1: NA)
				stt.addAll(stu);

			}

			//rank based comparison of two lists (within species)
			else if(option.equals("up_up") || option.equals("up_down") ||
			option.equals("down_down") || option.equals("down_up")){
				ll1 = list1;
				ll2 = list2;
				for(int i=0; i<list1.size(); i++)
					rank1.put(list1.get(i), i+1);
				for(int i=0; i<list2.size(); i++)
					rank2.put(list2.get(i), i+1);
				this.stt = new Vector<String>(); //return string;
				stt.add(Integer.toString(list1.size())); //output ll1 size
				stt.add(Integer.toString(list2.size())); //output ll2 size
				Vector<String> stu = new Vector<String>();
				for(int i=0; i<ll1.size(); i++){
					float k = (float)(i+1) / (float)(ll1.size());
					final_background.add(ll1.get(i));
					if(option.equals("up_up") || option.equals("up_down")){
						if(k>threshold) continue;
					}else if(option.equals("down_down") || option.equals("down_up")){
						if(k<1.0f-threshold) continue;
					}
					if(!rank2.containsKey(ll1.get(i))) continue;
					float k2 = (float)(rank2.get(ll1.get(i))) / (float)(ll2.size());
					if(option.equals("up_up") || option.equals("down_up")){
						if(!show_all && k2>threshold) continue;
						if(show_all && show_other_not_significant && k2<threshold) continue;
					}else if(option.equals("down_down") || option.equals("up_down")){
						if(!show_all && k2<1.0f-threshold) continue;
						if(show_all && show_other_not_significant && k2>1.0f-threshold) continue;
					}
					String gg = ll1.get(i);
					String fx = String.format("%.3f", (float)(rank2.get(gg)) / (float)(ll2.size()));
					String fy = String.format("%.0f", 100f);
					String sd = gg + " " + fx + " " + fy;
					String rs = gg + " " + String.format("%.3f", k) + "|" + sd;
					stu.add(rs);
					final_list.add(gg);
				}
				final_organism = organism1;
				if(option.equals("down_down") || option.equals("down_up")){
					Collections.reverse(stu);
					Collections.reverse(final_list);
				}
				stt.add("-1"); //average sequence similarity (-1: NA)
				stt.addAll(stu);
			}

		}else{
			Prepare(tempDir, organism1, organism2);
			ll1 = new Vector<String>();
			ll2 = new Vector<String>();

			String mname = organism1 + "_" + organism2;
			Map<String, Vector<Similarity> > ms = m.get(mname);
			for(int i=0; i<list1.size(); i++){
				String g = list1.get(i);
				Vector<Similarity> ss = null;
				if((ss=ms.get(g))==null) continue;
				for(int j=0; j<ss.size(); j++){
					String g2 = ss.get(j).gene;
					if(s2.contains(g2)){
						ll1.add(g);
						break;
					}
				}
			}
			mname = organism2 + "_" + organism1;
			ms = m.get(mname);
			for(int i=0; i<list2.size(); i++){
				String g = list2.get(i);
				Vector<Similarity> ss = null;
				if((ss=ms.get(g))==null) continue;
				for(int j=0; j<ss.size(); j++){
					String g2 = ss.get(j).gene;
					if(s1.contains(g2)){
						ll2.add(g);
						break;
					}
				}
			}
			for(int i=0; i<ll1.size(); i++)
				rank1.put(ll1.get(i), i+1);
			for(int i=0; i<ll2.size(); i++)
				rank2.put(ll2.get(i), i+1);
			mname = organism1 + "_" + organism2;
			ms = m.get(mname);

			this.stt = new Vector<String>(); //return string;
			stt.add(Integer.toString(ll1.size())); //output ll1 size
			stt.add(Integer.toString(ll2.size())); //output ll2 size
			Vector<String> stu = new Vector<String>();
			float averageSimilarity = 0;
			int numSimilarity = 0;
			for(int i=0; i<ll1.size(); i++){
				//float k = (float)(i+1) / (float)(ll1.size());
				float k = (float) (rank_orig1.get(ll1.get(i))) / (float) (list1.size());
				final_background.add(ll1.get(i));
				if(option.equals("up_up") || option.equals("up_down")){
					if(k>threshold) continue;
				}else if(option.equals("down_down") || option.equals("down_up")){
					if(k<1.0f-threshold) continue;
				}
				String g = ll1.get(i);
				Vector<Similarity> ss = ms.get(g);
				Vector<Similarity> sx = new Vector<Similarity>();
				for(int j=0; j<ss.size(); j++){
					String g2 = ss.get(j).gene;
					if(s2.contains(g2)){
						float k2 = (float) (rank_orig2.get(g2)) / (float) (list2.size());
						//float k2 = (float)(rank2.get(g2)) / (float)(ll2.size());
						if(option.equals("up_up") || option.equals("down_up")){
							if(!show_all && k2>threshold) continue;
							if(show_all && show_other_not_significant && k2<threshold) continue;
						}else if(option.equals("down_down") || option.equals("up_down")){
							if(!show_all && k2<1.0f-threshold) continue;
							if(show_all && show_other_not_significant && k2>1.0f-threshold) continue;
						}
						//if(k2>threshold) continue; 
						//if(k2<threshold2) continue; 
						sx.add(ss.get(j));
					}
				}
				if(sx.size()==0) continue;
				Collections.sort(sx, new MyComparator());
				String[] sd = new String[sx.size()];
				for(int j=0; j<sx.size(); j++){
					String gg = sx.get(j).gene;
					//String fx = String.format("%.3f", (float)(rank2.get(gg)) / (float)(ll2.size()));
					String fx = String.format("%.3f", (float)(rank_orig2.get(gg)) / (float)(list2.size()));
					String fy = String.format("%.0f", sx.get(j).value1); //sx.get(j).value is the sequence similarity
					sd[j] = gg + " " + fx + " " + fy;
					averageSimilarity += sx.get(j).value1;
					numSimilarity++;
				}
				String rs = g + " " + String.format("%.3f", k) + "|" + StringUtils.join(sd, ";");
				stu.add(rs);
				final_list.add(g);
			}
			averageSimilarity /= (float) numSimilarity;

			final_organism = organism1;
			if(option.equals("down_down") || option.equals("down_up")){
				Collections.reverse(stu);
				Collections.reverse(final_list);
			}
			if(show_all){
				stt.add("-1"); //average sequence similarity (-1: NA) if showing all genes (even non-conserved)
			}else{
				stt.add(String.format("%.1f", averageSimilarity));
			}
			stt.addAll(stu);
		}
	}

}
