package seek;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarrayComparativeImage extends SeekImage{
	public class ExpressionMatrix{
		float[][][] mat;
		float[][][] gmat;
		public ExpressionMatrix(float[][][] mat, float[][][] gmat){
			this.mat = mat;
			this.gmat = gmat;
		}
	}
	BufferedImage image;
    BufferedImage header_image;
	BufferedImage dendrogram_image;
	int dendro_height;

	Vector<OrthologGroup> og;
	Vector<String> geneWithSpace;
	Map<Integer, Integer> mapGene;
	Map<Integer, Integer> mapOrthologGroup;

	MicroarraySamples microarray_samples;
	MicroarrayGenes microarray_genes;

	String dsetTitleDir;
	String gene_order_file;

	Vector<String> dataset_annot;
	int dataset_start;

	boolean isQuery;
	boolean useReference;
	boolean sortSamples;

    float[][][] matrix; //used in init()
    float[][][] norm_matrix; //used in init()
	float[][][] tmp_norm_matrix; //used in init()
	int[][] sample_order;
	int[] gene_order;

    int[][][] color_red;
    int[][][] color_green;
    int[][][] color_blue;

	boolean normalizeExpr;
	float upper_clip;
	float lower_clip;

	int column_header_height = 65;
	int geneNameWidth = 80;
	boolean bPrintGeneName = true;
	//int horizontalUnitWidth = 10;
	int horizontalUnitWidth = 2;
	int sampleLabelWidth = 200;
	int headerVerticalLength = 300;

	public MicroarrayComparativeImage(String tempDir, String organism, String colorChoice) throws IOException{
		super(tempDir, organism, colorChoice);
	}
	public int[][] GetSampleOrder(){
		return this.microarray_samples.GetSampleOrder();
	}
	public float[][] GetAverageExpression(){
		return this.microarray_samples.GetAverageExpression(); //in sample order
	}
    protected int calculateWidth(int s){
    	return s*calculateUnitWidth(s);
    }
	public int[] GetDatasetSize(){
		return size;
	}
	public float GetLowerClip(){
		return lower_clip;
	}
	public float GetUpperClip(){
		return upper_clip;
	}
    protected int calculateUnitWidth(int s){
    	return horizontalUnitWidth;
    }
    protected void calculateSize(){
    	int x = 5;
    	int y = 0;
    	for(int i=0; i<numDatasetToShow; i++)
    		x+=calculateWidth(size[i]) + horizontalDatasetSpacing;
    	//x+=10;
    	//x-=horizontalDatasetSpacing;
    	y += verticalUnitHeight*numGeneToShow;
		if(bPrintGeneName){
			windowWidth = x + geneNameWidth;
		}else{
	    	windowWidth = x;
		}
    	//windowWidth = x;
    	windowHeight = y;
    }
    public void save(String name, String column_header_name){
    	File f = new File(name);
    	File f2 = new File(column_header_name);
    	try{
    		ImageIO.write(image, "png", f);
    		ImageIO.write(header_image, "png", f2);
    	}catch(IOException e){	
    	}
    }
    public void init(ExpressionMatrix em, boolean isQuery) throws IOException{
        matrix = em.mat;
		int nD = matrix.length;

        int i, j, k;
		color_red = new int[nD][][];
		color_green = new int[nD][][];
		color_blue = new int[nD][][];
		this.tmp_norm_matrix = new float[nD][][];
		this.size = new int[nD];

		//norm_matrix dimension: dataset, genes, samples        
		for(i=0; i<nD; i++){ //dataset
			int nS = matrix[i].length; //number of samples
			this.tmp_norm_matrix[i] = new float[nS][];
			color_red[i] = new int[nS][];
			color_green[i] = new int[nS][];
			color_blue[i] = new int[nS][];
        	this.size[i] = nS;  //number of column
        	for(j=0; j<nS; j++){
				//int nG = matrix[i][j].length; //number of genes
				int nG = geneWithSpace.size();
        		color_red[i][j] = new int[nG];
        		color_green[i][j] = new int[nG];
        		color_blue[i][j] = new int[nG];
        		this.tmp_norm_matrix[i][j] = new float[nG];
				for(k=0; k<nG; k++){
					this.tmp_norm_matrix[i][j][k] = 328.f;
				}
        	}
			int nG = matrix[i][0].length;
        	for(j=0; j<nG; j++){ //number of genes
        		float[] mm = new float[size[i]];
        		for(k=0; k<size[i]; k++){ //for each sample
        			mm[k] = matrix[i][k][j];
        		}
        		for(k=0; k<size[i]; k++){
      		  		this.tmp_norm_matrix[i][k][mapGene.get(j)] = mm[k];
        		}
        	}
        }
		//if(isQuery){
			//read gene order
			//where matrix[0][0].length is number of query genes
		//	this.gene_order = MicroarrayGenes.ReadGeneOrder(this.gene_order_file, matrix[0][0].length); 
		//}
		if(sortSamples){
			//read sample order
			//matrix is used to get dimension of sample_order
			this.sample_order = MicroarraySamples.ReadSampleOrder(this.dset_order_file, matrix); 
			//once order is read, sort samples based on this order
			//if(isQuery){
			//	this.norm_matrix = MicroarraySamples.SortGenesSamples(this.sample_order, this.gene_order, this.tmp_norm_matrix);
			//}else{
				this.norm_matrix = MicroarraySamples.SortSamples(this.sample_order, this.tmp_norm_matrix);
			//}
		}
		else{
			sample_order = new int[nD][];
			for(i=0; i<nD; i++){
				int nS = matrix[i].length;
				sample_order[i] = new int[nS];
			}
			for(i=0; i<nD; i++){
				Vector<Pair> np = new Vector<Pair>();
				Vector<Pair> np2 = new Vector<Pair>();
				int num = 0;
				int nS = matrix[i].length;
				for(j=0; j<nS; j++){
					float sum = 0;
					num = 0;
					int nG = geneWithSpace.size();
					for(k=0; k<nG; k++){
        				if(tmp_norm_matrix[i][j][k]>327.0f) continue;
						sum+=tmp_norm_matrix[i][j][k];
						num++;
					}
					Pair p = new Pair(Integer.toString(j), (double) sum, -1);
					Pair p2 = new Pair(Integer.toString(j), (double) j, -1);
					np.add(p);
					np2.add(p2);
				}
				for(j=0; j<np.size(); j++){
					sample_order[i][j] = Integer.parseInt(np2.get(j).term);
				}
			}
			//if(isQuery){
			//	this.norm_matrix = MicroarraySamples.SortGenesSamples(this.sample_order, this.gene_order, this.tmp_norm_matrix);
			//}else{
				this.norm_matrix = MicroarraySamples.SortSamples(this.sample_order, this.tmp_norm_matrix);
			//}
		}

		int max_size = red.length;
		float unit_length = (upper_clip - lower_clip) / (float) max_size;
		float NaN = 9999;
		for(i=0; i<matrix.length; i++){
        	for(j=0; j<matrix[i].length; j++){
        		for(k=0; k<geneWithSpace.size(); k++){        	
        			color_red[i][j][k] = neutral_red;
        			color_green[i][j][k] = neutral_green;
        			color_blue[i][j][k] = neutral_blue;
        			if(norm_matrix[i][j][k]>327.0f){
        				color_green[i][j][k] = neutral_red;
        				color_red[i][j][k] = neutral_green;
        				color_blue[i][j][k] = neutral_blue;
        				continue;
        			}
					if(norm_matrix[i][j][k]>upper_clip){
						color_red[i][j][k] = red[max_size-1];
						color_green[i][j][k] = green[max_size-1];
						color_blue[i][j][k] = blue[max_size-1];
						continue;
					}
					if(norm_matrix[i][j][k]<lower_clip){
						color_red[i][j][k] = red[0];
						color_green[i][j][k] = green[0];
						color_blue[i][j][k] = blue[0];
						continue;
					}
					int level = (int) Math.round((float)(1.0f*(norm_matrix[i][j][k] - lower_clip) / unit_length));
					if(level<0) level=0;
					else if(level>=max_size) level=max_size-1;
					color_red[i][j][k] = red[level];
					color_green[i][j][k] = green[level];
					color_blue[i][j][k] = blue[level];
        		}
        	}
        }
        names = new String[geneWithSpace.size()];
        for(i=0; i<names.length; i++){
        	if(geneWithSpace.get(i).equals("empty")){
				names[i] = "---";
			}else{
				names[i] = map_genes.get(geneWithSpace.get(i));
			}
        }
		/*
		if(isQuery){
			String[] new_names = new String[genes.size()];
			Vector<String> new_genes = new Vector<String>();
			for(i=0; i<new_names.length; i++){
				new_names[i] = names[gene_order[i]];
				new_genes.add(genes.get(gene_order[i]));
			}
			names = new_names;
			genes = new_genes;
		}
		*/
        description = new String[numDatasetToShow];

		for(k=0; k<numDatasetToShow; k++){
			//String stem = getStem(dsets.get(k));
			String stem = ReadScore.getDatasetID(dsets.get(k));
			description[k] = map_datasets.get(stem);
		}

		System.out.println("init num genes " + names.length + " num genes to show " + numGeneToShow);
        //calculateSize();
        //image = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);
        //header_image = new BufferedImage(windowWidth, column_header_height, BufferedImage.TYPE_INT_RGB);
    }

	public void CalculateDendrogramHeight(){
		TreeNode[][] dendro_tree = this.microarray_samples.GetDendroTree();
		float[] dendro_max_depth = this.microarray_samples.GetDendrogramMaxDepth();
		float temp_height = dendro_max_depth[0];
		for(int i=1; i < this.numDatasetToShow; i++){
			if(temp_height < dendro_max_depth[i]){
				temp_height = dendro_max_depth[i];
			}
		}
		this.dendro_height = (int) (3 * temp_height + 2);
	}

	public void paint_dendrogram(String dendrogram_file){
		this.calculateSize();
		this.CalculateDendrogramHeight();

		this.dendrogram_image = new BufferedImage(this.windowWidth, this.dendro_height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g4 = dendrogram_image.createGraphics();
		g4.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	
		MicroarrayComparativePainter.paint_dendrogram_core(0, 0, this.dendro_height, g4, this);
    	File f = new File(dendrogram_file);
    	try{
    		ImageIO.write(dendrogram_image, "png", f);
    	}catch(IOException e){
    	}
	}

    public void paint() {
    }

	//paint as separate images
	public void paint(String headerFile, String dendroFile, String geneNameFile, String expressionFile, 
	String sampleLabelFile, int zoomLevel){
		int i, j, k, x, y;	
		boolean doSampleLabel = false;
		boolean doDendrogram = true;
		boolean doHeader = true;
		boolean doHeaderVertical = false;
		Graphics2D g2, g3;
		if(zoomLevel==3){
			this.horizontalUnitWidth = 10;
			doDendrogram = true;
			doSampleLabel = true;
		}else if(zoomLevel==2){
			this.horizontalUnitWidth = 4;
			doDendrogram = true;
			doSampleLabel = false;
		}else if(zoomLevel==1){
			this.horizontalUnitWidth = 2;
			doDendrogram = false;
			doSampleLabel = false;
		}else if(zoomLevel==0){
			this.horizontalUnitWidth = 1;
			doDendrogram = false;
			doSampleLabel = false;
			doHeaderVertical = true;
		}
		if(this.isQuery){
			doDendrogram = false;
			doSampleLabel = false;
		}
		this.bPrintGeneName = false;
		this.calculateSize();
		int dendro_canvas_height = 0;
		if(doDendrogram){
			this.CalculateDendrogramHeight();
		}
		//expression image
        this.image = new BufferedImage(this.windowWidth, 
			this.windowHeight, BufferedImage.TYPE_INT_RGB);
		g2 = this.image.createGraphics();
		MicroarrayComparativePainter.paint_expression(0, 0, g2, this);
		MicroarrayComparativePainter.paint_ortholog_boundary(0, 0, g2, this, this.windowWidth);
		File f = null;
    	try{
			f = new File(expressionFile);
    		ImageIO.write(this.image, "png", f);
		}catch(IOException e){
			System.err.println("Error writing file" + expressionFile);
		}

        this.image = new BufferedImage(this.geneNameWidth, 
			this.windowHeight, BufferedImage.TYPE_INT_RGB);
		g2 = this.image.createGraphics();
		MicroarrayComparativePainter.paint_gene_name(0, 0, g2, this);
		MicroarrayComparativePainter.paint_ortholog_boundary(0, 0, g2, this, this.geneNameWidth);
    	try{
			f = new File(geneNameFile);
    		ImageIO.write(this.image, "png", f);
		}catch(IOException e){
			System.err.println("Error writing file" + geneNameFile);
		}

		if(doDendrogram){
	        this.image = new BufferedImage(this.windowWidth, 
				this.dendro_height, BufferedImage.TYPE_INT_RGB);
			g3 = this.image.createGraphics();
			MicroarrayComparativePainter.paint_dendrogram_core(0, 0, this.dendro_height, g3, this);
			try{
    			f = new File(dendroFile);
    			ImageIO.write(this.image, "png", f);
			}catch(IOException e){
				System.err.println("Error writing file" + dendroFile);
			}
		}

		if(doHeader){
	        this.image = new BufferedImage(this.windowWidth, 
				this.column_header_height, BufferedImage.TYPE_INT_RGB);
			g3 = this.image.createGraphics();
			MicroarrayComparativePainter.paint_header(0, 0, g3, this);
    		try{
				f = new File(headerFile);
    			ImageIO.write(this.image, "png", f);
			}catch(IOException e){
				System.err.println("Error writing file" + headerFile);
			}
		}else if(doHeaderVertical){
	        this.image = new BufferedImage(this.windowWidth, 
				this.headerVerticalLength, BufferedImage.TYPE_INT_RGB);
			g3 = this.image.createGraphics();
        	g3.setPaint(this.bg);
 	      	g3.fill(new Rectangle2D.Double(0, 0, this.windowWidth, this.headerVerticalLength));
    	    g3.setPaint(this.fg);
			int offset_vert = -1 * this.headerVerticalLength;
			int offset_horiz = 0;
			MicroarrayComparativePainter.paint_header_vertical(offset_vert, offset_horiz, g3, this);
    		try{
				f = new File(headerFile);
    			ImageIO.write(this.image, "png", f);
			}catch(IOException e){
				System.err.println("Error writing file" + headerFile);
			}
		}

		if(doSampleLabel){
	        this.image = new BufferedImage(this.windowWidth, 
				this.sampleLabelWidth, BufferedImage.TYPE_INT_RGB);
			g3 = this.image.createGraphics();
        	g3.setPaint(this.bg);
 	      	g3.fill(new Rectangle2D.Double(0, 0, this.windowWidth, this.sampleLabelWidth));
    	    g3.setPaint(this.fg);
			int offset_vert = -1 * this.sampleLabelWidth;
			int offset_horiz = 0;
			MicroarrayComparativePainter.paint_label(offset_vert, offset_horiz, g3, this);
    		try{
				f = new File(sampleLabelFile);
    			ImageIO.write(this.image, "png", f);
			}catch(IOException e){
				System.err.println("Error writing file" + sampleLabelFile);
			}
		}
	}


	//paint a combined image (with header, dendrogram, expression, sample label, gene labels)
	//zoom level: 1 (least detailed), 2, 3 (most detailed)
	public void paint(MicroarrayComparativeImage mi2, String imageFile, int zoomLevel) {
        int i, j, k;
		int x, y;
		boolean doSampleLabel = false;
		boolean doDendrogram = true;
		boolean doHeader = true;
		boolean doHeaderVertical = false;
		this.bPrintGeneName = true;
		mi2.bPrintGeneName = true;

		if(zoomLevel==3){
			this.horizontalUnitWidth = 10;
			doDendrogram = true;
			doSampleLabel = true;
		}else if(zoomLevel==2){
			this.horizontalUnitWidth = 4;
			doDendrogram = true;
			doSampleLabel = false;
		}else if(zoomLevel==1){
			this.horizontalUnitWidth = 2;
			doDendrogram = false;
			doSampleLabel = false;
		}else if(zoomLevel==0){
			this.horizontalUnitWidth = 1;
			doDendrogram = false;
			doSampleLabel = false;
			doHeaderVertical = true;
		}

		if(mi2.isQuery){
			doDendrogram = false;
			doSampleLabel = false;
			//doHeader = false;
			//doHeaderVertical = false;
		}

		mi2.horizontalUnitWidth = this.horizontalUnitWidth;

		int dendro_canvas_height = 0;

		if(doDendrogram){
			this.CalculateDendrogramHeight();
			mi2.CalculateDendrogramHeight();
			dendro_canvas_height = Math.max(this.dendro_height, mi2.dendro_height);
		}

		this.calculateSize();
		mi2.calculateSize();

		/*		
		dendrogram_image = new BufferedImage(this.windowWidth + mi2.windowWidth, 
			dendro_canvas_height, BufferedImage.TYPE_INT_RGB);
        image = new BufferedImage(windowWidth + mi2.windowWidth, 
			windowHeight, BufferedImage.TYPE_INT_RGB);
        header_image = new BufferedImage(windowWidth + mi2.windowWidth, 
			column_header_height, BufferedImage.TYPE_INT_RGB);

		Graphics2D g4 = dendrogram_image.createGraphics();
		Graphics2D g2 = image.createGraphics();
		Graphics2D g3 = header_image.createGraphics();

		//paint this dendrogram	
		this.paint_dendrogram_core(0, 0, dendro_canvas_height, g4, this);
		//paint the other dendrogram
		this.paint_dendrogram_core(windowWidth, 0, dendro_canvas_height, g4, mi2);

		this.paint_header(0, 0, g3, this);
		//draw the other dataset
		this.paint_header(windowWidth, 0, g3, mi2);
		this.paint_expression(0, 0, g2, this);
		this.paint_expression(windowWidth, 0, g2, mi2);
		*/
		int newHeight = this.windowHeight;
		int extraHeight = 0;
		if(doHeaderVertical){
			extraHeight += this.headerVerticalLength;
		}else if(doHeader){
			extraHeight += this.column_header_height;
		}
		if(doDendrogram){
			extraHeight += dendro_canvas_height;
		}
		if(doSampleLabel){
			extraHeight += this.sampleLabelWidth;
		}

		newHeight += extraHeight;
        this.image = new BufferedImage(this.windowWidth + mi2.windowWidth, 
			newHeight, BufferedImage.TYPE_INT_RGB);
		Graphics2D g2 = this.image.createGraphics();
		int offset_y = 0;

		if(doHeaderVertical){
        	g2.setPaint(this.bg);
 	      	g2.fill(new Rectangle2D.Double(0, offset_y, this.windowWidth + mi2.windowWidth, this.headerVerticalLength));
    	    g2.setPaint(this.fg);
			int offset_vert = -1 * this.headerVerticalLength;
			int offset_horiz = 0;
			MicroarrayComparativePainter.paint_header_vertical(offset_vert, offset_horiz, g2, this);
			offset_horiz += this.windowWidth;
			MicroarrayComparativePainter.paint_header_vertical(offset_vert, offset_horiz, g2, mi2);
			offset_y += this.headerVerticalLength;
		}else if(doHeader){
			MicroarrayComparativePainter.paint_header(0, offset_y, g2, this);
			MicroarrayComparativePainter.paint_header(this.windowWidth, offset_y, g2, mi2);
			offset_y += this.column_header_height;
		}
		if(doDendrogram){
			MicroarrayComparativePainter.paint_dendrogram_core(0, offset_y, dendro_canvas_height, g2, this);
			MicroarrayComparativePainter.paint_dendrogram_core(this.windowWidth, offset_y, dendro_canvas_height, g2, mi2);
			offset_y += dendro_canvas_height;
		}
		if(doSampleLabel){
        	g2.setPaint(this.bg);
 	      	g2.fill(new Rectangle2D.Double(0, offset_y, this.windowWidth + mi2.windowWidth, this.sampleLabelWidth));
    	    g2.setPaint(this.fg);
			int offset_vert = -1 * extraHeight;
			int offset_horiz = 0;
			MicroarrayComparativePainter.paint_label(offset_vert, offset_horiz, g2, this);
			offset_horiz += this.windowWidth;
			MicroarrayComparativePainter.paint_label(offset_vert, offset_horiz, g2, mi2);
			offset_y += sampleLabelWidth;
		}

		MicroarrayComparativePainter.paint_expression(0, offset_y, g2, this);
		MicroarrayComparativePainter.paint_expression(this.windowWidth, offset_y, g2, mi2);

		y = offset_y;
		//y = 0;
		for(k=0; k<numGeneToShow; k++){
			if(k<numGeneToShow-1 && !geneWithSpace.get(k+1).equals("empty") &&
			this.mapOrthologGroup.get(k+1)!=this.mapOrthologGroup.get(k)){
				g2.setPaint(fg);
				g2.drawLine(5, y + verticalUnitHeight, this.windowWidth + mi2.windowWidth - horizontalDatasetSpacing - 10, 
					y + verticalUnitHeight);
			}
			y += verticalUnitHeight;
        }

    	File f = new File(imageFile);
    	try{
    		ImageIO.write(image, "png", f);
    	}catch(IOException e){	
    	}
    }

	public Vector<Vector<String> > GetSampleAnnotation() throws IOException{
		Vector<Vector<String> > sample_annot = new Vector<Vector<String> >();
		Map<String,String> dset_plat = this.ReadMap(sf.dataset_platform_file);
		String parsedGSM = sf.sample_description_dir;

		for(String s : dsets){
			Vector<String> mm = new Vector<String>();
			String dset_platform = dset_plat.get(s);

			String dset_name = ReadScore.getDatasetName(s);
			/*if(dset_platform.indexOf("GPL")==0){
				dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
			}else{
				dset_name = s;
			}*/
			//String dset_platform = StringUtils.split(s, ".")[1];
			//String dset_name = StringUtils.split(StringUtils.split(s, ".")[0], "_")[0];
			String dsetPath = parsedGSM + "/" + dset_platform + "/" + dset_name;
			try{
				BufferedReader in = new BufferedReader(new FileReader(dsetPath));
				String ss = null;
				Vector<String> ll = new Vector<String>();
				while((ss=in.readLine())!=null){
					ll.add(ss);
				}
				in.close();
				sample_annot.add(ll);
				//mm.add(StringUtils.join(ll, ";;"));
			}catch(IOException e){
				sample_annot.add(new Vector<String>());
				System.out.println("BAD I/O");
			}
		}

		return sample_annot;
	}

	public Map<String,String> ReadMap(String file) throws IOException{
		Map<String,String> m = new HashMap<String,String>();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String ss = null;
			while((ss=in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(ss, "\t");
				String s1 = st.nextToken();
				String s2 = st.nextToken();
				m.put(s1, s2);
			}
			in.close();
		}catch(IOException e){
			System.out.println("BAD I/O!");
		}
		return m;
	}

	//gStart and gEnd do not apply for query
	public boolean readQuery(String query_path, String dataset_path, String pval_path,
	int gStart, int gEnd, int dStart, int dEnd, 
	Vector<OrthologGroup> onlyGenes)
	//boolean filter_by_pval, float pvalCutoff, boolean negative_cor) 
	throws IOException{

		Map<String, Integer> mm = null;
		dataset_start = dStart;
		int numD = dEnd - dStart + 1;
		//int numG = gEnd - gStart + 1;

		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();
		dataset_annot = new Vector<String>(); //seems to be doing nothing
		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		this.og = new Vector<OrthologGroup>();
		this.geneWithSpace = new Vector<String>();
		this.mapGene = new HashMap<Integer,Integer>();
		this.mapOrthologGroup = new HashMap<Integer,Integer>();
		
		Scanner ss = null;
		float[] sc_pval = null;
		try{
			//map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(this.sf.gene_entrez_map_file);
			name_mapping = this.sf.dataset_platform_file;
			mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc, false);
                        
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart+i).val;
				if(fval==0.0f) continue;
				dataset_annot.add(ReadScore.getDatasetID(vp.get(dStart+i).term));
				dsets.add(vp.get(dStart + i).term);
				dataset_score.add(new Float(vp.get(dStart + i).val));
			}

			/*
			ss = new Scanner(new BufferedReader(new FileReader(query_path)));
			String s1 = ss.nextLine();
			StringTokenizer st = new StringTokenizer(s1);
			while(st.hasMoreTokens()){
				genes.add(st.nextToken());
			}
			ss.close();
			*/

			int organism_index = -1;
			if(onlyGenes.get(0).getOrganism(0).equals(this.organism)){
				organism_index = 0;
			}else{
				organism_index = 1;
			}

			for(int i=0; i<onlyGenes.size(); i++){
				OrthologGroup this_ortho = onlyGenes.get(i);
				this.genes.addAll(this_ortho.getOrthologs(organism_index));
				this.og.add(this_ortho);
			}
			for(int i=0; i<this.og.size(); i++){
				Vector<String> ortho = this.og.get(i).getOrthologs(organism_index);
				for(int j=0; j<ortho.size(); j++){
					this.geneWithSpace.add(ortho.get(j));
				}
				int size1 = this.og.get(i).getOrthologs(organism_index).size();
				int size2 = this.og.get(i).getOrthologs(1-organism_index).size();
				for(int j=0; j<size2 - size1; j++){
					this.geneWithSpace.add("empty");
				}
			}
			for(String g : this.genes){
				this.mapGene.put(this.genes.indexOf(g), this.geneWithSpace.indexOf(g));
			}
			for(int i=0; i<this.og.size(); i++){
				Vector<String> ortho = this.og.get(i).getOrthologs(organism_index);
				for(int j=0; j<ortho.size(); j++){
					String g = ortho.get(j);
					//mapOrthologGroup.put(geneWithSpace.indexOf(g), og.get(i).getID());
					this.mapOrthologGroup.put(this.geneWithSpace.indexOf(g), i);
				}
			}

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

		this.numDatasetToShow = dsets.size();
		this.numGeneToShow = geneWithSpace.size();
		System.out.println("Number of genes: " + numGeneToShow);	
		System.out.println("Number of dataset: " + numDatasetToShow);	

		return true;    
	}

	public boolean read(String gene_path, String dataset_path, 
	int oStart, int oEnd, int dStart, int dEnd,
	Vector<OrthologGroup> onlyGenes) throws IOException{
    		
		Map<String, Integer> mm = null;
		int numD = dEnd - dStart + 1;
		int numO = oEnd - oStart + 1; //number of orthologGroups

		dataset_start = dStart;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();
		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		pval_score = new Vector<Float>();
		dataset_annot = new Vector<String>();
		float[] sc_pval = null;

		this.og = new Vector<OrthologGroup>();
		this.geneWithSpace = new Vector<String>();
		this.mapGene = new HashMap<Integer,Integer>();
		this.mapOrthologGroup = new HashMap<Integer,Integer>();

		try{
			name_mapping = this.sf.dataset_platform_file;
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc, false);
			
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart+i).val;
				if(fval==0.0f) continue;
				dsets.add(vp.get(dStart + i).term);
				dataset_score.add(new Float(vp.get(dStart + i).val));
				dataset_annot.add(ReadScore.getDatasetID(vp.get(dStart+i).term));
			}
		
			int organism_index = -1;
			if(onlyGenes.get(0).getOrganism(0).equals(this.organism)){
				organism_index = 0;
			}else{
				organism_index = 1;
			}

			for(int i=0; i<numO; i++){
				if(oStart+i>=onlyGenes.size()) break;
				OrthologGroup this_ortho = onlyGenes.get(oStart + i);
				genes.addAll(this_ortho.getOrthologs(organism_index));
				og.add(this_ortho);
			}
			for(int i=0; i<og.size(); i++){
				Vector<String> ortho = og.get(i).getOrthologs(organism_index);
				for(int j=0; j<ortho.size(); j++){
					geneWithSpace.add(ortho.get(j));
				}
				int size1 = og.get(i).getOrthologs(organism_index).size();
				int size2 = og.get(i).getOrthologs(1-organism_index).size();
				for(int j=0; j<size2 - size1; j++){
					geneWithSpace.add("empty");
				}
			}
			for(String g : genes){
				mapGene.put(genes.indexOf(g), geneWithSpace.indexOf(g));
			}
			for(int i=0; i<og.size(); i++){
				Vector<String> ortho = og.get(i).getOrthologs(organism_index);
				for(int j=0; j<ortho.size(); j++){
					String g = ortho.get(j);
					//mapOrthologGroup.put(geneWithSpace.indexOf(g), og.get(i).getID());
					mapOrthologGroup.put(geneWithSpace.indexOf(g), i);
				}
			}

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}
    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = geneWithSpace.size();
    
		System.out.println("Number of genes: " + numGeneToShow);	
		System.out.println("Number of dataset: " + numDatasetToShow);	
		return true;    
    }
    
	public ExpressionMatrix Common(String sessionID, boolean isQuery,
	int oStart, int oEnd, int dStart, int dEnd,  
	boolean filterByPValue, float PValue, 
	Vector<OrthologGroup> onlyGenes, boolean is_gene_entrez, boolean negative_cor) 
	throws IOException{
    	Socket kkSocket = null;
		this.dset_order_file = this.sf.getPath(sessionID + "_dset_order");
		this.gene_order_file = this.sf.getPath(sessionID + "_gene_order");
		this.dsetTitleDir = this.sf.dataset_description_dir;
		if(isQuery){
			if(!this.readQuery(
				this.sf.getPath(sessionID+"_query"),
				this.sf.getPath(sessionID+"_dweight"),
				this.sf.getPath(sessionID+"_pval"),
				-1, -1, dStart, dEnd, onlyGenes)){
				//filterByPValue, PValue, negative_cor)){
				return null;
			}
		}else{
			if(!this.read(
				this.sf.getPath(sessionID+"_gscore"),
				this.sf.getPath(sessionID+"_dweight"),
				oStart, oEnd, dStart, dEnd, onlyGenes)){
				return null;
			}
			/*if(!this.read( 
				this.sf.getPath(sessionID+"_gscore"),
				this.sf.getPath(sessionID+"_dweight"),
				this.sf.getPath(sessionID+"_pval"),
				oStart, oEnd, dStart, dEnd,
				filterByPValue, PValue, onlyGenes, is_gene_entrez, negative_cor)){
				return null;
			}*/
		}
		GetExpression ge = new GetExpression(false, normalizeExpr, dsets, genes, null);
		ge.PerformQueryExpression(sf.pclserver_port);
		float[][][] gmat = ge.GetGeneExpression();
		System.out.println("Datasets " + dsets + " " + genes);
		System.out.println("Number of datasets " + gmat.length);

		int num_datasets = dsets.size();
		int num_genes = genes.size();
		float[][][] mat = new float[num_datasets][][];

    	for(int i=0; i<num_datasets; i++){	
    		mat[i] = new float[gmat[i][0].length][];
			System.out.println("Dataset " + i + " " + gmat[i][0].length);
    		for(int k=0; k<gmat[i][0].length; k++) //num arrays per dataset
    			mat[i][k] = new float[num_genes];
    		for(int j=0; j<gmat[i][0].length; j++){ //num arrays
				for(int k=0; k<gmat[i].length; k++){ //num genes
					mat[i][j][k] = gmat[i][k][j];
				}
			}
		}
		System.out.println("Finished " + num_datasets + " " + num_genes);
		return new ExpressionMatrix(mat, gmat);
	}		

	public boolean SampleOrder(String sessionID, 
	int oStart, int oEnd, int dStart, int dEnd,
	boolean isQuery,
	boolean normalizeExpr, 
	boolean filterByPValue, float PValue, 
	Vector<OrthologGroup> onlyGenes, boolean is_gene_entrez, 
	boolean negative_cor) throws IOException{ //is_gene_entrez applicable if onlyGenes specified

		this.isQuery = isQuery;
		this.normalizeExpr = normalizeExpr;
		ExpressionMatrix exp_matrix = Common(sessionID, isQuery,
			oStart, oEnd, dStart, dEnd, filterByPValue, PValue, 
			onlyGenes, is_gene_entrez, negative_cor);

		this.microarray_samples = new MicroarraySamples();
		this.microarray_samples.CalculateSampleOrder(exp_matrix.mat);
		this.microarray_samples.WriteSampleOrder(this.dset_order_file, this.dsets);
		this.size = this.microarray_samples.GetSize();
		//this.paint_dendrogram(dendrogramFile);
		return true;
	}

	//not used yet
    public boolean LoadQuery(String sessionID,
	int dStart, int dEnd,
	float lower_clip, float upper_clip,
	boolean normalizeExpr,
	boolean sortSamples,
	boolean filterByPValue, float PValue,
	Vector<OrthologGroup> onlyGenes, boolean is_gene_entrez, boolean negative_cor) throws IOException{
		
		this.sortSamples = sortSamples;
		this.lower_clip = lower_clip;
		this.upper_clip = upper_clip;  	
		this.normalizeExpr = normalizeExpr;
		this.isQuery = true;

		ExpressionMatrix exp_matrix = Common(sessionID, true,
			-1, -1, dStart, dEnd, filterByPValue, PValue, onlyGenes, is_gene_entrez, negative_cor);

		//this.microarray_genes = new MicroarrayGenes();
		//this.microarray_genes.CalculateGeneOrder(exp_matrix.gmat);
		//this.microarray_genes.WriteGeneOrder(this.gene_order_file);

    	this.init(exp_matrix, true);
		return true;
    }

    public boolean Load(String sessionID,
	int gStart, int gEnd, int dStart, int dEnd,
	float lower_clip, float upper_clip,
	boolean normalizeExpr,
	boolean sortSamples,
	boolean filterByPValue, float PValue, 
	Vector<OrthologGroup> onlyGenes, boolean is_gene_entrez, boolean negative_cor) throws IOException{
		
		this.sortSamples = sortSamples;
		this.lower_clip = lower_clip;
		this.upper_clip = upper_clip;  	
		this.normalizeExpr = normalizeExpr;
		this.isQuery = false;

		ExpressionMatrix exp_matrix = Common(sessionID, false,
			gStart, gEnd, dStart, dEnd, filterByPValue, PValue, onlyGenes, is_gene_entrez, negative_cor);

    	this.init(exp_matrix, false);
		return true;
    }
}
