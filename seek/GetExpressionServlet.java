package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import seek.ReadPacket;
import seek.Network;
import seek.GetExpression;
import seek.SeekFile;

public class GetExpressionServlet extends HttpServlet {

  public void doPost( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException {

	boolean enableQuery = false;
	if(req.getParameter("enable_query").equals("1")){
		enableQuery = true;
	}
	boolean normalize = false;
	if(req.getParameter("normalize").equals("1")){
		normalize = true;
	}

	String sessionID = req.getParameter("sessionID");
	File tempDir = (File) getServletContext().
		getAttribute( "javax.servlet.context.tempdir" );
	float rbp_p = Float.parseFloat(req.getParameter("rbp_p"));
	String organism = req.getParameter("organism");
	SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);

	Vector<String> dset = new Vector<String>();
	Vector<String> gene = new Vector<String>();
	Vector<String> query = new Vector<String>();

	String[] dsetS;
	String[] geneS;
	String[] queryS;

	dsetS = StringUtils.split(req.getParameter("dataset"), "+");
	geneS = StringUtils.split(req.getParameter("gene"), "+");
	if(enableQuery){
		queryS = StringUtils.split(req.getParameter("query"), "+");
		for(int i=0; i<queryS.length; i++){
			query.add(queryS[i]);
		}
		System.out.println("Query " + query);
	}

	for(int i=0; i<dsetS.length; i++){
		dset.add(dsetS[i]);
	}
	for(int i=0; i<geneS.length; i++){
		gene.add(geneS[i]);
	}

	System.out.println(dset + " " + gene);
	//Set<String> dsetSet = new HashSet<String>(dset);

	GetExpression ge = 
		new GetExpression(enableQuery, normalize, dset, gene, query, rbp_p);
	
	try{
		ge.PerformQuery(sf.pclserver_port);	
		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");

		if(!enableQuery){
			float[][][] gmat = ge.GetGeneExpression();
			int num_genes = gmat[0].length;
			int num_datasets = gmat.length;
	
			Vector<String> gmat_str = new Vector<String>();
			//by gene, then by dataset, then by arrays
			for(int k=0; k<num_genes; k++){
				for(int j=0; j<num_datasets; j++){
					for(int i=0; i<gmat[j][k].length; i++){ //dataset size
						gmat_str.add(String.format("%.2f", gmat[j][k][i]));
					}
				}
			}
			String send_str = StringUtils.join(gmat_str, ",");
			res.getWriter().write(send_str);
			return ;
		}

		//enableQuery == true;
		float[][][] qmat = ge.GetQueryExpression();
		float[][][] gmat = ge.GetGeneExpression();
		float[][] comat = ge.GetGeneCoexpression();
		int num_genes = gmat[0].length;
		int num_datasets = gmat.length;
		int num_query = qmat[0].length;
	
		Vector<String> gmat_str = new Vector<String>();
		Vector<String> qmat_str = new Vector<String>();
		Vector<String> coexpr_str = new Vector<String>();
		//by gene, then by dataset, then by arrays
		for(int k=0; k<num_genes; k++){
			for(int j=0; j<num_datasets; j++){
				for(int i=0; i<gmat[j][k].length; i++){ //dataset size
					gmat_str.add(String.format("%.2f", gmat[j][k][i]));
				}
			}
		}

		for(int k=0; k<num_query; k++){
			for(int j=0; j<num_datasets; j++){
				for(int i=0; i<qmat[j][k].length; i++){
					qmat_str.add(String.format("%.2f", qmat[j][k][i]));
				}
			}
		}

		for(int k=0; k<num_genes; k++){
			for(int j=0; j<num_datasets; j++){
				coexpr_str.add(String.format("%.2f", comat[j][k]));
			}
		}
		String send_str = StringUtils.join(qmat_str, ",") + 
			"\n" + StringUtils.join(gmat_str, ",") + 
			"\n" + StringUtils.join(coexpr_str, ",");
		res.getWriter().write(send_str);
		

	}catch(IOException e){
	}

  }

}
