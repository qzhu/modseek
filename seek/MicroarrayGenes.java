package seek;
import seek.Distance;
import seek.TreeNode;
import seek.Cluster;
import java.util.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.ReadDataset;
import seek.Network;
import seek.ReadPacket;
import seek.SeekImage;
import seek.GetExpression;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class MicroarrayGenes {

	int[] gene_order;

	public MicroarrayGenes(){

	}

	//Requires gene_order_file, and genes
	public void WriteGeneOrder(String gene_order_file) throws IOException{
		PrintWriter out1 = new PrintWriter(new FileWriter(gene_order_file));
		int i;
		for(i=0; i<this.gene_order.length; i++){
			if(i==this.gene_order.length-1){
				out1.print(this.gene_order[i]);
			}else{
				out1.print(this.gene_order[i] + " ");
			}
		}
		out1.println();
		out1.close();
		System.out.println("Finished writing gene order");
	}


	public void CalculateGeneOrder(float[][][] gene_matrix){
		this.gene_order = new int[gene_matrix[0].length];
		if(this.gene_order.length==1){
			this.gene_order[0] = 0;
		}else{
			HierarchicalClusterGeneOrder(gene_matrix); //outputs gene_order
		}
		System.out.println("Finished calculating gene order");
	}


	//assumes that gene_matrix has perfect dimension (same number of genes across datasets)
	 public void HierarchicalClusterGeneOrder(float[][][] gene_matrix){
        int i, j, k;

        int mat_genes = gene_matrix[0].length;
        System.out.println("Mat genes" + mat_genes);
        float[][] final_distance = new float[mat_genes][mat_genes];
        int[][] final_weight = new int[mat_genes][mat_genes];

        for(j=0; j<mat_genes; j++){ //genes
            for(k=0; k<mat_genes; k++){ //genes
                final_distance[j][k] = 0; //initialization
                final_weight[j][k] = 0;
            }
        }

        for(i=0; i<gene_matrix.length; i++){ //dataset
            int num_samples = gene_matrix[i][0].length;
            for(j=0; j<mat_genes; j++){
                if(gene_matrix[i][j][0]>327.0f){
                    continue;
                }
                for(k=j+1; k<mat_genes; k++){
                    if(gene_matrix[i][k][0]>327.0f){
                        continue;
                    }
					float d = Distance.EuclideanDistance(gene_matrix[i][j], gene_matrix[i][k]);
                    d = d * (float) num_samples;
                    final_distance[j][k] += d;
                    final_distance[k][j] += d;
                    final_weight[j][k] += num_samples;
                    final_weight[k][j] += num_samples;
                }
            }
        }

        for(j=0; j<mat_genes; j++){ //genes
            for(k=j+1; k<mat_genes; k++){ //genes
                final_distance[j][k] /= (float) final_weight[j][k];
                final_distance[k][j] /= (float) final_weight[k][j];
            }
        }

        Cluster c = new Cluster(final_distance);
        TreeNode[] t = c.HClusterAvg();
        Vector<Integer> vi = c.Linearize(t);
        Map<Integer,Float> mm = c.GetDepth(t);

        //float g_dendro_max_depth = 0;
		TreeNode[] g_dendro_tree = t;
		Vector<Float> dist = new Vector<Float>();
		for(TreeNode tr : t){
			dist.add(tr.distance);
		}
		//g_dendro_max_depth = Collections.max(dist);
		this.gene_order = new int[mat_genes];

		for(j=0; j<mat_genes; j++){
			this.gene_order[j] = vi.get(j);
		}
	}
	public static int[] ReadGeneOrder(String gene_order_file, int num_genes) throws IOException{
		int[] gene_order = new int[num_genes]; //number of query genes
		BufferedReader bb = new BufferedReader(new FileReader(gene_order_file));
		String s = bb.readLine();;
		StringTokenizer st = new StringTokenizer(s, " ");
		int ki = 0;
		while(st.hasMoreTokens()){
			int order = Integer.parseInt(st.nextToken());
			gene_order[ki] = order;
			ki++;
		}
		bb.close();
		return gene_order;
	}
}
