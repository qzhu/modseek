package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

public class GetDatasetCategory extends HttpServlet {

	public String ReadDatasetCategory(String file) throws IOException{
		String ret = "all";
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String ss = null;
			ss = in.readLine();
			if(ss.equals("all")){
				ret = ss;
			}else if(ss.startsWith("category")){
				ret = ss;
			}else{
				ret = "Custom";
			}
			in.close();
		}catch(IOException e){
			System.out.println("BAD i/o!\n");
		}
		return ret;
	}


	public void doGet( HttpServletRequest req, HttpServletResponse res )
	throws ServletException, IOException {

		String sessionID = req.getParameter("sessionID");
		String organism = req.getParameter("organism");

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession();

		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		SeekFile sf = new SeekFile(tempDir.getAbsolutePath(), organism);

		String file_path = sf.getPath(sessionID + "_dcategory");
		File f = new File(file_path);
		String ret = "Not available";
		File f2 = new File(sf.getPath(sessionID + "_guide_query"));	
		boolean is_guided = f2.exists() && !f2.isDirectory();

		if(f.exists() && !f.isDirectory()){
			ret = ReadDatasetCategory(file_path);
		}

		if(ret.equals("all") && is_guided){
			ret = "all_guided";
		}

		res.getWriter().write(ret + "\n");
	}
}
