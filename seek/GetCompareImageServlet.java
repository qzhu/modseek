package seek;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import seek.*;

public class GetCompareImageServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {

		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		String organism1 = req.getParameter("organism1");
		String organism2 = req.getParameter("organism2");
		String sessionID1 = req.getParameter("sessionID1");
		String sessionID2 = req.getParameter("sessionID2");
		int zoomLevel = Integer.parseInt(req.getParameter("zoom_level"));

		SeekFile sf1 = new SeekFile(tempDir.getAbsolutePath(), organism1);
		SeekFile sf2 = new SeekFile(tempDir.getAbsolutePath(), organism2);

		CompareList2 cp = new CompareList2();
		String option = "up_up";
		String show_list = "shared";
		boolean toShuffle = false;

		cp.Compare(organism1, organism2, sessionID1, sessionID2, tempDir.getAbsolutePath(), 
			option, show_list, toShuffle);
		
		MicroarrayComparativeImage mi1 = new MicroarrayComparativeImage(
			tempDir.getAbsolutePath(), organism1, "blue-black-yellow");
		
		String imageFile1 = mi1.sf.getPath(sessionID1+"_image.png");
		String headerFile1 = mi1.sf.getPath(sessionID1+"_header_image.png");
		String dendrogramFile1 = mi1.sf.getPath(sessionID1+"_dendrogram_image.png");
		String sampleLabelFile1 = mi1.sf.getPath(sessionID1+"_sampleLabel_image.png");
		String geneNameFile1 = mi1.sf.getPath(sessionID1+"_geneName_image.png");
		boolean displayDatasetKeywords = false;
		int dStart1, dEnd1, dStart2, dEnd2;
		int gStart1, gEnd1, gStart2, gEnd2;
		dStart1 = Integer.parseInt(req.getParameter("d1_start"));
		dEnd1 = Integer.parseInt(req.getParameter("d1_end"));
		boolean normalize=true;

		mi1.ReadMap(displayDatasetKeywords);
		mi1.SampleOrder(sessionID1,
			0, 50, dStart1, dEnd1,
			false, //is query or not
			normalize, //normalize expression or not
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);

		gStart1 = Integer.parseInt(req.getParameter("g1_start"));
		gEnd1 = Integer.parseInt(req.getParameter("g1_end"));
		boolean sortSample = true;
		float down = -2.0f; //clip
		float up = 2.0f; //clip
		mi1.Load(sessionID1,
			gStart1, gEnd1, dStart1, dEnd1,
			down, up,
			normalize, //normalize expression or not
			sortSample, //sort Samples 
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);
		
		mi1.paint(headerFile1, dendrogramFile1, geneNameFile1, imageFile1, sampleLabelFile1, zoomLevel);
		
		MicroarrayComparativeImage mi2 = new MicroarrayComparativeImage(
			tempDir.getAbsolutePath(), organism2, "blue-black-yellow");
		
		String imageFile2 = mi2.sf.getPath(sessionID2+"_image.png");
		String headerFile2 = mi2.sf.getPath(sessionID2+"_header_image.png");
		String dendrogramFile2 = mi2.sf.getPath(sessionID2+"_dendrogram_image.png");
		String sampleLabelFile2 = mi2.sf.getPath(sessionID2+"_sampleLabel_image.png");
		String geneNameFile2 = mi2.sf.getPath(sessionID2+"_geneName_image.png");
		displayDatasetKeywords = false;

		dStart2 = Integer.parseInt(req.getParameter("d2_start"));
		dEnd2 = Integer.parseInt(req.getParameter("d2_end"));
		normalize=true;

		mi2.ReadMap(displayDatasetKeywords);
		mi2.SampleOrder(sessionID2,
			0, 50, dStart2, dEnd2,
			false, //is query or not
			normalize, //normalize expression or not
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);

		gStart2 = Integer.parseInt(req.getParameter("g2_start"));
		gEnd2 = Integer.parseInt(req.getParameter("g2_end"));
		sortSample = true;
		down = -2.0f; //clip
		up = 2.0f; //clip
		mi2.Load(sessionID2,
			gStart2, gEnd2, dStart2, dEnd2,
			down, up,
			normalize, //normalize expression or not
			sortSample, //sort Samples 
			false, -1.0f, cp.GetConservedOrthologGroups(), true, 
			false);
		mi2.paint(headerFile2, dendrogramFile2, geneNameFile2, imageFile2, sampleLabelFile2, zoomLevel);

		MicroarrayComparativeImage mi1_q = new MicroarrayComparativeImage(
			tempDir.getAbsolutePath(), organism1, "blue-black-yellow");
		MicroarrayComparativeImage mi2_q = new MicroarrayComparativeImage(
			tempDir.getAbsolutePath(), organism2, "blue-black-yellow");
		mi1_q.ReadMap(displayDatasetKeywords);
		mi2_q.ReadMap(displayDatasetKeywords);
		mi1_q.LoadQuery(sessionID1, 
			dStart1, dEnd1, 
			down, up, 
			normalize, 
			sortSample, false, -1.0f, cp.GetQueryOrthologGroups(), true, false);

		imageFile1 = mi1.sf.getPath(sessionID1+"_query_image.png");
		headerFile1 = mi1.sf.getPath(sessionID1+"_query_header_image.png");
		dendrogramFile1 = mi1.sf.getPath(sessionID1+"_query_dendrogram_image.png");
		sampleLabelFile1 = mi1.sf.getPath(sessionID1+"_query_sampleLabel_image.png");
		geneNameFile1 = mi1.sf.getPath(sessionID1+"_query_geneName_image.png");
		mi1_q.paint(headerFile1, dendrogramFile1, geneNameFile1, imageFile1, 
			sampleLabelFile1, zoomLevel);

		mi2_q.LoadQuery(sessionID2, 
			dStart2, dEnd2, 
			down, up, 
			normalize, 
			sortSample, false, -1.0f, cp.GetQueryOrthologGroups(), true, false);

		imageFile2 = mi2.sf.getPath(sessionID2+"_query_image.png");
		headerFile2 = mi2.sf.getPath(sessionID2+"_query_header_image.png");
		dendrogramFile2 = mi2.sf.getPath(sessionID2+"_query_dendrogram_image.png");
		sampleLabelFile2 = mi2.sf.getPath(sessionID2+"_query_sampleLabel_image.png");
		geneNameFile2 = mi2.sf.getPath(sessionID2+"_query_geneName_image.png");
		mi2_q.paint(headerFile2, dendrogramFile2, geneNameFile2, imageFile2, 
			sampleLabelFile2, zoomLevel);

		res.setContentType("text/plain");
		res.setCharacterEncoding("UTF-8");

		String return_str = mi1.windowWidth + " " + mi2.windowWidth;
		res.getWriter().write(return_str);

	}
}
