package seek;
import java.nio.*;
import org.apache.commons.lang3.StringUtils;
import java.io.*;
import java.util.*;
import seek.Pair;
import seek.ReadScore;
import seek.RankList;
import seek.OrthologGroup;

public class OrthologReader{
	public static Map<String,Map<String,OrthologGroup> > 
	ReadMap(String file, String org1, String org2) throws IOException{
		Map<String,Map<String,OrthologGroup> > m = new HashMap<String,Map<String,OrthologGroup> >();
		try{
			BufferedReader in = new BufferedReader(new FileReader(file));
			String s = null;
			Map<String,OrthologGroup > fmap = new HashMap<String,OrthologGroup>();
			Map<String,OrthologGroup > rmap = new HashMap<String,OrthologGroup>();
			int ID = 0;
			while((s = in.readLine())!=null){
				StringTokenizer st = new StringTokenizer(s, "|");
				String g1 = st.nextToken();
				String g2 = st.nextToken();

				Vector<String> en1 = new Vector<String>();
				StringTokenizer st1 = new StringTokenizer(g1, " ");
				while(st1.hasMoreTokens())
					en1.add(st1.nextToken());
				Vector<String> en2 = new Vector<String>();
				StringTokenizer st2 = new StringTokenizer(g2, " ");
				while(st2.hasMoreTokens())
					en2.add(st2.nextToken());
				
				OrthologGroup ortho1 = new OrthologGroup(ID, org1, org2, en1, en2);
				OrthologGroup ortho2 = new OrthologGroup(ID, org2, org1, en2, en1);

				for(String os : en1){
					fmap.put(os, ortho1);
				}
				for(String os : en2){
					rmap.put(os, ortho2);
				}
				ID++;
			}
			m.put(org1 + "_" + org2, fmap);
			m.put(org2 + "_" + org1, rmap);
			in.close();
		}catch(IOException e){
			System.out.println("Error opening file " + file);
		}
		return m;
	}

	public static Map<String,Map<String, OrthologGroup> >
	Prepare(String tempDir, String org1, String org2) throws IOException{

		String t = tempDir;
		String file_worm_fly = t + "/" + "filtered.map.worm.fly.txt";
		String file_worm_yeast = t + "/" + "filtered.map.worm.yeast.txt";
		String file_worm_zebrafish = t + "/" + "filtered.map.worm.zebrafish.txt";
		String file_worm_human = t + "/" + "filtered.map.worm.human.txt";
		String file_worm_mouse = t + "/" + "filtered.map.worm.mouse.txt";
		String file_zebrafish_yeast = t + "/" + "filtered.map.zebrafish.yeast.txt";
		String file_zebrafish_human = t + "/" + "filtered.map.zebrafish.human.txt";
		String file_zebrafish_mouse = t + "/" + "filtered.map.zebrafish.mouse.txt";
		String file_fly_zebrafish = t + "/" + "filtered.map.fly.zebrafish.txt";
		String file_fly_yeast = t + "/" + "filtered.map.fly.yeast.txt";
		String file_fly_human = t + "/" + "filtered.map.fly.human.txt";
		String file_fly_mouse = t + "/" + "filtered.map.fly.mouse.txt";
		String file_human_yeast = t + "/" + "filtered.map.human.yeast.txt";
		String file_human_mouse = t + "/" + "filtered.map.human.mouse.txt";
		String file_mouse_yeast = t + "/" + "filtered.map.mouse.yeast.txt";
		//this.m = new HashMap<String, Map<String,OrthologGroup> >();
		Set<String> orgSet = new HashSet<String>();
		orgSet.add(org1);
		orgSet.add(org2);
		Map<String,Map<String,OrthologGroup> > m = null;
		if(orgSet.contains("worm") && orgSet.contains("fly"))
			m = ReadMap(file_worm_fly, "worm", "fly");
		else if(orgSet.contains("worm") && orgSet.contains("yeast"))
			m = ReadMap(file_worm_yeast, "worm", "yeast");
		else if(orgSet.contains("fly") && orgSet.contains("yeast"))
			m = ReadMap(file_fly_yeast, "fly", "yeast");
		else if(orgSet.contains("zebrafish") && orgSet.contains("worm"))
			m = ReadMap(file_worm_zebrafish, "worm", "zebrafish");
		else if(orgSet.contains("zebrafish") && orgSet.contains("fly"))
			m = ReadMap(file_fly_zebrafish, "fly", "zebrafish");
		else if(orgSet.contains("zebrafish") && orgSet.contains("yeast"))
			m = ReadMap(file_zebrafish_yeast, "zebrafish", "yeast");
		else if(orgSet.contains("human") && orgSet.contains("yeast"))
			m = ReadMap(file_human_yeast, "human", "yeast");
		else if(orgSet.contains("human") && orgSet.contains("worm"))
			m = ReadMap(file_worm_human, "worm", "human");
		else if(orgSet.contains("human") && orgSet.contains("fly"))
			m = ReadMap(file_fly_human, "fly", "human");
		else if(orgSet.contains("human") && orgSet.contains("zebrafish"))
			m = ReadMap(file_zebrafish_human, "zebrafish", "human");
		else if(orgSet.contains("mouse") && orgSet.contains("yeast"))
			m = ReadMap(file_mouse_yeast, "mouse", "yeast");
		else if(orgSet.contains("mouse") && orgSet.contains("worm"))
			m = ReadMap(file_worm_mouse, "worm", "mouse");
		else if(orgSet.contains("mouse") && orgSet.contains("fly"))
			m = ReadMap(file_fly_mouse, "fly", "mouse");
		else if(orgSet.contains("mouse") && orgSet.contains("zebrafish"))
			m = ReadMap(file_zebrafish_mouse, "zebrafish", "mouse");
		else if(orgSet.contains("mouse") && orgSet.contains("human"))
			m = ReadMap(file_human_mouse, "human", "mouse");
		return m;
	}

}
