package seek;
import java.util.*;
import java.awt.GraphicsDevice;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import seek.Pair;
import seek.ReadScore;
import seek.ReadDataset;
import seek.Network;
import seek.ReadPacket;
import seek.SeekImage;
import seek.GetExpression;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;

public class CoexpressionImage extends SeekImage{

	public class ImageSet{
		public BufferedImage image;
		public int[][] color_red;
		public int[][] color_green;
		public int[][] color_blue;
		public float[][] comat;
		public int num_dset;
		public int num_gene;
	}

	//black bg
	/*int[] query_red =   {10, 29, 67, 95, 123, 161, 199, 227, 255};
	int[] query_green = {7, 20, 53, 86, 110, 142, 165, 188, 211};
	int[] query_blue =  {0, 0, 0, 0, 0, 0, 0, 0, 0};
	*/
	//white bg
	/*int[] query_red = {255, 254, 253, 253, 252, 239, 215, 179, 127};
	int[] query_green = {247, 232, 212, 187, 141, 101, 48, 0, 0};
	int[] query_blue = {236, 200, 158, 132, 89, 72, 31, 0, 0};
	*/
	int[] query_red;
	int[] query_green;
	int[] query_blue;

	ImageSet image;
	ImageSet query_image;
	BufferedImage column_header_image;

	Vector<String> dataset_annot;
	String dsetTitleDir;
    String[] query_names;
	Vector<String> query;

	float dset_sum_score = 0f;
	int compendium_num_dset = 0;
	float dset_max_score = 0f;

	int width_per_dataset = 15;    	
	int column_header_height = 180;

	float upper_clip;
	float lower_clip;

	float query_upper_clip;
	float query_lower_clip;

	int dataset_start;
	//column header width is same as windowWidth

	public CoexpressionImage(String tempDir, String organism, String colorChoice)throws IOException{
		super(tempDir, organism, colorChoice);
		query_red = pos_red;
		query_green = pos_green;
		query_blue = pos_blue;
	}

	public Color[] GetQueryColorGradient(){
		Color[] c = new Color[query_red.length];
		for(int i=0; i<query_red.length; i++)
			c[i] = new Color(query_red[i], query_green[i], query_blue[i]);
		return c;
	}

	public float GetQueryUpperClip(){
		return query_upper_clip;
	}

	public float GetQueryLowerClip(){
		return query_lower_clip;
	}

	public float[][] GetMatrix(){
		return image.comat;
	}

	public float[][] GetQueryMatrix(){
		return query_image.comat;
	}

	public int[] GetDatasetSize(){
		size = new int[dsets.size()];
		for(int i=0; i<dsets.size(); i++){
			size[i] = width_per_dataset;	
		}
		return size;
	}

	public int GetHorizontalUnitWidth(){
		return width_per_dataset;
	}

	public int GetHorizontalDatasetSpacing(){
		return 0;
	}
    
    protected int calculateUnitWidth(int s){
    	if(s<=10){
    		return 2;
    	}else if(s<=50){
    		return 2;
    	}else if(s<=200){
    		return 2;
    	}else if(s<=300){
    		return 2;
    	}else{
    		return 2;
    	}
    }

    protected void calculateSize(){
    	//int x = 5 + geneNameWidth;
    	int x = 5;
    	int y = 0;

    	for(int i=0; i<numDatasetToShow; i++){
			x+=width_per_dataset;
    	}
    	x+=10;
    	y += verticalUnitHeight*numGeneToShow;
    	windowWidth = x;
    	windowHeight = y;
    }
    
    public void save(String image_name, 
		String query_image_name, 
		String column_header_image_name){

    	File f = new File(image_name);
    	File f1 = new File(query_image_name);
    	File f2 = new File(column_header_image_name);

    	try{
    		ImageIO.write(image.image, "png", f);
    		ImageIO.write(query_image.image, "png", f1);
			ImageIO.write(column_header_image, "png", f2);
    	}catch(IOException e){	
			System.out.println("Error in writing");
    	}
    }
   
    public ImageSet init(ImageSet ims, float lower_clip, float upper_clip, 
		int[] red, int[] green, int[] blue) throws IOException{

        int i, j, k;
       
		//matrix - datasets, then genes
		int num_datasets = ims.num_dset;
		int num_genes = ims.num_gene;

		ims.color_red = new int[num_genes][num_datasets];
		ims.color_green = new int[num_genes][num_datasets];
		ims.color_blue = new int[num_genes][num_datasets];

		int max_size = red.length;
		float unit_length = (upper_clip - lower_clip) / (float) max_size;

		//color_red - genes, then datasets
		for(i=0; i<num_datasets; i++){ //dataset
			for(j=0; j<num_genes; j++){ //genes
				ims.color_red[j][i] = neutral_red;
				ims.color_green[j][i] = neutral_green;
				ims.color_blue[j][i] = neutral_blue;

				if(ims.comat[i][j]>327.0f){
					ims.color_green[j][i] = neutral_green;
					ims.color_red[j][i] = neutral_red;
					ims.color_blue[j][i] = neutral_blue;
					continue;
				}

				if(ims.comat[i][j]>upper_clip){
					ims.color_red[j][i] = red[max_size-1];
					ims.color_green[j][i] = green[max_size-1];
					ims.color_blue[j][i] = blue[max_size-1];
					continue;
				}

				if(ims.comat[i][j]<lower_clip){
					ims.color_red[j][i] = red[0];
					ims.color_green[j][i] = green[0];
					ims.color_blue[j][i] = blue[0];
					continue;
				}

				int level = (int) Math.round((float)(1.0f * (ims.comat[i][j] - lower_clip) / unit_length));
				if(level<0){
					level=0;
				}else if(level>=max_size){
					level = max_size - 1;
				}
				ims.color_red[j][i] = red[level];
				ims.color_green[j][i] = green[level];
				ims.color_blue[j][i] = blue[level];
        	}
        }
       
		return ims;
	}

	
	public void init_data() throws IOException{
        names = new String[genes.size()];
        query_names = new String[query.size()];
		int i,j,k;

        for(i=0; i<names.length; i++){
        	names[i] = new String(map_genes.get(genes.get(i)));
        }
        for(i=0; i<query_names.length; i++){
        	query_names[i] = new String(map_genes.get(query.get(i)));
        }
        
        description = new String[numDatasetToShow];
		
		/*
        int STRING_MAX_LENGTH = 200;        
        for(k=0; k<numDatasetToShow; k++){
        	String stem = getStem(dsets.get(k));
        	String descr = map_datasets.get(stem);
        	if(descr.length()>STRING_MAX_LENGTH){
        		descr = descr.substring(0, 200) + "...";
        	}
        	description[k] = "#" + (k+1) + "  " + stem + ": " + descr;
        }
        */

		for(k=0; k<numDatasetToShow; k++){
			String stem = ReadScore.getDatasetID(dsets.get(k));
			description[k] = map_datasets.get(stem);
		}
		
        //new
        calculateSize();
        column_header_image = new BufferedImage(windowWidth, column_header_height, BufferedImage.TYPE_INT_RGB);
        image.image = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_RGB);

    	int queryHeight = verticalUnitHeight*query.size();
        query_image.image = new BufferedImage(windowWidth, queryHeight, BufferedImage.TYPE_INT_RGB);

    }

    FontMetrics pickFont(Graphics2D g2,
                         String longString,
                         int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();

        while ( !fontFits ) {
            if ( (fontMetrics.getHeight() <= maxCharHeight)
                 && (fontMetrics.stringWidth(longString) <= xSpace) ) {
                fontFits = true;
            }
            else {
                if ( size <= minFontSize ) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name, style, --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

	public ImageSet paint_image(ImageSet img){
		Graphics2D g2 = img.image.createGraphics();
		int num_genes = img.num_gene;
		int num_datasets = img.num_dset;
		int x=5;
		int y=0;
		int i, j, k;
       	y=0;
        g2.setPaint(bg);
        g2.fill(new Rectangle2D.Double(0, 0, windowWidth, verticalUnitHeight*num_genes));
        g2.setPaint(fg);

        int[] unitWidth = new int[num_datasets];
        for(i=0; i<num_datasets; i++){ //dataset
			unitWidth[i] = width_per_dataset;
        }

		//y = 0;  
		Color hatching_color = null;
		if(neutral_red>200 && neutral_green>200 && neutral_blue>200){
			hatching_color = new Color(36, 36, 36); //light bg
		}else{
			hatching_color = new Color(128, 128, 128); //dark bg
		}
      
        for(k=0; k<num_genes; k++){
        	x = 5;
        	for(i=0; i<num_datasets; i++){ //dataset
				int orig_x = x;
				if(img.comat[i][k]>327f){
        			g2.setPaint(new Color(neutral_red, neutral_green, neutral_blue));
        		}else{
        			g2.setPaint(new Color(
        				img.color_red[k][i], img.color_green[k][i], img.color_blue[k][i]));
        		}
        		g2.fill(new Rectangle2D.Double(x, y, unitWidth[i], 
        			verticalUnitHeight));
        		x+=unitWidth[i];

				if(img.comat[i][k]>327f){
					g2.setPaint(hatching_color);
					int xt = orig_x;
					int un = 5;
					while(true){
						if(xt+un>2*(orig_x+unitWidth[i])) break;
						g2.drawLine(xt, y+un, xt+un, y);
						un += 5;
					}
					
					/*for(int ki=0; ki<5; ki++){
						if(xt+un>orig_x+unitWidth[i]){
							g2.drawLine(xt+un, y+un, 
					}*/
				}

        	}
			g2.setPaint(bg);
			g2.fill(new Rectangle2D.Double(x, y, verticalUnitHeight, verticalUnitHeight));

        	y+=verticalUnitHeight;
        }

		return img;
	}

	public void paint(){ //paints header only
		Graphics2D g3 = column_header_image.createGraphics();

		Color link_color = new Color(0, 0, 102); //white background
		//Color link_color = new Color(255, 255, 200); //black background

        int x=5; //initial coordinate
        int y=width_per_dataset; //initial coordinate
        int i, j, k;

		//drawing header image=================================
        BufferedImage img2 = new BufferedImage(column_header_height, windowWidth, BufferedImage.TYPE_INT_RGB);
		Graphics2D g3a = img2.createGraphics();
        g3a.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
        	RenderingHints.VALUE_ANTIALIAS_ON);
        g3a.setPaint(bg);
        g3a.fill(new Rectangle2D.Double(0, 0, column_header_height, windowWidth));
        g3a.setPaint(fg);
		Font font = g3a.getFont();
		int fontSize = font.getSize();
		String fontName = "Arial";
		int fontStyle = font.getStyle();
		font = new Font(fontName, fontStyle, fontSize-2);
		g3a.setFont(font);

		y=17;
        g3a.setPaint(fg);
		for(k=0; k<dsets.size(); k++){
			g3a.setColor(link_color);
			g3a.drawString((dataset_start + k+1) + ". " + 
				description[k],
				0, y);
			y+=15;
		}
		//finished===============
		int w = img2.getWidth(), h = img2.getHeight();
		int neww = h, newh = w;
		g3.setPaint(bg);
		g3.fill(new Rectangle2D.Double(0, 0, windowWidth, column_header_height));
		g3.setPaint(fg);
		g3.translate((neww-w)/2, (newh-h)/2);
		g3.rotate(-1.0*Math.toRadians(90), w/2, h/2);
		g3.drawRenderedImage(img2, null);
		//======================================================
	}

	public String[] getQueryNames(){
		return query_names;
	}

    public Vector<String> getQuery(){
    	return query;
    }
 
	//generates a coexpressed image
	public boolean read(String query_path,
	String gene_path, String dataset_path, String pval_path,
	int gStart, int gEnd,
	int dStart, int dEnd,
	boolean filter_by_pval, float pvalCutoff, 
	Vector<String> onlyGenes, //include only these genes! 
	boolean negative_cor)
	throws IOException{
	
		Map<String, Integer> mm = null;
		dataset_start = dStart;
		String name_mapping = null;
		String name_mapping_2 = null;
		float[] sc = null;
		Vector<Pair> vp = null;

		dsets = new Vector<String>();
		genes = new Vector<String>();
		query = new Vector<String>();

		dataset_score = new Vector<Float>();
		gene_score = new Vector<Float>();
		pval_score = new Vector<Float>();
		Scanner ss = null;

		image = new ImageSet();
		query_image = new ImageSet();
		float[] sc_pval = null;

		int numD = dEnd - dStart + 1;
		int numG = gEnd - gStart + 1;

		float NA_VALUE = -320.0f;
		if(negative_cor)
			NA_VALUE = 320.0f;

		try{
			//read the query file======================
    		ss = new Scanner(new BufferedReader(new FileReader(query_path)));
    		String s1 = ss.nextLine();
    		StringTokenizer st = new StringTokenizer(s1);
			while(st.hasMoreTokens()){
				query.add(st.nextToken());
			}
			ss.close();
			//=========================================

			//map_gene_symbol_entrez = ReadScore.readGeneSymbol2EntrezMapping(
			//	this.sf.gene_entrez_map_file);
    	
			name_mapping = this.sf.dataset_platform_file;
    		mm = ReadScore.readDatasetMapping(name_mapping);
			sc = ReadScore.ReadScoreBinary(dataset_path);
			vp = ReadScore.SortScores(mm, sc, false);

			dataset_annot = new Vector<String>();
			compendium_num_dset = 0;
			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(i).val;
				dset_sum_score += fval;
				if(fval>dset_max_score){
					dset_max_score = fval;
				}
				if(fval==0.0f){
					break;
				}
				compendium_num_dset++;
			}

			for(int i=0; i<numD; i++){
				float fval = (float) vp.get(dStart + i).val;
				if(fval==0.0f){
					continue;
				}
				//dsets.add(vp.get(dStart + i).term + ".pcl.bin");
				dsets.add(vp.get(dStart + i).term);
				dataset_score.add(new Float(vp.get(dStart + i).val));
				dataset_annot.add(vp.get(dStart+i).term);
				System.out.println("Dataset " + vp.get(dStart + i).term + " " + fval);
			}
			
    		name_mapping = this.sf.gene_map_file;
    		//name_mapping_2 = this.sf.gene_entrez_map_file;
    		//mm = ReadScore.readGeneMapping(name_mapping, name_mapping_2);
    		mm = ReadScore.readGeneMapping(name_mapping);
			sc = ReadScore.ReadScoreBinaryWithQueryNull(gene_path, query_path, mm, NA_VALUE);

			//filter by pval
			if(filter_by_pval){
				File test_file = new File(pval_path);
				if(test_file.exists()){
					sc_pval = ReadScore.ReadScoreBinaryWithQueryNull(pval_path, query_path, mm, 0.99f);
				}
				if(sc_pval!=null){	
					for(int i=0; i<sc.length; i++){
						if(sc_pval[i]>pvalCutoff){
							sc[i] = -320f;
						}
					}
				}
			}

			//filter by user-specified genes
			if(onlyGenes!=null && onlyGenes.size()>0){
				Set<Integer> include_id = new HashSet<Integer>();
				for(int i=0; i<onlyGenes.size(); i++){
					int internal_id = mm.get(onlyGenes.get(i));
					include_id.add(internal_id);
				}
				for(int i=0; i<sc.length; i++){
					if(!include_id.contains(i)){
						sc[i] = -320.0f;
					}
				}
			}

			vp = ReadScore.SortScores(mm, sc, negative_cor);

			for(int i=0; i<numG; i++){
				if(gStart+i>=vp.size()) break;
				//genes.add(map_gene_symbol_entrez.get(vp.get(gStart + i).term));
				genes.add(vp.get(gStart + i).term);
				gene_score.add(new Float(vp.get(gStart + i).val));
				if(filter_by_pval && sc_pval!=null){
					pval_score.add(new Float(sc_pval[vp.get(gStart+i).index]));
				}else{
					pval_score.add(0f);
				}
				//System.out.println("Gene " + map_gene_symbol_entrez.get(vp.get(gStart + i).term));
				System.out.println("Gene " + vp.get(gStart + i).term);
			}

		}catch(IOException e){
			System.out.println("Error has occurred, err 202");
		}

    	this.numDatasetToShow = dsets.size();
    	this.numGeneToShow = genes.size();
		return true;    
    }
    
    public boolean Load(String sessionID, //input
	String imageFile, //output
	String queryImageFile, //output
	String column_header_file, //output
	int gStart, int gEnd, int dStart, int dEnd, //Coexpression View page
	float lower_clip, float upper_clip, //internal use
	boolean filterByPValue, float PValue, 
	float rbp_p, boolean multiplyDatasetWeight, 
	Vector<String> onlyGenes,  //only include these genes
	boolean negative_cor) throws IOException{

		this.lower_clip = lower_clip;
		this.upper_clip = upper_clip;
		dsetTitleDir = this.sf.dataset_description_dir;

 	    if(!this.read(
			this.sf.getPath(sessionID+"_query"),
			this.sf.getPath(sessionID+"_gscore"),
			this.sf.getPath(sessionID+"_dweight"),
			this.sf.getPath(sessionID+"_pval"),
			gStart, gEnd, dStart, dEnd,
			filterByPValue, PValue, onlyGenes, negative_cor)){
			return false;
		}

		GetExpression ge = new GetExpression(true, true, dsets, genes, query);
		ge.PerformQueryCoexpression(
			this.sf.getPath(sessionID+"_dweight_comp"),
			this.sf.getPath(sessionID+"_query_part"),
			this.sf.dataset_platform_file, this.sf.pclserver_port);
		float[][] comat = ge.GetGeneCoexpression();
		float[][] qcomat = ge.GetQueryCoexpression();

		int num_datasets = dsets.size();
		int num_genes = genes.size();
		int num_query = query.size();
		image.comat = new float[num_datasets][num_genes];
		query_image.comat = new float[num_datasets][num_query];

		image.num_gene = num_genes;
		image.num_dset = num_datasets;
		query_image.num_gene = num_query;
		query_image.num_dset = num_datasets;       

		lower_clip = -3.2f;
		upper_clip = 3.2f;

		for(int i=0; i<num_datasets; i++){
			for(int g=0; g<num_genes; g++){
				float v = comat[i][g];
				if(v<-10.0f || v>10.0f){
					v = 327.67f;
				}else{
					if(multiplyDatasetWeight){
						v = v * dataset_score.get(i) / dset_sum_score * compendium_num_dset;
					}
				}
				image.comat[i][g] = v;
			}
		}
			
		query_lower_clip = 0f;
		query_upper_clip = dset_max_score * 1000f;

		for(int i=0; i<num_datasets; i++){
			float sum = 0;
			for(int g=0; g<num_query; g++){
				sum+=qcomat[i][g];
			}
			//float factor = (dataset_score.get(i) * 1000f) / sum;
			for(int g=0; g<num_query; g++){
				float v = qcomat[i][g];
				if(v==-9999){
					v = 327.67f;
				}else{
					v = v * num_query;
				}
				query_image.comat[i][g] = v;
			}
		}

		this.init_data();
		image = this.init(image, lower_clip, upper_clip, red, green, blue);

		query_image = this.init(query_image, query_lower_clip, query_upper_clip, 
		query_red, query_green, query_blue);

		this.paint();
		image = this.paint_image(image);
		query_image = this.paint_image(query_image);

		this.save(imageFile, queryImageFile, column_header_file);
		return true;
    }
}
