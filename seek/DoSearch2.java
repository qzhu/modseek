package seek;
import java.nio.*;
import java.net.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.io.FilenameUtils;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import seek.Network;
import seek.ReadPacket;
import seek.SeekFile;

public class DoSearch2 extends HttpServlet {

	public Vector<Float> getFloatFromBinary(byte[] b){
		long size = (long) (b.length / 4);
		int i;
		ByteBuffer buf = null;
		buf = ByteBuffer.wrap(b);
		buf.order(ByteOrder.LITTLE_ENDIAN);
		Vector<Float> sc = new Vector<Float>();
		for(i=0; i<size; i++){
			sc.add(buf.getFloat());
		}
		return sc;
	}

 	Vector<String> string_split(String s, String delimiter){
		String[] ss = StringUtils.split(s, delimiter);
 		Vector<String> rs = new Vector<String>();
		for(int i=0; i<ss.length; i++){
			rs.add(ss[i]);
		}
		return rs;
	}

	public Vector<Float> setQueryNull(Vector<Float> vf, Vector<String> query,
	Map<String,Integer> mm, float na){
		for(String q:query){
			vf.set(mm.get(q), na);
		}
		return vf;
	}

	public boolean guided_search(
		Vector<Vector<String> > vecAllSearchDataset, 
		Vector<Vector<String> > vecAllQuery,
		SeekFile sf, HttpSession session,
		String search_alg, String search_distance, float rbp_p,  
		float percent_query, float percent_genome,
		String correlation_sign,
		Vector<String> status) throws IOException{

		Socket kkSocket = null;
		int i = 0;

		//convert vecAllSearchDataset to a single string
		Vector<String> tmpSearchDataset = new Vector<String>();
		for(i=0; i<vecAllSearchDataset.size(); i++)
			tmpSearchDataset.add(StringUtils.join(vecAllSearchDataset.get(i), " "));
		String strAllSearchDataset = StringUtils.join(tmpSearchDataset, "|");	

		//convert vecAllQuery to a single string
		Vector<String> tmpQuery = new Vector<String>();
		for(i=0; i<vecAllQuery.size(); i++)
			tmpQuery.add(StringUtils.join(vecAllQuery.get(i), " "));
		String strAllQuery = StringUtils.join(tmpQuery, "|");

		String searchParameter = search_alg + "_" + rbp_p + "_" + 
			percent_query + "_" + percent_genome + "_" + 
			search_distance + "_" + correlation_sign + "_false"; //correlation_sign is not supported if it is FN-guided, last "false" refers to check_dset_size 

		System.out.println(strAllSearchDataset);
		System.out.println(strAllQuery);

		byte[] dByte = Network.getStringByte(strAllSearchDataset);
		byte[] qByte = Network.getStringByte(strAllQuery);
		byte[] outputDirByte = Network.getStringByte("/tmp"); //TO DO: do not need since output will be sent to client over network
		byte[] sByte = Network.getStringByte(searchParameter);
		byte[] gByte = Network.getStringByte("null"); //not used

		boolean negative_cor = (correlation_sign.equals("negative")) ? true : false;
		float NA_VALUE = -320.0f;
		if(negative_cor){
			NA_VALUE = 320.0f;
		}

		Vector<String> available_genes = new Vector<String>();		

		try{
			kkSocket = new Socket("localhost", sf.seekserver_guide_port); //here is what's different
			kkSocket.getOutputStream().write(dByte, 0, dByte.length);
			kkSocket.getOutputStream().write(qByte, 0, qByte.length);
			kkSocket.getOutputStream().write(outputDirByte, 0, 
				outputDirByte.length);
			kkSocket.getOutputStream().write(sByte, 0, sByte.length);
			kkSocket.getOutputStream().write(gByte, 0, gByte.length);

			//first message is about dataset availability
			ReadPacket st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			if(st.strVal.startsWith("Error:")){ 
			//error, no dataset contains any of the query genes
				status.clear();
				status.add(st.strVal);
				return false;
			}
			String dset_new = st.strVal;
			String dset_availability = 
				processDatasetAvailabilityMessage(st.strVal);

			//second message is about query availability
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			//System.out.println(st.strVal);
			Vector<String> gene_availability = 
				processGeneAvailabilityMessage(sf.ent_hgnc, st.strVal, 
				available_genes);

			//add messages to status
			status.addAll(gene_availability);
			status.add(dset_availability);

			if(session!=null){
				session.setAttribute(sf.sessionID + "_status", status);
				session.setAttribute(sf.sessionID + "_query_attr", gene_availability);
				session.setAttribute(sf.sessionID + "_dset_attr", dset_availability);
			}

			while(true){
				ReadPacket rp = Network.readSize(kkSocket);
				rp = Network.readContent(kkSocket, rp);
				if(rp.isString){
					System.out.println(rp.strVal);
					status.add(rp.strVal);
					if(session!=null){
						session.setAttribute(sf.sessionID + "_status", status);
					}
				}
				if(rp.strVal.startsWith("Done Search")){
					break;
				}
			}

			Vector<Float> vecDweight;
			Vector<Float> vecDweightComp;
			Vector<String> vecQueryPart;
	
			ReadPacket rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_guide_dweight"), rp.originalByte); //note "_guide"
			//System.out.println(rp.vecFloat);
			vecDweight = rp.vecFloat;

			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_guide_gscore"), rp.originalByte); //note "_guide"
			byte[] gscoreByte = rp.originalByte;
			//System.out.println(rp.vecFloat);

			//If non-RBP algorithm, weight component would always be zero!
			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_guide_dweight_comp"), rp.originalByte); //note "_guide"
			vecDweightComp = rp.vecFloat;

			//If non-RBP algorithm, query_part is the full query
			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_guide_query_part"), rp.strVal, available_genes); //note "_guide"
			vecQueryPart = available_genes;

			boolean ret = writeGuidedQueryAndSearchDataset(StringUtils.join(available_genes, " "), 
				dset_new, sf, sf.sessionID, negative_cor);

			if(ret==false){
				status.add("Error: not enough query genes present in >70% of the datasets. Suggest restrict datasets to those containing all query genes, or change query.");
				session.setAttribute(sf.sessionID + "_status", status);
				return false;
			}

		}catch(IOException e){
			System.out.println("Bad IO search");
		}
		return true;
	}


	//returns status message
	public boolean search(Vector<Vector<String> > vecAllSearchDataset,
		String strDatasetCategory,
		Vector<Vector<String> > vecAllQuery, 
		SeekFile sf, HttpSession session, 
		String search_alg, String search_distance, float rbp_p, 
		float percent_query, float percent_genome,
		String correlation_sign,
		String check_dset_size, //true or false
		Vector<Vector<String> > vecGuideGeneSet, //can be null if guide gene set is disabled
		//output: status
		Vector<String> status) throws IOException{

		Socket kkSocket = null;
		int i = 0;

		//convert vecAllSearchDataset to a single string
		Vector<String> tmpSearchDataset = new Vector<String>();
		for(i=0; i<vecAllSearchDataset.size(); i++)
			tmpSearchDataset.add(StringUtils.join(vecAllSearchDataset.get(i), " "));
		String strAllSearchDataset = StringUtils.join(tmpSearchDataset, "|");	

		//convert vecAllQuery to a single string
		Vector<String> tmpQuery = new Vector<String>();
		for(i=0; i<vecAllQuery.size(); i++)
			tmpQuery.add(StringUtils.join(vecAllQuery.get(i), " "));
		String strAllQuery = StringUtils.join(tmpQuery, "|");

		String strGuideGeneSet = "null";
		if(vecGuideGeneSet!=null){
			Vector<String> tmpGuideGeneSet = new Vector<String>();
			for(i=0; i<vecGuideGeneSet.size(); i++)
				tmpGuideGeneSet.add(StringUtils.join(vecGuideGeneSet.get(i), " "));
			strGuideGeneSet = StringUtils.join(tmpGuideGeneSet, "|");
		}

		//EqualWeighting, CVCUSTOM, CV, ORDERSTATS
		String searchParameter = search_alg + "_" + rbp_p + "_" + 
			percent_query + "_" + percent_genome + "_" + 
			search_distance + "_" + correlation_sign + "_" + check_dset_size;

		byte[] dByte = Network.getStringByte(strAllSearchDataset);
		byte[] qByte = Network.getStringByte(strAllQuery);
		byte[] outputDirByte = Network.getStringByte("/tmp"); //TO DO: do not need since output will be sent to client over network
		byte[] sByte = Network.getStringByte(searchParameter);
		byte[] gByte = Network.getStringByte(strGuideGeneSet); //if guide gene set is not null

		boolean negative_cor = (correlation_sign.equals("negative")) ? true : false;

		Vector<String> available_genes = new Vector<String>();		

		Socket kkSocketPValue = null;
		Socket kkSocketDatasetPValue = null;

		try{
			kkSocket = new Socket("localhost", sf.seekserver_port);

			kkSocket.getOutputStream().write(dByte, 0, dByte.length);
			kkSocket.getOutputStream().write(qByte, 0, qByte.length);
			kkSocket.getOutputStream().write(outputDirByte, 0, 
				outputDirByte.length);
			kkSocket.getOutputStream().write(sByte, 0, sByte.length);
			kkSocket.getOutputStream().write(gByte, 0, gByte.length); //for guide-gene-set

			//first message is about dataset availability
			ReadPacket st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			//System.out.println(st.strVal);
			if(st.strVal.startsWith("Error:")){ 
			//error, no dataset contains any of the query genes
				status.clear();
				status.add(st.strVal);
				return false;
			}
			String dset_new = st.strVal;
			String dset_availability = 
				processDatasetAvailabilityMessage(st.strVal);

			//second message is about query availability
			st = Network.readSize(kkSocket);
			st = Network.readContent(kkSocket, st);
			//System.out.println(st.strVal);
			Vector<String> gene_availability = 
				processGeneAvailabilityMessage(sf.ent_hgnc, st.strVal, 
				available_genes);

			//add messages to status
			status.addAll(gene_availability);
			status.add(dset_availability);

			if(session!=null){
				session.setAttribute(sf.sessionID + "_status", status);
				session.setAttribute(sf.sessionID + "_query_attr", gene_availability);
				session.setAttribute(sf.sessionID + "_dset_attr", dset_availability);
			}

			while(true){
				ReadPacket rp = Network.readSize(kkSocket);
				rp = Network.readContent(kkSocket, rp);
				if(rp.isString){
					System.out.println(rp.strVal);
					status.add(rp.strVal);
					if(session!=null){
						session.setAttribute(sf.sessionID + "_status", status);
					}
				}
				if(rp.strVal.startsWith("Done Search")){
					break;
				}
			}

			Vector<Float> vecDweight;
			Vector<Float> vecDweightComp;
			Vector<String> vecQueryPart;
	
			ReadPacket rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_dweight"), rp.originalByte);
			//System.out.println(rp.vecFloat);
			vecDweight = rp.vecFloat;

			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_gscore"), rp.originalByte);
			byte[] gscoreByte = rp.originalByte;
			//System.out.println(rp.vecFloat);

			//if(search_alg.equals("RBP")){
			//If non-RBP algorithm, weight component would always be zero!
			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_dweight_comp"), rp.originalByte);
			vecDweightComp = rp.vecFloat;

			//If non-RBP algorithm, query_part is the full query
			rp = Network.readSize(kkSocket);
			rp = Network.readContent(kkSocket, rp);
			write(sf.getPath(sf.sessionID + "_query_part"), rp.strVal, available_genes);
			vecQueryPart = available_genes;
			//}

			//Test for Gene based PValue===================
			try{
				kkSocketPValue = new Socket("localhost", sf.seekpvalue_port);
				byte[] q1 = Network.getStringByte(StringUtils.join(vecAllQuery.get(0), " "));
				byte[] v1 = Network.getStringByte(getFloatFromBinary(gscoreByte));
				byte[] m1 = Network.getStringByte("rank");
				kkSocketPValue.getOutputStream().write(m1, 0, m1.length);
				kkSocketPValue.getOutputStream().write(q1, 0, q1.length);
				kkSocketPValue.getOutputStream().write(v1, 0, v1.length);
				rp = Network.readSize(kkSocketPValue);
				rp = Network.readContent(kkSocketPValue, rp);
				write(sf.getPath(sf.sessionID + "_pval"), rp.originalByte);
			}catch(IOException e){
				//error opening port
			}
			//==============================================
			if(available_genes.size()>=2){
				try{
					kkSocketDatasetPValue = new Socket("localhost", sf.seekdatasetpvalue_port);
					Map<Integer,String> mx = sf.int_dset; //id to dataset string mapping
					Vector<String> dsetList = new Vector<String>();
					Vector<Float> dset_num_query = new Vector<Float>();
					Vector<Float> dset_score = new Vector<Float>();
					int num_query = available_genes.size();
					for(i=0; i<mx.keySet().size(); i++){
						dsetList.add(mx.get(i));
						int d_id = i;
						int nq = 0;
						for(int g=0; g<num_query; g++){
							float v = vecDweightComp.get(d_id*num_query+g);
							if(v<0 || Float.isNaN(v) || Float.isInfinite(v)) continue;
							nq++;
						}
						dset_num_query.add((float)nq);
						dset_score.add(vecDweight.get(d_id));
					}
					byte[] q1 = Network.getStringByte(StringUtils.join(dsetList, " "));
					byte[] v1 = Network.getStringByte(dset_score);
					byte[] m1 = Network.getStringByte(dset_num_query);
					kkSocketDatasetPValue.getOutputStream().write(q1, 0, q1.length);
					kkSocketDatasetPValue.getOutputStream().write(v1, 0, v1.length);
					kkSocketDatasetPValue.getOutputStream().write(m1, 0, m1.length);
					rp = Network.readSize(kkSocketDatasetPValue);
					rp = Network.readContent(kkSocketDatasetPValue, rp);
					write(sf.getPath(sf.sessionID + "_dset_pval"), rp.originalByte);
				}catch(IOException e){
					//error opening port
				}
			}
			//======================================================

			boolean ret = writeQueryAndSearchDataset(
				StringUtils.join(available_genes, " "), dset_new, strDatasetCategory,
				sf, sf.sessionID, sf.ent_int, sf.dset_int, negative_cor);

			if(ret==false){
				status.add("Error: not enough query genes present in >70% of the datasets. Suggest restrict datasets to those containing all query genes, or change query.");
				session.setAttribute(sf.sessionID + "_status", status);
				return false;
			}

		}catch(IOException e){
			System.out.println("Bad IO search");
		}
		return true;
	}

	public void write(String file, byte[] b) throws IOException{
		try{
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(
				new FileOutputStream(file)));
			long size = (long) (b.length / 4);
			ByteBuffer buf = ByteBuffer.allocate(8);
			buf.order(ByteOrder.LITTLE_ENDIAN);
			byte[] bSize = buf.putLong(size).array();
			out.write(bSize, 0, 8);
			out.write(b, 0, b.length);
			out.close();
		}catch(IOException e){
			System.out.println("Bad IO w");
		}
	}

	public void write(String file, String v, Vector<String> q) throws IOException{
		StringTokenizer tok = new StringTokenizer(v, "|");
			
		try{
			PrintWriter out1 = new PrintWriter(
				new FileWriter(file));
			if(tok.countTokens()==0){
				for(String s : q)
					out1.println(s);
			}else{
				while(tok.hasMoreElements()){
					out1.println(tok.nextElement());
				}
			}
			out1.close();
		}catch(IOException e){
			System.out.println("Bad IO");
		}
	}

	public Vector<String> processGeneAvailabilityMessage(
		Map<String, String> m, String s, Vector<String> outGenes){

		Vector<String> vs = new Vector<String>();
		String[] geneAvail = StringUtils.split(s, ";");

		for(String t : geneAvail){
			String[] ga = StringUtils.split(t, ":");
			String ns = "";
			
			if(Integer.parseInt(ga[1])==0){
				ns = ga[0] + " is not found in any dataset";
			}else{
				ns = "Found " + ga[1] + " datasets containing " + m.get(ga[0]);
				outGenes.add(ga[0]);
			}
			vs.add(ns);
		}

		return vs;
	}

	public String processDatasetAvailabilityMessage(String s){
		String[] dsetAvail = StringUtils.split(s, " ");
		return "Searching " + dsetAvail.length + " datasets";
	}

	public boolean writeGuidedQueryAndSearchDataset(String q, String d,
		SeekFile sf, String sessionID, boolean negative_cor) throws IOException{
		try{
			PrintWriter out1 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_guide_query")));
			PrintWriter out2 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_guide_dset")));
			out1.println(q);
			out2.println(d);
			out1.close();
			out2.close();
			if(negative_cor){
				PrintWriter out4 = new PrintWriter(
					new FileWriter(sf.getPath(sessionID + "_guide_neg_cor")));
				out4.println("1");
				out4.close();
			}
		}catch(IOException e){
			System.out.println("Bad IO");
		}
		return true;
	}

	public boolean writeQueryAndSearchDataset(String q, String d,
		String dCategory, 
		SeekFile sf, String sessionID, 
		Map<String,Integer> ent_int, Map<String,Integer> dset_int, 
		boolean negative_cor) 
		throws IOException{
		try{
			PrintWriter out1 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_query")));
			PrintWriter out2 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_dset")));
			PrintWriter out3 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_dcategory")));
			out1.println(q);
			out2.println(d);
			out3.println(dCategory);
			out1.close();
			out2.close();
			out3.close();
			if(negative_cor){
				PrintWriter out4 = new PrintWriter(
					new FileWriter(sf.getPath(sessionID + "_neg_cor")));
				out4.println("1");
				out4.close();
			}
		}catch(IOException e){
			System.out.println("Bad IO");
		}

		float NA_VALUE = -320.0f;
		if(negative_cor){
			NA_VALUE = 320.0f;
		}

		float[] g_sc = ReadScore.ReadScoreBinaryWithQueryNull(sf.getPath(sessionID+"_gscore"), 
			sf.getPath(sessionID + "_query"), ent_int, NA_VALUE);
		float[] d_sc = ReadScore.ReadScoreBinary(sf.getPath(sessionID+"_dweight"));
		
		Vector<Pair> vg_sc = ReadScore.SortScores(ent_int, g_sc, negative_cor);
		Vector<Pair> vd_sc = ReadScore.SortScores(dset_int, d_sc, false);
		Vector<String> vec_gene = new Vector<String>();
		Vector<String> vec_dset = new Vector<String>();

		int topDataset = Math.min(300, vd_sc.size());
		int topGene = Math.min(3000, vg_sc.size());
		//int topDataset = d_sc.length;
		//int topGene = g_sc.length;		

		/*if(vd_sc.size()<topDataset || vg_sc.size()<topGene){
			return false;
		}*/

		for(int i=0; i<topDataset; i++){
			if(vd_sc.get(i).val==0) break;
			vec_dset.add(vd_sc.get(i).term);
		}
		for(int i=0; i<topGene; i++){
			if(!negative_cor && vg_sc.get(i).val==-320) break;
			if(negative_cor && vg_sc.get(i).val==320) break;
			vec_gene.add(vg_sc.get(i).term);
		}

		//TO DO: really need these output?
		try{
			PrintWriter out1 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_result.txt")));
			out1.println(StringUtils.join(vec_dset, " "));
			out1.println(StringUtils.join(vec_gene, " "));
			out1.close();

			PrintWriter out2 = new PrintWriter(
				new FileWriter(sf.getPath(sessionID + "_result.query")));
			out2.println(StringUtils.join(vec_dset, " "));
			out2.println(q);
			out2.close();
		}catch(IOException e){
			System.out.println("Bad IO");
		}
		return true;

	}

	public void doPost( HttpServletRequest req, HttpServletResponse res )
      throws ServletException, IOException{
		//parameters:
		//dset - "all" or array of datasets (separated by ;),
		//        or "category" followed by the category names 
		//        (separated by ;)
		//query - query genes (separated by space)
		//query_mode - entrez or gene symbol
		//sessionID
		//search_alg (string, RBP, OrderStatistics, or EqualWeighting)
		//search_distance (Correlation, Zscore, or ZscoreHubbinessCorrected)
		//rbp_p (float 0.95)
		//percent_query (float 0.0)
		//percent_genome (float 0.0)
		//correlation_sign (positive or negative)
		//use_guide_dset ("FN", none)

		String tmpStr;
		File tempDir = (File) getServletContext().
			getAttribute("javax.servlet.context.tempdir");
		HttpSession session = req.getSession();
		String tempDirPath = tempDir.getAbsolutePath();
    	String sessionID = java.net.URLDecoder.decode(
			req.getParameter("sessionID"), "UTF-8");
		String organism = req.getParameter("organism");
		String dset = java.net.URLDecoder.decode(req.getParameter("dset"), 
			"UTF-8");
		String query = java.net.URLDecoder.decode(req.getParameter("query"), 
			"UTF-8");
		String query_mode = java.net.URLDecoder.decode(
			req.getParameter("query_mode"), "UTF-8");
		String search_alg = java.net.URLDecoder.decode(
			req.getParameter("search_alg"), "UTF-8");
		String search_distance = java.net.URLDecoder.decode(
			req.getParameter("search_distance"), "UTF-8");
		String correlation_sign = null;
		if(req.getParameter("correlation_sign")!=null){
			correlation_sign = java.net.URLDecoder.decode(
				req.getParameter("correlation_sign"), "UTF-8");
		}else{
			correlation_sign = "positive";
		}
		String check_dset_size = null;
		if(req.getParameter("check_dset_size")!=null){
			check_dset_size = java.net.URLDecoder.decode(
				req.getParameter("check_dset_size"), "UTF-8");
		}else{
			check_dset_size = "false";
		}
		float rbp_p = Float.parseFloat(java.net.URLDecoder.decode(
			req.getParameter("rbp_p"), "UTF-8"));
		float percent_query = Float.parseFloat(java.net.URLDecoder.decode(
			req.getParameter("percent_query"), "UTF-8"));
		float percent_genome = Float.parseFloat(java.net.URLDecoder.decode(
			req.getParameter("percent_genome"), "UTF-8"));
		String guide_dset = null;
		if(req.getParameter("guide_dset")!=null){ //if guide_dset is set, CV is overwritten with CVCUSTOM
			guide_dset = java.net.URLDecoder.decode(
				req.getParameter("guide_dset"), "UTF-8");
		}else{
			guide_dset = "none";
		}

		SeekFile sf = new SeekFile(tempDirPath, organism);
		sf.sessionID = sessionID;
		sf.ReadGeneMap();
		sf.ReadDatasetMap();
		if(!guide_dset.equals("none")){
			sf.ReadGuideDatasetMap();
		}

		Set<String> absent_query = new HashSet<String>();
		//Vector<String> absent_dset = new Vector<String>();

		Set<String> v_dset = new HashSet<String>();
		Set<String> v_query = new HashSet<String>();
		Vector<String> status = new Vector<String>();
	
		//convert gene symbol to entrez
		if(query_mode.equals("gene_symbol")){
			String[] ss = StringUtils.split(query, " ");
			for(String s : ss){
				//do a filtering
				if(sf.hgnc_ent.containsKey(s))
					v_query.add(sf.hgnc_ent.get(s));
				else
					absent_query.add(s);
			}
		}else{ //if query is already in entrez
			String[] ss = StringUtils.split(query, " ");
			for(String s : ss)
				v_query.add(s);
		}

		if(dset.equals("all")){
			for(String d : sf.dset_int.keySet())
				v_dset.add(d);
		}else if(dset.startsWith("category")){
			sf.ReadDatasetAnnotation();
			String[] selected_datasets = StringUtils.split(dset, ";");	
			Set<String> hashDsetList = new HashSet<String>();
			for(int i=1; i<selected_datasets.length; i++){
				if(sf.mapEntity.containsKey(selected_datasets[i])){
					Vector<String> gselist = 
						sf.mapEntity.get(selected_datasets[i]);
					for(String g : gselist)
						for(String gg : sf.gse_index.get(g))
							hashDsetList.add(gg);
				}
			}
			for(String d : hashDsetList)
				v_dset.add(d);
		}else{ //if datasets are specified (for special dataset selection,
			//only gseid is provided)
			String[] ss = StringUtils.split(dset, ";"); //Each dataset must be GSEXXX.PLATFORM
			for(String d : ss){
				//for(String g: sf.gse_index.get(d))
				//	v_dset.add(g); //do not check, just add
				v_dset.add(d); //do not check, just add
			}
		}


		Vector<Vector<String> > vv_d = new Vector<Vector<String> > ();
		Vector<Vector<String> > vv_q = new Vector<Vector<String> > ();
		vv_d.add(new Vector<String>(v_dset));
		vv_q.add(new Vector<String>(v_query));
		
		for(String s : absent_query)
			status.add(s + " is not found in any dataset");

		if(!guide_dset.equals("none")){
			Set<String> v_guide_dset = new HashSet<String>();
			//guide dataset, load it
			for(String d : sf.guide_dset_int.keySet())
				v_guide_dset.add(d);
			Vector<Vector<String> > vv_guide_d = new Vector<Vector<String> >();
			vv_guide_d.add(new Vector<String>(v_guide_dset));
	
			//two steps:
			//1) search in FN
			//2) use FN result to help weight dataset
			status.add("Identifying guided gene set...");
			session.setAttribute(sf.sessionID + "_status", status);
			String guide_search_distance = search_distance;
			if(guide_dset.equals("FN")){
				guide_search_distance = "Zscore";
			}
			boolean ret = guided_search(vv_guide_d, vv_q, sf, session, 
				search_alg, guide_search_distance, rbp_p, percent_query, percent_genome, 
				correlation_sign, status);

			//read guided gene scores, find the top 100 guided genes
			float[] g_sc = ReadScore.ReadScoreBinary(sf.getPath(sessionID+"_guide_gscore"));
			boolean negative_cor = (correlation_sign.equals("negative")) ? true : false;
			Vector<Pair> vg_sc = ReadScore.SortScores(sf.ent_int, g_sc, negative_cor);
			Vector<String> vec_gene = new Vector<String>();
			int topGene = Math.min(100, vg_sc.size());
			for(int i=0; i<topGene; i++){
				if(!negative_cor && vg_sc.get(i).val==-320) break;
				if(negative_cor && vg_sc.get(i).val==320) break;
				vec_gene.add(vg_sc.get(i).term);
			}
			vec_gene.addAll(v_query);
			Vector<Vector<String> > vv_guided_g = new Vector<Vector<String> >();
			vv_guided_g.add(new Vector<String>(vec_gene));

			//Second step
			status.add("Performing guided search...");
			session.setAttribute(sf.sessionID + "_status", status);
			if(v_query.size()==0 || v_dset.size()==0){
				status.add("Error: no dataset contains any of the query genes");
				session.setAttribute(sf.sessionID + "_status", status);
			}else{
				String vv_category = dset;
				ret = search(vv_d, vv_category, vv_q, sf, session, 
					"CVCUSTOM", search_distance, rbp_p, percent_query, percent_genome, 
					correlation_sign, check_dset_size, vv_guided_g, status); //note: guide gene set is specified
				if(ret){
					status.add("Done writing results.");
					session.setAttribute(sf.sessionID + "_status", status);
				}
			}
		
		}else{
			if(v_query.size()==0 || v_dset.size()==0){
				status.add("Error: no dataset contains any of the query genes");
				session.setAttribute(sf.sessionID + "_status", status);
			}else{
				String vv_category = dset;
				boolean ret = search(vv_d, vv_category, vv_q, sf, session, 
					search_alg, search_distance, rbp_p, percent_query, percent_genome, 
					correlation_sign, check_dset_size, null, status); //guide gene set is null
				if(ret){
					status.add("Done writing results.");
					session.setAttribute(sf.sessionID + "_status", status);
				}
			}
		}

		for(String s : status){
			System.out.println(s);
		}
	}

	public static void main(String[] args) throws IOException{
		DoSearch2 d = new DoSearch2();

		String dset = "GSE10934.GPL570.pcl GSE13002.GPL570.pcl GSE14103.GPL570.pcl " + 
			"GSE15913.GPL570.pcl GSE16797.GPL570.pcl GSE16836.GPL570.pcl " + 
			"GSE17351.GPL570.pcl GSE17537.GPL570.pcl GSE18173.GPL6244.pcl " + 
			"GSE18349.GPL570.pcl GSE18497.GPL570.pcl GSE18826.GPL570.pcl " + 
			"GSE18864.GPL570.pcl GSE19197.GPL570.pcl GSE19713.GPL570.pcl " + 
			"GSE19826.GPL570.pcl GSE22011.GPL6244.pcl GSE3744.GPL570.pcl " + 
			"GSE4780.GPL570.pcl GSE9390.GPL570.pcl GSE9844.GPL570.pcl GSE10960.GPL570.pcl " + 
			"GSE11916.GPL570.pcl GSE12195.GPL570.pcl GSE13401.GPL570.pcl " + 
			"GSE14905.GPL570.pcl GSE17007.GPL570.pcl GSE18235.GPL570.pcl " + 
			"GSE18505.GPL570.pcl GSE18920.GPL5175.pcl GSE19018.GPL570.pcl " + 
			"GSE19697.GPL570.pcl GSE20124.GPL570.pcl GSE20595.GPL6244.pcl";
		//String query = "4174 55165 9603 5163 10130 5594 3832 4199 1058 11339 6641 " + 
		//	"701 9133 2146 10762 83879 6241 1230 55143 3418 990";
		String query = "4174 55165 9603 101242124 124025122";
		String sessionID = "ABCDEFG";
		String path="/tmp/Seek";
		//String path="/var/lib/tomcat6/work/Catalina/localhost/_";
		String tempDirPath = path;

		SeekFile sf = new SeekFile(path, "human");
		sf.tempDirPath = path;
		sf.sessionID = sessionID;
		sf.ReadGeneMap();
		sf.ReadDatasetMap();

		Vector<String> absent_query = new Vector<String>();
		Vector<String> v_dset = new Vector<String>();
		Vector<String> v_query = new Vector<String>();
		String out = "/tmp/Seek";
		String vv_category = "all";

		String[] ds = StringUtils.split(dset, " ");
		for(String s : ds)
			v_dset.add(s);

		String[] qs = StringUtils.split(query, " ");
		for(String s : qs){
			if(sf.ent_hgnc.containsKey(s))
				v_query.add(s);
			else
				absent_query.add(s);
		}

		Vector<String> status = new Vector<String>();

		Vector<Vector<String> > vv_d = new Vector<Vector<String> > ();
		Vector<Vector<String> > vv_q = new Vector<Vector<String> > ();
		vv_d.add(v_dset);
		vv_q.add(v_query);

		for(String s : absent_query)
			status.add(s + " is not found in any dataset");

		d.search(vv_d, vv_category, vv_q, sf, null, "RBP", "ZscoreHubbinessCorrected", 0.99f, 0f, 0f, "positive", "false", null, status);

		for(String s : status){
			System.out.println(s);
		}
	}

}	
